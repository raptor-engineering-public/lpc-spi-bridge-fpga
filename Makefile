# © 2017 - 2020 Raptor Engineering, LLC
#
# Released under the terms of the AGPL v3
# See the LICENSE file for full details

DESIGN_TARGET_FREQUENCY_MHZ = 50

YOSYS_ICE40_SIM_LIB = $(shell yosys-config --datdir/ice40/cells_sim.v)
TRELLIS_LIB_DIR = /usr/share/trellis

UART_CORE_FILES = third_party/uart/raminfr.v third_party/uart/timescale.v third_party/uart/uart_debug_if.v third_party/uart/uart_defines.v third_party/uart/uart_receiver.v third_party/uart/uart_regs.v third_party/uart/uart_rfifo.v third_party/uart/uart_sync_flops.v third_party/uart/uart_tfifo.v third_party/uart/uart_top.v third_party/uart/uart_transmitter.v third_party/uart/uart_wb.v

.PRECIOUS: lpc_spi_bridge_%.int

lpc_spi_bridge_%.int: lpc_spi_bridge.json timing_constraints.py main.pcf
	echo "" > $@
	nextpnr-ice40 --seed $* --hx8k --package ct256 --pcf main.pcf --freq ${DESIGN_TARGET_FREQUENCY_MHZ} --pre-pack timing_constraints.py --json $< --asc $@

lpc_spi_bridge.int: lpc_spi_bridge_1.int
	cp lpc_spi_bridge_1.int lpc_spi_bridge.int

lpc_spi_bridge.json: main.v lpc_interface.v fsi_master.v fsi_slave.v spi_master.v ipmi_bt_slave.v flash_block_cache.v simple_rtc.v clock_generator.v $(UART_CORE_FILES)
	yosys -l yosys.log -q -p "synth_ice40 -top lpc_bridge_top -json lpc_spi_bridge.json" main.v

lpc_spi_bridge.ex: lpc_spi_bridge.int
	icebox_explain lpc_spi_bridge.int > lpc_spi_bridge.ex

lpc_spi_bridge_postsynth_sim.blif: main.v lpc_interface.v fsi_master.v fsi_slave.v spi_master.v ipmi_bt_slave.v flash_block_cache.v simple_rtc.v clock_generator.v $(UART_CORE_FILES) timing_constraints.py main.pcf
	yosys -l yosys_postsynth.log -q -f "verilog -DSIMULATION -DPOST_SYNTHESIS" -p "synth_ice40 -top lpc_bridge_top -blif lpc_spi_bridge_postsynth_sim.blif" main.v

lpc_spi_bridge_postsynth_sim.v: lpc_spi_bridge_postsynth_sim.blif
	yosys -q -o lpc_spi_bridge_postsynth_sim.v lpc_spi_bridge_postsynth_sim.blif

lpc_spi_bridge.bin: lpc_spi_bridge.int
	icepack lpc_spi_bridge.int lpc_spi_bridge.bin

blank.rom:
	dd if=/dev/zero ibs=1M count=4 | tr "\000" "\377" > blank.rom

lpc_spi_bridge.rom: blank.rom lpc_spi_bridge.bin
	cp blank.rom lpc_spi_bridge.rom
	dd if=lpc_spi_bridge.bin of=lpc_spi_bridge.rom conv=notrunc

all: lpc_spi_bridge.rom

dump_toolchain_info:
	-@echo "================================================================================"
	-@echo "Base system:\t"
	-@echo -n "Architecture:\t"
	-@uname -m 2>/dev/null
	-@echo -n "gcc:\t\t"
	-@gcc -dumpversion 2>/dev/null
	-@echo -n "clang:\t\t"
	-@clang --version 2>/dev/null | head -n 1
	-@echo "\nFPGA toolchain:"
	-@echo -n "Icarus verilog:\t"
	-@iverilog -V 2>/dev/null | head -n 1
	-@echo -n "Yosys:\t\t"
	-@yosys -V 2>/dev/null
	-@echo -n "arachne-pnr:\t"
	-@arachne-pnr -v 2>/dev/null
	-@echo "================================================================================"

lpc_interface_test.vcd: main.v lpc_interface.v fsi_master.v fsi_slave.v spi_master.v ipmi_bt_slave.v flash_block_cache.v simple_rtc.v clock_generator.v $(UART_CORE_FILES) testbench_lpc.v
	rm -f lpc_interface_sim
	rm -f lpc_interface_test.vcd
	/usr/bin/iverilog -DSIMULATION -o lpc_interface_sim main.v spi_master.v simple_rtc.v clock_generator.v $(UART_CORE_FILES) testbench_lpc.v
	./lpc_interface_sim

simulate_lpc: lpc_interface_test.vcd

simulate_lpc_view: lpc_interface_test.vcd
	gtkwave lpc_interface_test.vcd

fsi_interface_test.vcd: fsi_master.v fsi_slave.v testbench_fsi.v
	rm -f fsi_interface_sim
	rm -f fsi_interface_test.vcd
	/usr/bin/iverilog -DSIMULATION -o fsi_interface_sim fsi_master.v fsi_slave.v testbench_fsi.v
	./fsi_interface_sim

simulate_fsi: fsi_interface_test.vcd

simulate_fsi_view: fsi_interface_test.vcd
	gtkwave fsi_interface_test.vcd

test: lpc_spi_bridge.bin
	iceprog -S lpc_spi_bridge.bin

flash: lpc_spi_bridge.bin
	iceprog lpc_spi_bridge.bin

clean:
	rm -f lpc_spi_bridge.json lpc_spi_bridge_postsynth_sim.blif lpc_spi_bridge_postsynth_sim.v lpc_spi_bridge.ex lpc_spi_bridge.int lpc_spi_bridge.tmg lpc_spi_bridge_*.int lpc_spi_bridge_*.tmg blank.rom lpc_spi_bridge.rom lpc_interface_sim lpc_interface_test.vcd fsi_interface_sim fsi_interface_test.vcd yosys.log
