# lpc-spi-bridge-fpga

WIP work on LPC to SPI bridge HDL for FPGA

# What is it?

OpenPOWER systems, such as the Talos II and Blackbird, use the LPC bus to load firmware despite the main host firmware (PNOR) typically residing on a SPI Flash device.  Unlike older x86 systems, OpenPOWER machines use a combination of LPC firmware cycles and several out of band (OOB) channels to the Baseboard Management Controller (BMC), which in this case is a single iCE40 FPGA with the SPI PNOR (Micron N25Q512A) attached.

This project aims to provide the bare minimum logic required to fully IPL a typical Raptor Computing Systems POWER9 host using the standard firmware.  See the Status section for current progress.

# How does it work?

The POWER9 host requires the following devices and cycles available on the LPC bus to IPL:

- Firmware memory read/write cycles
- IPMI BT
  Identity, get capabilities, and sensor commands
  - HIOMAP
    Full protocol implementation required
  - DCMI
    DCMI Capabilities and Get Power Cap commands required for OCC startup

In response to previous HIOMAP commands, the FPGA will translate LPC firmware memory cycles to specific regions of the SPI Flash, including write operations if required.  The FPGA has the capability to silently drop writes to specific regions, and to cache a small amount of the SPI PNOR Flash contents for increased speed during IPL.

When power is applied, the CPU will not start the IPL process until the SBE is fired using an external FSI command.  The POWER9 host also expects to see a 16550-compliant UART on port 0x3f8 for console output.

The POWER9 host must be able to write to the *VPD sections in the PNOR or it will not be able to start up after any hardware configuration change.  Hostboot caches all attached device VPD information in that PNOR section, and must write then read valid data in that region to continue the IPL process.

IPMI DCMI/sensor commands are primarily required for OCC configuration, but some configuration settings are also exposed to hostboot via the IPMI sensor interface.  Hostboot normally picks sane defaults when the sensors are unavailable, but it is useful to be able to control the associated hostboot options when/if desired.

# Current Status
- [x] Firmware read/write cycle support
- [x] IPMI BT interface
- [x] IPMI sensor interface
- [ ] IPMI FRU interface
- [x] IPMI RTC interface
- [x] HIOMAP interface
- [x] Minimal DCMI interface
- [x] Internal RTC implementation
- [ ] FSI logic to initiate SBE
- [x] 16550 compatible UART
