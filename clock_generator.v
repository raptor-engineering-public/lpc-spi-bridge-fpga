// © 2017 - 2020 Raptor Engineering, LLC
//
// Released under the terms of the AGPL v3
// See the LICENSE file for full details

// `define SIMULATION 1

// Because timing constraints are not yet implemented in arachne-pnr,
// it can be difficult to ensure a high enough implemented frequency
// for proper CPU operation.  Allow slowing of the main CPU clock by
// around 5% to 22.875MHz if needed
// `define USE_22_875MHZ_CLOCK 1

module clock_generator(
		// Main 12MHz platform clock
		input wire platform_clock,
		output wire spi_core_pll_lock,
		output wire slow_1843khz_uart_clock,
		output wire slow_175khz_platform_clock,
		output wire slow_183hz_platform_clock,
		output wire slow_11hz_platform_clock,

		// Main 33MHz LPC clock
		input wire lpc_clock,
		output wire sequencer_clock,
		output wire spi_core_clock,
		input wire reset_lpc_derived_plls
	);

`ifdef SIMULATION
	assign spi_core_clock = lpc_clock;
`else
	// 66MHz SPI core clock
	SB_PLL40_CORE #(
		.FEEDBACK_PATH("SIMPLE"),
		.PLLOUT_SELECT("GENCLK"),
		.DIVR(4'b0000),
		.DIVF(7'b0001111),
		.DIVQ(3'b011),
		.FILTER_RANGE(3'b011)
	) platform_pll (
		.LOCK(spi_core_pll_lock),
		.RESETB(~reset_lpc_derived_plls),
		.BYPASS(1'b0),
		.REFERENCECLK(lpc_clock),
		.PLLOUTGLOBAL(spi_core_clock)
	);
`endif

	// UART clock generator
	reg internal_1843khz_clock;
	reg [5:0] uart_clock_counter = 0;
	always @(posedge lpc_clock) begin
		if (uart_clock_counter < 8) begin
			uart_clock_counter <= uart_clock_counter + 1;
		end else begin
			internal_1843khz_clock <= ~internal_1843khz_clock;
			uart_clock_counter <= 0;
		end
	end

	// Slow clock generator
	reg [20:0] slow_clock_counter = 0;
	always @(posedge platform_clock) begin
		slow_clock_counter = slow_clock_counter + 4;
	end

	// Buffer 1843Hz slow clock
	SB_GB platform_1843khz_clock_buffer (
		.USER_SIGNAL_TO_GLOBAL_BUFFER(internal_1843khz_clock),
		.GLOBAL_BUFFER_OUTPUT(slow_1843khz_uart_clock)
	);

	// 175KHz slow clock
	assign slow_175khz_platform_clock = slow_clock_counter[5];

	// 183.10546875Hz slow clock
	assign slow_183hz_platform_clock = slow_clock_counter[16];

	// 11.44091796875Hz slow clock
	assign slow_11hz_platform_clock = slow_clock_counter[20];

	// Generate sequencer clock
	assign sequencer_clock = lpc_clock;
endmodule
