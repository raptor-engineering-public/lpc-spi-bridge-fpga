// © 2020 Raptor Engineering, LLC
//
// Released under the terms of the AGPL v3
// See the LICENSE file for full details

module flash_block_cache(
		input wire [11:0] addr_port_a,
		input wire [11:0] addr_port_b,
		input wire [11:0] read_addr,
		input wire [7:0] write_data_port_a,
		input wire [7:0] write_data_port_b,
		output reg [7:0] read_data,
		input wire wren_port_a,
		input wire wren_port_b,
		input wire port_ab_control_switch,
		input wire clk
	);

	reg [11:0] muxed_address;
	reg [7:0] muxed_write_data;
	reg [7:0] muxed_wren;

	// Muxed address lines
	always @ * begin
		case (port_ab_control_switch)
			0: muxed_address = addr_port_a;
			1: muxed_address = addr_port_b;
		endcase
	end

	// Muxed write data lines
	always @ * begin
		case (port_ab_control_switch)
			0: muxed_write_data = write_data_port_a;
			1: muxed_write_data = write_data_port_b;
		endcase
	end

	// Muxed write enable lines
	always @ * begin
		case (port_ab_control_switch)
			0: muxed_wren = wren_port_a;
			1: muxed_wren = wren_port_b;
		endcase
	end

	// Infer block RAM
	reg [7:0] mem [4095:0];
	always @(posedge clk) begin
		if (muxed_wren) begin
			mem[muxed_address] <= muxed_write_data;
		end
	end
	always @(posedge clk) begin
		read_data <= mem[read_addr];
	end
endmodule