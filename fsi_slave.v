// © 2020 Raptor Engineering, LLC
//
// Released under the BSD 3-Clause license
// See the LICENSE.fsi file for full details
//
// Alternatively, this file may be used under
// the terms of the AGPL v3.  See the LICENSE
// file for full details.

module fsi_slave_interface(
		input wire [1:0] slave_id,			// Valid IDs are 0 - 3 inclusive
		output wire [20:0] address,
		input wire [31:0] tx_data,
		output wire [31:0] rx_data,
		output wire [1:0] data_length,			// 0 == 8 bit, 1 == 16 bit, 2 = 32 bit
		output wire data_direction,			// 0 == read from slave, 1 == write to slave
		input wire enable_extended_address_mode,	// In 23-bit extended address mode, the complete address is {slave_id, address}
		input wire enable_crc_protection,
		output wire data_request_strobe,
		input wire data_ready_strobe,
		input wire [1:0] enhanced_error_recovery,	// Bit 1 == EER for IPOLL, Bit 0 == EER for all other transmissions

		input wire [1:0] interrupt_field,
		input wire [2:0] dma_control_field,

		output wire fsi_data_out,			// Must have I/O output register enabled in top level SB_IO or equivalent, output data driven at rising edge of clock
		input wire fsi_data_in,
		output wire fsi_data_direction,			// 0 == tristate (input), 1 == driven (output)
		input wire fsi_clock_in,			// Must not be inverted by the edge latch

		output wire [7:0] debug_port,

		input wire peripheral_reset,
		input wire peripheral_clock
	);

	// General FSI parameters
	// NOTE: Must match standard / master parameters!
	parameter FSI_TIMEOUT_CYCLES = 256;
	parameter FSI_TURNAROUND_CYCLES = 3;

	// Slave specific parameters
	parameter FSI_BUSY_TX_CYCLES = FSI_TIMEOUT_CYCLES-32;	// Send BUSY messages 32 cycles before anticipated timeout

	// Command codes are variable length
	parameter FSI_CODEWORD_TX_MSG_BREAK_LEN = 256;
	parameter FSI_CODEWORD_TX_MSG_ABS_ADR_DAT = 3'b100;
	parameter FSI_CODEWORD_TX_MSG_ABS_ADR_LEN = 3;
	parameter FSI_CODEWORD_TX_MSG_REL_ADR_DAT = 3'b101;
	parameter FSI_CODEWORD_TX_MSG_REL_ADR_LEN = 3;
	parameter FSI_CODEWORD_TX_MSG_SAME_ADR_DAT = 2'b11;
	parameter FSI_CODEWORD_TX_MSG_SAME_ADR_LEN = 2;
	parameter FSI_CODEWORD_TX_MSG_D_POLL_DAT = 3'b010;
	parameter FSI_CODEWORD_TX_MSG_D_POLL_LEN = 3;
	parameter FSI_CODEWORD_TX_MSG_E_POLL_DAT = 3'b011;
	parameter FSI_CODEWORD_TX_MSG_E_POLL_LEN = 3;
	parameter FSI_CODEWORD_TX_MSG_I_POLL_DAT = 3'b001;
	parameter FSI_CODEWORD_TX_MSG_I_POLL_LEN = 3;
	parameter FSI_CODEWORD_TX_MSG_TERM_DAT = 6'b111111;
	parameter FSI_CODEWORD_TX_MSG_TERM_LEN = 6;

	// All response codes are 2 bits long
	parameter FSI_CODEWORD_RX_MSG_ACK = 2'b00;
	parameter FSI_CODEWORD_RX_MSG_BUSY = 2'b01;
	parameter FSI_CODEWORD_RX_MSG_ERR_A = 2'b10;
	parameter FSI_CODEWORD_RX_MSG_ERR_C = 2'b11;

	parameter FSI_ERROR_NONE = 0;
	parameter FSI_ERROR_REL_ADDRESS = 1;
	parameter FSI_ERROR_INVALID_POLL = 2;
	parameter FSI_ERROR_INVALID_COMMAND = 3;
	parameter FSI_ERROR_BAD_TX_CHECKSUM = 4;
	parameter FSI_ERROR_BAD_RX_CHECKSUM = 5;
	parameter FSI_ERROR_INTERNAL_FAULT = 6;

	parameter FSI_TRANSFER_STATE_IDLE = 0;
	parameter FSI_TRANSFER_STATE_RX01 = 16;
	parameter FSI_TRANSFER_STATE_RX02 = 17;
	parameter FSI_TRANSFER_STATE_RX03 = 18;
	parameter FSI_TRANSFER_STATE_RX04 = 19;
	parameter FSI_TRANSFER_STATE_RX05 = 20;
	parameter FSI_TRANSFER_STATE_RX06 = 21;
	parameter FSI_TRANSFER_STATE_RX07 = 22;
	parameter FSI_TRANSFER_STATE_RX08 = 23;
	parameter FSI_TRANSFER_STATE_RX09 = 24;
	parameter FSI_TRANSFER_STATE_RX10 = 25;
	parameter FSI_TRANSFER_STATE_TR01 = 32;
	parameter FSI_TRANSFER_STATE_TR02 = 33;
	parameter FSI_TRANSFER_STATE_TX01 = 48;
	parameter FSI_TRANSFER_STATE_TX02 = 49;
	parameter FSI_TRANSFER_STATE_TX03 = 50;
	parameter FSI_TRANSFER_STATE_TX04 = 51;
	parameter FSI_TRANSFER_STATE_TX05 = 52;
	parameter FSI_TRANSFER_STATE_TX06 = 53;
	parameter FSI_TRANSFER_STATE_TX07 = 54;
	parameter FSI_TRANSFER_STATE_IR01 = 64;
	parameter FSI_TRANSFER_STATE_IR02 = 65;
	parameter FSI_TRANSFER_STATE_IR03 = 66;
	parameter FSI_TRANSFER_STATE_IR04 = 67;
	parameter FSI_TRANSFER_STATE_IR05 = 68;
	parameter FSI_TRANSFER_STATE_IR06 = 69;
	parameter FSI_TRANSFER_STATE_IR07 = 70;
	parameter FSI_TRANSFER_STATE_IR08 = 71;
	parameter FSI_TRANSFER_STATE_IR09 = 72;

	reg fsi_data_reg = 0;
	wire fsi_data_in_internal;
	reg fsi_data_direction_reg = 0;
	reg [1:0] slave_id_reg = 0;
	reg [20:0] address_reg = 0;
	reg [31:0] rx_data_reg = 0;
	reg data_direction_reg = 0;
	reg [1:0] data_length_reg = 0;
	reg data_request_strobe_reg = 0;
	reg [2:0] cycle_error_reg = 0;
	assign fsi_data_out = ~fsi_data_reg;		// FSI data line is electrically inverted
	assign fsi_data_in_internal = ~fsi_data_in;
	assign fsi_data_direction = fsi_data_direction_reg;
	assign address = address_reg;
	assign rx_data = rx_data_reg;
	assign data_direction = data_direction_reg;
	assign data_length = data_length_reg;
	assign data_request_strobe = data_request_strobe_reg;

	// Low level protocol handler
	reg fsi_data_in_internal_prev = 0;
	reg [20:0] address_rx_reg = 0;
	reg [31:0] tx_data_reg = 0;
	reg enable_extended_address_mode_reg = 0;
	reg enable_crc_protection_reg = 0;
	reg [1:0] enhanced_error_recovery_reg = 0;
	reg [8:0] cycle_counter = 0;
	reg [8:0] break_cycle_counter = 0;
	reg [20:0] last_address = 0;
	reg last_address_valid = 0;
	reg [7:0] fsi_command_code = 0;
	reg [7:0] fsi_command_code_length = 0;
	reg [7:0] fsi_command_code_prev = 0;
	reg [7:0] fsi_command_code_length_prev = 0;
	reg [7:0] fsi_response_code_prev = 0;
	reg fsi_response_code_prev_valid = 0;
	reg crc_protected_bits_transmitting = 0;
	reg crc_protected_bits_receiving = 0;
	reg fsi_data_reg_internal = 0;
	reg [3:0] crc_data = 0;
	reg crc_feedback = 0;
	reg [7:0] control_state = 0;
	reg [1:0] rx_slave_id = 0;
	reg [1:0] fsi_response_code = 0;
	reg fsi_response_code_set = 0;
	reg [8:0] timeout_counter = 0;
	reg fsi_rel_address_delta_negative = 0;
	reg [7:0] fsi_rel_address_delta = 0;
	reg [1:0] slave_error_recovery_state = 0;
	reg ipoll_in_process = 0;
	reg busy_response_in_process = 0;
	reg [1:0] interrupt_field_reg = 0;
	reg [2:0] dma_control_field_reg = 0;

	assign debug_port = control_state;

	always @(posedge fsi_clock_in) begin
		if (peripheral_reset) begin
			data_request_strobe_reg <= 0;
			last_address_valid <= 0;
			fsi_response_code_prev_valid <= 0;
			crc_protected_bits_receiving <= 0;
			ipoll_in_process <= 0;
			busy_response_in_process <= 0;
			break_cycle_counter <= 0;
			data_request_strobe_reg <= 0;
			cycle_error_reg <= FSI_ERROR_NONE;
			control_state <= FSI_TRANSFER_STATE_IDLE;
		end else begin
			if (break_cycle_counter >= (FSI_CODEWORD_TX_MSG_BREAK_LEN - 1)) begin
				data_request_strobe_reg <= 0;
				last_address_valid <= 0;
				fsi_response_code_prev_valid <= 0;
				crc_protected_bits_receiving <= 0;
				ipoll_in_process <= 0;
				busy_response_in_process <= 0;
				cycle_error_reg <= FSI_ERROR_NONE;
				control_state <= FSI_TRANSFER_STATE_IDLE;
			end else begin
				case (control_state)
					FSI_TRANSFER_STATE_IDLE: begin
						// Wait for start signal (logical 0 to 1 transition)
						//
						// Detecting a logical 1 only, without the transition, would
						// exacerbate any desync encountered.  Especially in the case
						// of a received BREAK command, it would result in continual
						// error generation until the end of the command was received!
						if (fsi_data_in_internal && !fsi_data_in_internal_prev) begin
							slave_id_reg <= slave_id;
							enable_extended_address_mode_reg <= enable_extended_address_mode;
							enable_crc_protection_reg <= enable_crc_protection;
							enhanced_error_recovery_reg <= enhanced_error_recovery;
							crc_protected_bits_receiving = 1;
							control_state <= FSI_TRANSFER_STATE_RX01;
						end
						if (!busy_response_in_process) begin
							data_request_strobe_reg <= 0;
						end
						fsi_response_code_set <= 0;
						ipoll_in_process <= 0;
					end
					FSI_TRANSFER_STATE_RX01: begin
						// Receive slave ID, bit 1
						crc_protected_bits_receiving = 1;
						rx_slave_id[1] <= fsi_data_in_internal;
						control_state <= FSI_TRANSFER_STATE_RX02;
					end
					FSI_TRANSFER_STATE_RX02: begin
						// Receive slave ID, bit 2
						crc_protected_bits_receiving = 1;
						rx_slave_id[0] <= fsi_data_in_internal;
						cycle_counter <= 0;
						fsi_command_code <= 0;
						control_state <= FSI_TRANSFER_STATE_RX03;
					end
					FSI_TRANSFER_STATE_RX03: begin
						// Receive command code
						crc_protected_bits_receiving = 1;
						fsi_command_code[7:1] <= fsi_command_code[6:0];
						fsi_command_code[0] <= fsi_data_in_internal;
						cycle_counter <= cycle_counter + 1;
						if (((cycle_counter + 1) == FSI_CODEWORD_TX_MSG_ABS_ADR_LEN)
							&& ({fsi_command_code[6:0], fsi_data_in_internal} == FSI_CODEWORD_TX_MSG_ABS_ADR_DAT)) begin
							fsi_command_code_length <= FSI_CODEWORD_TX_MSG_ABS_ADR_LEN;
							address_reg <= 0;
							cycle_counter <= 21;
							control_state <= FSI_TRANSFER_STATE_RX04;
						end else if (((cycle_counter + 1) == FSI_CODEWORD_TX_MSG_REL_ADR_LEN)
							&& ({fsi_command_code[6:0], fsi_data_in_internal} == FSI_CODEWORD_TX_MSG_REL_ADR_DAT)) begin
							fsi_command_code_length <= FSI_CODEWORD_TX_MSG_REL_ADR_LEN;
							address_reg <= 0;
							cycle_counter <= 8;
							control_state <= FSI_TRANSFER_STATE_RX04;
						end else if (((cycle_counter + 1) == FSI_CODEWORD_TX_MSG_SAME_ADR_LEN)
							&& ({fsi_command_code[6:0], fsi_data_in_internal} == FSI_CODEWORD_TX_MSG_SAME_ADR_DAT)) begin
							fsi_command_code_length <= FSI_CODEWORD_TX_MSG_SAME_ADR_LEN;
							address_reg <= 0;
							cycle_counter <= 2;
							control_state <= FSI_TRANSFER_STATE_RX04;
						end else if (((cycle_counter + 1) == FSI_CODEWORD_TX_MSG_D_POLL_LEN)
							&& ({fsi_command_code[6:0], fsi_data_in_internal} == FSI_CODEWORD_TX_MSG_D_POLL_DAT)) begin
							fsi_command_code_length <= FSI_CODEWORD_TX_MSG_D_POLL_LEN;
							if (busy_response_in_process) begin
								cycle_counter <= 4;
								control_state <= FSI_TRANSFER_STATE_RX09;
							end else begin
								busy_response_in_process <= 0;
								cycle_error_reg <= FSI_ERROR_INVALID_POLL;
								control_state <= FSI_TRANSFER_STATE_IDLE;
							end
						end else if (((cycle_counter + 1) == FSI_CODEWORD_TX_MSG_E_POLL_LEN)
							&& ({fsi_command_code[6:0], fsi_data_in_internal} == FSI_CODEWORD_TX_MSG_E_POLL_DAT)) begin
							fsi_command_code_length <= FSI_CODEWORD_TX_MSG_E_POLL_LEN;
							if (fsi_response_code_prev_valid) begin
								cycle_counter <= 4;
								control_state <= FSI_TRANSFER_STATE_RX09;
							end else begin
								busy_response_in_process <= 0;
								cycle_error_reg <= FSI_ERROR_INVALID_POLL;
								control_state <= FSI_TRANSFER_STATE_IDLE;
							end
						end else if (((cycle_counter + 1) == FSI_CODEWORD_TX_MSG_I_POLL_LEN)
							&& ({fsi_command_code[6:0], fsi_data_in_internal} == FSI_CODEWORD_TX_MSG_I_POLL_DAT)) begin
							fsi_command_code_length <= FSI_CODEWORD_TX_MSG_I_POLL_LEN;
							ipoll_in_process <= 1;
							cycle_counter <= 4;
							control_state <= FSI_TRANSFER_STATE_RX09;
						end else begin
							if (cycle_counter >= 7) begin
								busy_response_in_process <= 0;
								cycle_error_reg <= FSI_ERROR_INVALID_COMMAND;
								control_state <= FSI_TRANSFER_STATE_IDLE;
							end
						end
					end
					FSI_TRANSFER_STATE_RX04: begin
						// Receive read/write flag
						crc_protected_bits_receiving = 1;
						data_direction_reg <= ~fsi_data_in_internal;
						if (fsi_command_code == FSI_CODEWORD_TX_MSG_REL_ADR_DAT) begin
							control_state <= FSI_TRANSFER_STATE_RX05;
						end else begin
							control_state <= FSI_TRANSFER_STATE_RX06;
						end
					end
					FSI_TRANSFER_STATE_RX05: begin
						// Receive relative address sign bit
						crc_protected_bits_receiving = 1;
						fsi_rel_address_delta_negative <= fsi_data_in_internal;
						control_state <= FSI_TRANSFER_STATE_RX06;
					end
					FSI_TRANSFER_STATE_RX06: begin
						// Receive address
						if (cycle_counter > 0) begin
							crc_protected_bits_receiving = 1;
							address_reg[cycle_counter-1] = fsi_data_in_internal;
							cycle_counter <= cycle_counter - 1;
							if (cycle_counter == 1) begin
								// Invalid sizes are treated as byte transfers
								data_length_reg <= 0;
								control_state <= FSI_TRANSFER_STATE_RX07;
							end
						end else begin
							// Should never reach this state
							busy_response_in_process <= 0;
							cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
							control_state <= FSI_TRANSFER_STATE_IDLE;
						end
					end
					FSI_TRANSFER_STATE_RX07: begin
						// Receive data size bit
						crc_protected_bits_receiving = 1;
						if (fsi_data_in_internal == 1) begin
							if (!address_reg[1] && address_reg[0]) begin
								data_length_reg = 2;
							end else if (!address_reg[0]) begin
								data_length_reg = 1;
							end
						end else begin
							data_length_reg = 0;
						end
						if (data_direction_reg) begin
							// Write
							case (data_length_reg)
								0: begin
									// Byte transfer
									cycle_counter <= 8;
								end
								1: begin
									// Half word transfer
									cycle_counter <= 16;
								end
								2: begin
									// Word transfer
									cycle_counter <= 32;
								end
								default: begin
									// Invalid sizes are treated as byte transfers
									cycle_counter <= 8;
								end
							endcase
							rx_data_reg <= 0;
							control_state <= FSI_TRANSFER_STATE_RX08;
						end else begin
							// Read
							cycle_counter <= 4;
							control_state <= FSI_TRANSFER_STATE_RX09;
						end

						// Compute absolute address if required
						if (fsi_command_code == FSI_CODEWORD_TX_MSG_REL_ADR_DAT) begin
							if (last_address_valid) begin
								if (fsi_rel_address_delta_negative) begin
									address_reg = last_address - (9'h100 - address_reg[7:0]);
								end else begin
									address_reg = last_address + address_reg[7:0];
								end
							end else begin
								// Relative address command used without prior absolute address command
								// Idle transition will be queued in next state
								busy_response_in_process <= 0;
								cycle_error_reg <= FSI_ERROR_REL_ADDRESS;
							end
						end else if (fsi_command_code == FSI_CODEWORD_TX_MSG_SAME_ADR_DAT) begin
							if (!data_direction && (address_reg[1:0] == 2'b11) && fsi_data_in_internal) begin
								// OpenFSI suffers from an unfortunate conflict between the SAME_ADR command
								// and the TERM command.  Both start with 2'b11 and since both are variable
								// length it is impossible to determine if a TERM command was sent until this
								// point in the receiver process!
								//
								// Set correct command code for further processing
								fsi_command_code <= FSI_CODEWORD_TX_MSG_TERM_DAT;
								fsi_command_code_length <= FSI_CODEWORD_TX_MSG_TERM_LEN;
							end else begin
								if (last_address_valid) begin
									address_reg <= {last_address[20:2], address[1:0]};
								end else begin
									// Relative address command used without prior absolute address command
									// Idle transition will be queued in next state
									busy_response_in_process <= 0;
									cycle_error_reg <= FSI_ERROR_REL_ADDRESS;
								end
							end
						end

						// Force lowest address bits to specification-mandated values if required
						case (data_length_reg)
							0: address_reg = address_reg[20:0];
							1: address_reg = {address_reg[20:1], 1'b0};
							2: address_reg = {address_reg[20:2], 2'b01};
							default: address_reg = address_reg[20:0];
						endcase
					end
					FSI_TRANSFER_STATE_RX08: begin
						if ((fsi_command_code_length == FSI_CODEWORD_TX_MSG_TERM_LEN) && (fsi_command_code == FSI_CODEWORD_TX_MSG_TERM_DAT)) begin
							// If a TERM command is detected at this point, ACK it and unset BUSY status...
							busy_response_in_process <= 0;
							control_state <= FSI_TRANSFER_STATE_RX09;
						end else if (cycle_error_reg == FSI_ERROR_REL_ADDRESS) begin
							control_state <= FSI_TRANSFER_STATE_IDLE;
						end else begin
							// Receive data
							if (cycle_counter > 0) begin
								crc_protected_bits_receiving = 1;
								rx_data_reg[cycle_counter-1] = fsi_data_in_internal;
								cycle_counter <= cycle_counter - 1;
								if (cycle_counter == 1) begin
									cycle_counter <= 4;
									control_state <= FSI_TRANSFER_STATE_RX09;
								end
							end else begin
								// Should never reach this state
								busy_response_in_process <= 0;
								cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
								control_state <= FSI_TRANSFER_STATE_IDLE;
							end
						end
					end
					FSI_TRANSFER_STATE_RX09: begin
						if (cycle_error_reg == FSI_ERROR_REL_ADDRESS) begin
							control_state <= FSI_TRANSFER_STATE_IDLE;
						end else begin
							// Receive CRC
							if (cycle_counter > 0) begin
								// Load CRC bits into Galios LFSR for verification
								crc_protected_bits_receiving = 1;
								cycle_counter <= cycle_counter - 1;
								if (cycle_counter == 1) begin
									control_state <= FSI_TRANSFER_STATE_RX10;
								end
							end else begin
								// Should never reach this state
								busy_response_in_process <= 0;
								cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
								control_state <= FSI_TRANSFER_STATE_IDLE;
							end
						end
					end
					FSI_TRANSFER_STATE_RX10: begin
						crc_protected_bits_receiving = 0;
						if ((crc_data != 0) && enable_crc_protection_reg) begin
							// Slave received corrupted message from master
							if (((enhanced_error_recovery_reg[1] && ipoll_in_process)
								|| (enhanced_error_recovery_reg[0] && !ipoll_in_process))
								&& (slave_error_recovery_state == 0)) begin
								// Configure state machine to send ERR_C
								fsi_response_code <= FSI_CODEWORD_RX_MSG_ERR_C;
								cycle_counter <= 2;
								fsi_response_code_set <= 1;
								crc_data <= 0;
								slave_error_recovery_state <= 1;
								control_state <= FSI_TRANSFER_STATE_TR01;
							end else begin
								// Master does not support enhanced error recovery or error recovery failed
								if (!ipoll_in_process) begin
									cycle_error_reg <= FSI_ERROR_BAD_RX_CHECKSUM;
								end
								// Configure state machine to send ERR_A
								fsi_response_code <= FSI_CODEWORD_RX_MSG_ERR_A;
								cycle_counter <= 2;
								fsi_response_code_set <= 1;
								crc_data <= 0;
								control_state <= FSI_TRANSFER_STATE_TR01;
							end
						end else begin
							if (!enable_extended_address_mode_reg && (rx_slave_id != slave_id_reg)) begin
								// Message was not meant for this slave
								// Return to idle
								address_reg <= 0;
								rx_data_reg <= 0;
								crc_data <= 0;
								control_state <= FSI_TRANSFER_STATE_IDLE;
							end else begin
								if ((ipoll_in_process)
									|| ((fsi_command_code_length == FSI_CODEWORD_TX_MSG_E_POLL_LEN) && (fsi_command_code == FSI_CODEWORD_TX_MSG_E_POLL_DAT))) begin
									if (!busy_response_in_process) begin
										data_request_strobe_reg <= 0;
									end
								end else begin
									data_request_strobe_reg <= 1;
								end
								slave_error_recovery_state <= 0;
								last_address <= address_reg;
								last_address_valid <= 1;
								control_state <= FSI_TRANSFER_STATE_TR01;
							end
						end
					end
					FSI_TRANSFER_STATE_TR01: begin
						// Reception complete, switch direction
						fsi_data_reg_internal = 0;

						// Stop CRC generation
						crc_protected_bits_transmitting = 0;
						crc_protected_bits_receiving = 0;

						if (cycle_counter >= (FSI_TURNAROUND_CYCLES - 1)) begin
							crc_data <= 0;
							if (slave_error_recovery_state == 1) begin
								slave_error_recovery_state <= 2;
								control_state <= FSI_TRANSFER_STATE_TX01;
							end else if (cycle_error_reg == FSI_ERROR_BAD_RX_CHECKSUM) begin
								control_state <= FSI_TRANSFER_STATE_TX01;
							end else if ((fsi_command_code_length == FSI_CODEWORD_TX_MSG_E_POLL_LEN) && (fsi_command_code == FSI_CODEWORD_TX_MSG_E_POLL_DAT)) begin
								fsi_response_code <= fsi_response_code_prev;
								fsi_command_code <= fsi_command_code_prev;
								fsi_command_code_length <= fsi_command_code_length_prev;
								fsi_response_code_set <= 1;
								control_state <= FSI_TRANSFER_STATE_TX01;
							end else if (ipoll_in_process) begin
								interrupt_field_reg <= interrupt_field;
								dma_control_field_reg <= dma_control_field;
								control_state <= FSI_TRANSFER_STATE_IR01;
							end else begin
								timeout_counter <= 0;
								busy_response_in_process <= 0;
								control_state <= FSI_TRANSFER_STATE_TR02;
							end
							fsi_data_direction_reg <= 0;
						end else begin
							cycle_counter <= cycle_counter + 1;
						end
					end
					FSI_TRANSFER_STATE_TR02: begin
						if ((fsi_command_code_length == FSI_CODEWORD_TX_MSG_TERM_LEN) && (fsi_command_code == FSI_CODEWORD_TX_MSG_TERM_DAT)) begin
							// Terminate active transfer and ACK
							data_request_strobe_reg <= 0;
							control_state <= FSI_TRANSFER_STATE_TX01;
						end else begin
							// Wait for data ready strobe
							if (timeout_counter < FSI_BUSY_TX_CYCLES) begin
								if (data_ready_strobe) begin
									data_request_strobe_reg <= 0;
									tx_data_reg <= tx_data;
									control_state <= FSI_TRANSFER_STATE_TX01;
								end
								timeout_counter <= timeout_counter + 1;
							end else begin
								fsi_response_code <= FSI_CODEWORD_RX_MSG_BUSY;
								cycle_counter <= 2;
								busy_response_in_process <= 1;
								fsi_response_code_set <= 1;
								control_state <= FSI_TRANSFER_STATE_TX01;
							end
						end
					end
					FSI_TRANSFER_STATE_TX01: begin
						if (!data_ready_strobe) begin
							// Send start bit
							fsi_data_direction_reg <= 1;
							crc_protected_bits_transmitting = 1;
							fsi_data_reg_internal = 1;

							// Set up response code
							if (fsi_response_code_set) begin
								// Response code already set up, transmit as-is...
								cycle_counter <= 2;
								fsi_response_code_set <= 1;
								control_state <= FSI_TRANSFER_STATE_TX02;
							end else begin
								// Set based on command...
								if (((fsi_command_code_length == FSI_CODEWORD_TX_MSG_ABS_ADR_LEN) && (fsi_command_code == FSI_CODEWORD_TX_MSG_ABS_ADR_DAT))
									|| ((fsi_command_code_length == FSI_CODEWORD_TX_MSG_REL_ADR_LEN) && (fsi_command_code == FSI_CODEWORD_TX_MSG_REL_ADR_DAT))
									|| ((fsi_command_code_length == FSI_CODEWORD_TX_MSG_SAME_ADR_LEN) && (fsi_command_code == FSI_CODEWORD_TX_MSG_SAME_ADR_DAT))
									|| ((fsi_command_code_length == FSI_CODEWORD_TX_MSG_D_POLL_LEN) && (fsi_command_code == FSI_CODEWORD_TX_MSG_D_POLL_DAT))
									|| ((fsi_command_code_length == FSI_CODEWORD_TX_MSG_TERM_LEN) && (fsi_command_code == FSI_CODEWORD_TX_MSG_TERM_DAT))) begin
									fsi_response_code <= FSI_CODEWORD_RX_MSG_ACK;
									cycle_counter <= 2;
									fsi_response_code_set <= 1;
									if (enable_extended_address_mode_reg) begin
										control_state <= FSI_TRANSFER_STATE_TX04;
									end else begin
										control_state <= FSI_TRANSFER_STATE_TX02;
									end
								end else begin
									busy_response_in_process <= 0;
									cycle_error_reg <= FSI_ERROR_INVALID_COMMAND;
									control_state <= FSI_TRANSFER_STATE_IDLE;
								end
							end
						end
					end
					FSI_TRANSFER_STATE_TX02: begin
						// Send slave ID bit 1
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = (slave_id_reg >> 1) & 1'b1;
						control_state <= FSI_TRANSFER_STATE_TX03;
					end
					FSI_TRANSFER_STATE_TX03: begin
						// Send slave ID bit 2
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = slave_id_reg & 1'b1;
						control_state <= FSI_TRANSFER_STATE_TX04;
					end
					FSI_TRANSFER_STATE_TX04: begin
						// Send response code
						if (cycle_counter > 0) begin
							fsi_data_direction_reg <= 1;
							crc_protected_bits_transmitting = 1;
							fsi_data_reg_internal = fsi_response_code[cycle_counter-1];
							cycle_counter <= cycle_counter - 1;
							if (cycle_counter == 1) begin
								if ((fsi_command_code == FSI_CODEWORD_TX_MSG_TERM_DAT) && (fsi_command_code_length == FSI_CODEWORD_TX_MSG_TERM_LEN)) begin
									// TERM command received -- send ACK message
									cycle_counter <= 4;
									control_state <= FSI_TRANSFER_STATE_TX06;
								end else begin
									if (fsi_response_code == FSI_CODEWORD_RX_MSG_ACK) begin
										if (data_direction_reg) begin
											cycle_counter <= 4;
											control_state <= FSI_TRANSFER_STATE_TX06;
										end else begin
											case (data_length_reg)
												0: begin
													// Byte transfer
													cycle_counter <= 8;
												end
												1: begin
													// Half word transfer
													cycle_counter <= 16;
												end
												2: begin
													// Word transfer
													cycle_counter <= 32;
												end
												default: begin
													// Invalid sizes are treated as byte transfers
													cycle_counter <= 8;
												end
											endcase
											control_state <= FSI_TRANSFER_STATE_TX05;
										end
									end else begin
										// All other response codes do not contain any data
										cycle_counter <= 4;
										control_state <= FSI_TRANSFER_STATE_TX06;
									end
								end
							end
						end else begin
							// Should never reach this state
							busy_response_in_process <= 0;
							cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
							control_state <= FSI_TRANSFER_STATE_IDLE;
						end
					end
					FSI_TRANSFER_STATE_TX05: begin
						// Send data
						if (cycle_counter > 0) begin
							fsi_data_direction_reg <= 1;
							crc_protected_bits_transmitting = 1;
							fsi_data_reg_internal = tx_data_reg[cycle_counter-1];
							cycle_counter <= cycle_counter - 1;
							if (cycle_counter == 1) begin
								cycle_counter <= 4;
								control_state <= FSI_TRANSFER_STATE_TX06;
							end
						end else begin
							// Should never reach this state
							busy_response_in_process <= 0;
							cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
							control_state <= FSI_TRANSFER_STATE_IDLE;
						end
					end
					FSI_TRANSFER_STATE_TX06: begin
						// Send CRC
						if (cycle_counter > 0) begin
							fsi_data_direction_reg <= 1;
							crc_protected_bits_transmitting = 0;
							fsi_data_reg_internal = crc_data[cycle_counter-1];
							cycle_counter <= cycle_counter - 1;
							if (cycle_counter == 1) begin
								control_state <= FSI_TRANSFER_STATE_TX07;
							end
						end else begin
							// Should never reach this state
							busy_response_in_process <= 0;
							cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
							control_state <= FSI_TRANSFER_STATE_IDLE;
						end
					end
					FSI_TRANSFER_STATE_TX07: begin
						// Transmission complete, switch direction
						fsi_data_reg_internal = 0;

						if (cycle_counter >= (FSI_TURNAROUND_CYCLES - 1)) begin
							if (busy_response_in_process) begin
								timeout_counter <= 0;
								crc_data <= 0;
								control_state <= FSI_TRANSFER_STATE_IDLE;
							end else begin
								fsi_command_code_prev <= fsi_command_code;
								fsi_command_code_length_prev <= fsi_command_code_length;
								fsi_response_code_prev <= fsi_response_code;
								fsi_response_code_prev_valid <= 1;
								crc_data <= 0;
								control_state <= FSI_TRANSFER_STATE_IDLE;
							end
							fsi_data_direction_reg <= 0;
						end else begin
							cycle_counter <= cycle_counter + 1;
						end
					end
					FSI_TRANSFER_STATE_IR01: begin
						// Send start bit
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = 1;
						control_state <= FSI_TRANSFER_STATE_IR02;
					end
					FSI_TRANSFER_STATE_IR02: begin
						// Send slave ID bit 1
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = (slave_id_reg >> 1) & 1'b1;
						control_state <= FSI_TRANSFER_STATE_IR03;
					end
					FSI_TRANSFER_STATE_IR03: begin
						// Send slave ID bit 2
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = slave_id_reg & 1'b1;
						control_state <= FSI_TRANSFER_STATE_IR04;
					end
					FSI_TRANSFER_STATE_IR04: begin
						// Send I_POLL_RSP message type
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = 1'b0;
						control_state <= FSI_TRANSFER_STATE_IR05;
					end
					FSI_TRANSFER_STATE_IR05: begin
						// Send interrupt field bit 1
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = interrupt_field_reg[1];
						control_state <= FSI_TRANSFER_STATE_IR06;
					end
					FSI_TRANSFER_STATE_IR06: begin
						// Send interrupt field bit 2
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = interrupt_field_reg[0];
						control_state <= FSI_TRANSFER_STATE_IR07;
					end
					FSI_TRANSFER_STATE_IR07: begin
						// Send interrupt field bit 1
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = dma_control_field_reg[2];
						control_state <= FSI_TRANSFER_STATE_IR08;
					end
					FSI_TRANSFER_STATE_IR08: begin
						// Send interrupt field bit 2
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = dma_control_field_reg[1];
						control_state <= FSI_TRANSFER_STATE_IR09;
					end
					FSI_TRANSFER_STATE_IR09: begin
						// Send interrupt field bit 3
						fsi_data_direction_reg <= 1;
						crc_protected_bits_transmitting = 1;
						fsi_data_reg_internal = dma_control_field_reg[0];
						cycle_counter <= 4;
						control_state <= FSI_TRANSFER_STATE_TX06;
					end
					default: begin
						// Should never reach this state
						busy_response_in_process <= 0;
						cycle_error_reg <= FSI_ERROR_INTERNAL_FAULT;
						control_state <= FSI_TRANSFER_STATE_IDLE;
					end
				endcase
			end

			// Watch for asynchronous BREAK command
			if (!fsi_data_in_internal) begin
				break_cycle_counter <= 0;
			end else begin
				if (fsi_data_in_internal) begin
					if (break_cycle_counter < (FSI_CODEWORD_TX_MSG_BREAK_LEN - 1)) begin
						break_cycle_counter <= break_cycle_counter + 1;
					end
				end else begin
					break_cycle_counter <= 0;
				end
			end
		end

		// CRC calculation
		// Implement Galios-type LFSR for polynomial 0x7 (MSB first)
		if (crc_protected_bits_transmitting) begin
			crc_feedback = crc_data[3] ^ fsi_data_reg_internal;
		end
		if (crc_protected_bits_receiving) begin
			crc_feedback = crc_data[3] ^ fsi_data_in_internal;
		end
		if (crc_protected_bits_transmitting || crc_protected_bits_receiving) begin
			crc_data[0] <= crc_feedback;
			crc_data[1] <= crc_data[0] ^ crc_feedback;
			crc_data[2] <= crc_data[1] ^ crc_feedback;
			crc_data[3] <= crc_data[2];
		end

		// Transmit data
		fsi_data_reg <= fsi_data_reg_internal;

		fsi_data_in_internal_prev <= fsi_data_in_internal;
	end
endmodule
