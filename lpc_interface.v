// © 2017 - 2020 Raptor Engineering, LLC
//
// Released under the terms of the AGPL v3
// See the LICENSE file for full details
//
// This LPC slave peripheral currently implements I/O, TPM, and firmware memory read/write functionality

`ifndef DISABLE_FIRMWARE_MEMORY_CYCLES
`define ENABLE_FIRMWARE_MEMORY_CYCLES
`endif

module lpc_slave_wishbone(
		// Wishbone interface signals
		input clk_i,
		input wire [31:0] dat_i,
		output reg [31:0] dat_o,
		input wire [31:0] adr_i,
		input wire rst_i,

		output reg ack_o,
		input wire cyc_i,
		input wire stb_i,
		input wire we_i,

		output reg irq_o,

		// LPC core signals
`ifdef LPC_SLAVE_DEBUG
		output wire [15:0] debug_port,
`endif

		output wire [3:0] lpc_data_out,
		input wire [3:0] lpc_data_in,
		output wire lpc_data_direction,	// 0 == tristate (input), 1 == driven (output)
		output wire lpc_irq_out,
		input wire lpc_irq_in,
		output wire lpc_irq_direction,	// 0 == tristate (input), 1 == driven (output)

		input wire lpc_frame_n,
		input wire lpc_reset_n,
		input wire lpc_rx_clock,
		input wire lpc_tx_clock
	);

	// Internal LPC interface signals
	wire [27:0] lpc_slave_address;
	reg [7:0] lpc_slave_tx_data;
	wire [7:0] lpc_slave_rx_data;
	wire lpc_slave_tpm_cycle;
	wire lpc_slave_firmware_cycle;
	reg lpc_slave_continue;
	reg lpc_slave_data_ack;
	reg lpc_slave_exception_ack;
	wire lpc_slave_address_ready;
	wire lpc_slave_data_ready;
	wire [2:0] lpc_slave_exception;
	wire lpc_slave_cycle_direction;

	reg [16:0] irq_request;
	reg irq_tx_ready;
	wire irq_tx_queued;

	reg [8:0] lpc_fw_input_xfer_write_addr;
	reg [7:0] lpc_fw_input_xfer_write_data;
	reg lpc_fw_input_xfer_write_wren;
	reg [8:0] lpc_fw_output_xfer_read_addr;
	reg [8:0] lpc_fw_output_xfer_read_addr_prev;
	wire [7:0] lpc_fw_output_xfer_read_data;

	wire [3:0] lpc_slave_fw_idsel;
	wire [3:0] lpc_slave_fw_msize;

	wire combined_system_reset_n;
	reg lpc_slave_address_ready_prev;

	// Wishbone connector
	assign combined_system_reset_n = lpc_reset_n && !rst_i;
	always @(posedge clk_i) begin
		if (rst_i) begin
			ack_o <= 0;
			irq_o <= 0;
			dat_o <= 0;
			lpc_slave_address_ready_prev <= 0;
			lpc_fw_input_xfer_write_wren <= 0;
			lpc_fw_output_xfer_read_addr_prev <= 0;
		end else begin


			// Assert IRQ on incoming request from LPC side
			if (lpc_slave_address_ready_prev != lpc_slave_address_ready) begin
				irq_o <= 1;
			end

			if (stb_i) begin
				if (we_i) begin
					if (adr_i[31:8] == 1) begin
						// Buffer memory transfer
						lpc_fw_input_xfer_write_addr <= adr_i[7:0];
						lpc_fw_input_xfer_write_data <= dat_i[7:0];
						lpc_fw_input_xfer_write_wren <= 1;
					end else begin
						// Control register access
						case (adr_i)
							0: begin
								irq_request <= dat_i[16:0];
								irq_tx_ready <= dat_i[17];
								lpc_slave_tx_data <= dat_i[31:24];
							end
						endcase

						lpc_fw_input_xfer_write_wren <= 0;
					end

					lpc_slave_continue <= 1;
					lpc_slave_data_ack <= 0;
					lpc_slave_exception_ack <= 0;

					ack_o <= 1;
				end else begin
					if (adr_i[31:8] == 1) begin
						// Buffer memory transfer
						lpc_fw_output_xfer_read_addr <= adr_i[7:0];

						if (lpc_fw_output_xfer_read_addr_prev == lpc_fw_output_xfer_read_addr) begin
							dat_o[31:8] <= 0;
							dat_o[7:0] <= lpc_fw_output_xfer_read_data;
							ack_o <= 1;
						end else begin
							ack_o <= 0;
						end

						lpc_slave_data_ack <= 0;
						lpc_slave_exception_ack <= 0;
					end else begin
						// Control register access
						case (adr_i)
							// LPC I/O register access
							0: begin
								dat_o[15:0] <= lpc_slave_address[15:0];
								dat_o[16] <= lpc_slave_address_ready;
								dat_o[17] <= lpc_slave_data_ready;
								dat_o[20:18] <= lpc_slave_exception;
								dat_o[21] <= lpc_slave_cycle_direction;
								dat_o[22] <= lpc_slave_tpm_cycle;
								dat_o[23] <= lpc_slave_firmware_cycle;
								dat_o[31:24] <= lpc_slave_rx_data;

								lpc_slave_data_ack <= 1;
								lpc_slave_exception_ack <= 1;
							end
							// Firmware memory cycle access
							1: begin
								dat_o[11:0] <= lpc_slave_address[27:16];
								dat_o[15:12] <= lpc_slave_fw_msize;
								dat_o[19:16] <= lpc_slave_fw_idsel;
								dat_o[31:20] <= 0;
							end
							default: begin
								dat_o <= 0;

								lpc_slave_data_ack <= 0;
								lpc_slave_exception_ack <= 0;
							end
						endcase

						ack_o <= 1;
					end

					lpc_slave_continue <= 0;
					lpc_fw_input_xfer_write_wren <= 0;
				end

				irq_o <= 0;
			end else begin
				lpc_slave_continue <= 0;
				lpc_slave_data_ack <= 0;
				lpc_slave_exception_ack <= 0;
				lpc_fw_input_xfer_write_wren <= 0;

				ack_o <= 0;
			end

			lpc_slave_address_ready_prev <= lpc_slave_address_ready;
			lpc_fw_output_xfer_read_addr_prev <= lpc_fw_output_xfer_read_addr;
		end
	end

	// Instantiate slave
	lpc_slave_interface lpc_slave_interface(
		.address(lpc_slave_address),
		.tx_data(lpc_slave_tx_data),
		.rx_data(lpc_slave_rx_data),
		.tpm_cycle(lpc_slave_tpm_cycle),
		.firmware_cycle(lpc_slave_firmware_cycle),
		.continue(lpc_slave_continue),
		.data_ack(lpc_slave_data_ack),
		.exception_ack(lpc_slave_exception_ack),
		.address_ready(lpc_slave_address_ready),
		.data_ready(lpc_slave_data_ready),
		.exception(lpc_slave_exception),
		.data_direction(lpc_slave_cycle_direction),

		.irq_request(irq_request),
		.irq_tx_ready(irq_tx_ready),
		.irq_tx_queued(irq_tx_queued),

		.lpc_fw_input_xfer_write_addr(lpc_fw_input_xfer_write_addr),
		.lpc_fw_input_xfer_write_data(lpc_fw_input_xfer_write_data),
		.lpc_fw_input_xfer_write_clk(clk_i),
		.lpc_fw_input_xfer_write_wren(lpc_fw_input_xfer_write_wren),
		.lpc_fw_output_xfer_read_addr(lpc_fw_output_xfer_read_addr),
		.lpc_fw_output_xfer_read_data(lpc_fw_output_xfer_read_data),
		.lpc_fw_output_xfer_read_clk(clk_i),

		.fw_idsel(lpc_slave_fw_idsel),
		.fw_msize(lpc_slave_fw_msize),

`ifdef LPC_SLAVE_DEBUG
		.debug_port(debug_port),
`endif

		.lpc_data_out(lpc_data_out),
		.lpc_data_in(lpc_data_in),
		.lpc_data_direction(lpc_data_direction),

		.lpc_irq_out(lpc_irq_out),
		.lpc_irq_in(lpc_irq_in),
		.lpc_irq_direction(lpc_irq_direction),

		.lpc_frame_n(lpc_frame_n),
		.lpc_reset_n(combined_system_reset_n),
		.lpc_rx_clock(lpc_rx_clock),
		.lpc_tx_clock(lpc_tx_clock)
	);
endmodule

module lpc_slave_interface(
		output wire [27:0] address,
		input wire [7:0] tx_data,
		output reg [7:0] rx_data,
		output reg tpm_cycle,
		output reg firmware_cycle,
		input wire continue,
		input wire data_ack,
		input wire exception_ack,
		output reg address_ready,
		output reg data_ready,
		output reg [2:0] exception,
		output wire data_direction,	// 0 == read from slave, 1 == write to slave
		input wire [16:0] irq_request,
		input wire irq_tx_ready,
		output reg irq_tx_queued,

		input wire [8:0] lpc_fw_input_xfer_write_addr,
		input wire [7:0] lpc_fw_input_xfer_write_data,
		input wire lpc_fw_input_xfer_write_clk,
		input wire lpc_fw_input_xfer_write_wren,
		input wire [8:0] lpc_fw_output_xfer_read_addr,
		output wire [7:0] lpc_fw_output_xfer_read_data,
		input wire lpc_fw_output_xfer_read_clk,

		output wire [3:0] fw_idsel,
		output wire [3:0] fw_msize,

		output wire [15:0] debug_port,

		output reg [3:0] lpc_data_out,	// Must have I/O output register enabled in top level SB_IO or equivalent
		input wire [3:0] lpc_data_in,
		output reg lpc_data_direction,	// 0 == tristate (input), 1 == driven (output)
		output reg lpc_irq_out,
		input wire lpc_irq_in,
		output wire lpc_irq_direction,	// 0 == tristate (input), 1 == driven (output)

		input wire lpc_frame_n,
		input wire lpc_reset_n,
		input wire lpc_rx_clock,
		input wire lpc_tx_clock
	);

	parameter LPC_PERIPHERAL_ADDRESS_BASE_1 = 16'h0081;	// Debug port low byte
	parameter LPC_PERIPHERAL_ADDRESS_BASE_2 = 16'h0082;	// Debug port high byte
	parameter LPC_PERIPHERAL_ADDRESS_BASE_3 = 16'h00e4;	// IPMI BT port base
	parameter LPC_PERIPHERAL_ADDRESS_BASE_4 = 16'h004e;	// Config base
	parameter LPC_PERIPHERAL_ADDRESS_BASE_5 = 16'h02f8;	// UART base

	parameter LPC_CODEWORD_ISA_START = 4'b0000;
	parameter LPC_CODEWORD_FWR_START = 4'b1101;
	parameter LPC_CODEWORD_FWW_START = 4'b1110;
	parameter LPC_CODEWORD_TPM_START = 4'b0101;

	parameter LPC_CODEWORD_SYNC_READY = 4'b0000;
	parameter LPC_CODEWORD_SYNC_SWAIT = 4'b0101;
	parameter LPC_CODEWORD_SYNC_LWAIT = 4'b0110;
	parameter LPC_CODEWORD_SYNC_ERROR = 4'b1010;
	parameter LPC_CODEWORD_TURNAROUND = 4'b1111;

	parameter LPC_CYCLE_TYPE_IO = 2'b00;

	parameter LPC_RX_TRANSFER_STATE_IDLE = 0;
	parameter LPC_RX_TRANSFER_STATE_TR01 = 1;
	parameter LPC_RX_TRANSFER_STATE_TR02 = 2;
	parameter LPC_RX_TRANSFER_STATE_TR03 = 3;
	parameter LPC_RX_TRANSFER_STATE_TR04 = 4;
	parameter LPC_RX_TRANSFER_STATE_TR05 = 5;
	parameter LPC_RX_TRANSFER_STATE_TR06 = 6;
	parameter LPC_RX_TRANSFER_STATE_TR07 = 7;
	parameter LPC_RX_TRANSFER_STATE_TR08 = 8;
	parameter LPC_RX_TRANSFER_STATE_TR09 = 9;

	parameter LPC_RX_TRANSFER_STATE_FR01 = 10;
	parameter LPC_RX_TRANSFER_STATE_FR02 = 11;
	parameter LPC_RX_TRANSFER_STATE_FR03 = 12;
	parameter LPC_RX_TRANSFER_STATE_FR04 = 13;
	parameter LPC_RX_TRANSFER_STATE_FR05 = 14;
	parameter LPC_RX_TRANSFER_STATE_FR06 = 15;
	parameter LPC_RX_TRANSFER_STATE_FR07 = 16;
	parameter LPC_RX_TRANSFER_STATE_FR08 = 17;
	parameter LPC_RX_TRANSFER_STATE_FR09 = 18;
	parameter LPC_RX_TRANSFER_STATE_FR10 = 19;

	parameter LPC_TX_TRANSFER_STATE_IDLE = 0;
	parameter LPC_TX_TRANSFER_STATE_TR01 = 1;
	parameter LPC_TX_TRANSFER_STATE_TR02 = 2;
	parameter LPC_TX_TRANSFER_STATE_TR03 = 3;
	parameter LPC_TX_TRANSFER_STATE_TR04 = 4;
	parameter LPC_TX_TRANSFER_STATE_TR05 = 5;
	parameter LPC_TX_TRANSFER_STATE_TR06 = 6;
	parameter LPC_TX_TRANSFER_STATE_TR07 = 7;
	parameter LPC_TX_TRANSFER_STATE_TR08 = 8;
	parameter LPC_TX_TRANSFER_STATE_TR09 = 9;
	parameter LPC_TX_TRANSFER_STATE_TR10 = 10;
	parameter LPC_TX_TRANSFER_STATE_TR11 = 11;

	parameter LPC_TX_TRANSFER_STATE_FR01 = 12;
	parameter LPC_TX_TRANSFER_STATE_FR02 = 13;
	parameter LPC_TX_TRANSFER_STATE_FR03 = 14;
	parameter LPC_TX_TRANSFER_STATE_FR04 = 15;
	parameter LPC_TX_TRANSFER_STATE_FR05 = 16;

	parameter LPC_SERIRQ_STATE_IDLE = 0;
	parameter LPC_SERIRQ_STATE_TR01 = 1;
	parameter LPC_SERIRQ_STATE_TR02 = 2;
	parameter LPC_SERIRQ_STATE_TR03 = 3;
	parameter LPC_SERIRQ_STATE_TR04 = 4;

	reg [4:0] rx_transfer_state = 0;
	reg [4:0] tx_transfer_state = 0;
	reg [2:0] serirq_state = 0;
	reg start_tx_cycle = 0;
	reg abort_tx_cycle = 0;
	reg tx_cycle_done = 0;
	reg lpc_frame_n_prev = 1;
	reg [1:0] cycle_type = 0;
	reg cycle_direction;		// 0 == read, 1 == write
	reg [27:0] io_address = 0;	// Lower 16 bits I/O cycles only, full 28 bits used for FW cycles

	reg [3:0] fw_cycle_idsel = 0;
	reg [3:0] fw_cycle_msize = 0;

	reg [16:0] active_irq_request = 0;
	reg [3:0] irq_delay_counter = 0;
	reg [4:0] irq_frame_number = 0;
	reg lpc_irq_in_prev_1 = 1;
	reg lpc_irq_in_prev_2 = 1;
	reg lpc_irq_in_prev_3 = 1;
	reg irq_tx_ready_prev = 0;
	reg irq_quiet_mode = 0;

	reg lpc_data_direction_req = 0;
	reg lpc_irq_direction_reg = 0;

	assign address = io_address;
	assign data_direction = cycle_direction;

	assign fw_idsel = fw_cycle_idsel;
	assign fw_msize = fw_cycle_msize;

`ifdef LPC_SLAVE_DEBUG
	// Debug port
	assign debug_port[3:0] = lpc_data_in;
	assign debug_port[4] = lpc_frame_n;
	assign debug_port[5] = lpc_reset_n;
	assign debug_port[6] = cycle_direction;
	assign debug_port[7] = lpc_rx_clock;
// 	assign debug_port[11:8] = rx_transfer_state;
// 	assign debug_port[15:12] = tx_transfer_state;
// 	assign debug_port[15:8] = lpc_fw_input_xfer_read_data;
// 	assign debug_port[8] = lpc_irq_out;
	assign debug_port[10:8] = serirq_state[2:0];
	assign debug_port[12:11] = irq_delay_counter[1:0];
	assign debug_port[14:13] = irq_frame_number[1:0];
	assign debug_port[15] = lpc_irq_in;
`else
	assign debug_port = 16'h0000;
`endif

	reg tx_cycle_done_reg_rx = 0;

	reg [16:0] irq_request_reg = 0;
	reg irq_tx_ready_reg = 0;

	reg [8:0] lpc_fw_input_xfer_read_addr;
	wire [7:0] lpc_fw_input_xfer_read_data;
	reg [8:0] lpc_fw_output_xfer_write_addr;
	reg [7:0] lpc_fw_output_xfer_write_data;
	reg lpc_fw_output_xfer_write_wren;

	reg [8:0] fw_cycle_rx_nibble_counter;
	reg [7:0] fw_cycle_tx_byte_counter;

	assign lpc_irq_direction = lpc_irq_direction_reg;

	always @(posedge lpc_rx_clock) begin
		// Avoid logic glitches due to these signals crossing clock domains
		irq_request_reg <= irq_request;
		irq_tx_ready_reg <= irq_tx_ready;

		if (!lpc_reset_n) begin
			irq_quiet_mode <= 0;
			irq_tx_queued <= 0;
			lpc_irq_in_prev_1 <= 1;
			lpc_irq_in_prev_2 <= 1;
			lpc_irq_in_prev_3 <= 1;
			lpc_irq_out <= 1;
			lpc_irq_direction_reg <= 0;
			serirq_state <= LPC_SERIRQ_STATE_IDLE;
		end else begin
			case (serirq_state)
				LPC_SERIRQ_STATE_IDLE: begin
					if (irq_quiet_mode && irq_tx_ready_reg && !irq_tx_ready_prev) begin
						active_irq_request <= active_irq_request | irq_request_reg;
						irq_tx_queued <= 1;
						irq_delay_counter <= 0;

						// Initiate quiet mode transfer
						lpc_irq_out <= 0;
						lpc_irq_direction_reg <= 1;
						serirq_state <= LPC_SERIRQ_STATE_TR01;
					end else begin
						// Detect potential start signal from host
						// This can occur in either quiet or continuous mode
						if (!lpc_irq_in) begin
							if (irq_delay_counter > 2) begin
								// Latch current IRQ requests
								active_irq_request <= active_irq_request | irq_request_reg;
								serirq_state <= LPC_SERIRQ_STATE_TR02;
							end else begin
								irq_delay_counter <= irq_delay_counter + 1;
							end
						end else begin
							irq_delay_counter <= 0;
						end
					end
				end
				LPC_SERIRQ_STATE_TR01: begin
					// Tristate bus
					lpc_irq_out <= 0;
					lpc_irq_direction_reg <= 0;
					serirq_state <= LPC_SERIRQ_STATE_TR02;
				end
				LPC_SERIRQ_STATE_TR02: begin
					// Wait for completion of start signal from host
					if (lpc_irq_in) begin
						// IRQ0 needs to be asserted nearly immediately after the end of the start pulse
						// if it is to be asserted at all.  Handle IRQ0 start pulse assertion here, as the
						// heavy pipelining of the IRQ transmitter will not allow a short enough delay to
						// launch IRQ0 in the next state...
						if (active_irq_request[0]) begin
							// Drive IRQ assert for IRQ0
							lpc_irq_out <= 0;
							lpc_irq_direction_reg <= 1;
						end
						irq_delay_counter <= 1;
						irq_frame_number <= 0;
						serirq_state <= LPC_SERIRQ_STATE_TR03;
					end
				end
				LPC_SERIRQ_STATE_TR03: begin
					if (irq_frame_number < 17) begin
						if (irq_delay_counter == 0) begin
							if (active_irq_request[irq_frame_number]) begin
								// Drive IRQ assert
								lpc_irq_out <= 0;
								lpc_irq_direction_reg <= 1;
							end
						end else if (irq_delay_counter == 1) begin
							if (active_irq_request[irq_frame_number]) begin
								// Drive line back high to prepare for TAR cycle.
								// This avoids the line floating low / undetermined for an extended period of time
								// after we stop driving it; i.e. not relying solely on pullup resistor response.
								lpc_irq_out <= 1;
								lpc_irq_direction_reg <= 1;
							end
						end else begin
							lpc_irq_out <= 1;
							lpc_irq_direction_reg <= 0;
						end
					end else begin
						lpc_irq_out <= 1;
						serirq_state <= LPC_SERIRQ_STATE_TR04;
					end

					if (irq_delay_counter > 1) begin
						irq_frame_number <= irq_frame_number + 1;
						irq_delay_counter <= 0;
					end else begin
						irq_delay_counter <= irq_delay_counter + 1;
					end
				end
				LPC_SERIRQ_STATE_TR04: begin
					// Wait for rising edge
					if (!lpc_irq_in_prev_1 && lpc_irq_in) begin
						if (!lpc_irq_in_prev_3 && !lpc_irq_in_prev_2 && !lpc_irq_in_prev_1) begin
							irq_quiet_mode <= 0;
						end else begin
							irq_quiet_mode <= 1;
						end
						active_irq_request <= 0;
						serirq_state <= LPC_SERIRQ_STATE_IDLE;
					end

					// Ensure bus is tristated
					lpc_irq_direction_reg <= 0;
				end
				default: begin
					// Should never reach this state
					serirq_state <= LPC_SERIRQ_STATE_IDLE;
				end
			endcase
		end

		lpc_irq_in_prev_1 <= lpc_irq_in;
		lpc_irq_in_prev_2 <= lpc_irq_in_prev_1;
		lpc_irq_in_prev_3 <= lpc_irq_in_prev_2;
		irq_tx_ready_prev <= irq_tx_ready_reg;

		if ((serirq_state != LPC_SERIRQ_STATE_IDLE) && !irq_tx_ready_reg) begin
			irq_tx_queued <= 0;
		end
	end

	always @(posedge lpc_rx_clock) begin
		// Avoid logic glitches due to this signal crossing clock domains
		tx_cycle_done_reg_rx = tx_cycle_done;

		if (!lpc_reset_n) begin
			rx_transfer_state <= LPC_RX_TRANSFER_STATE_IDLE;

			lpc_data_direction_req <= 0;
			abort_tx_cycle <= 1;

			// Signal exception to CPU
			if (!exception_ack) begin
				exception[1] <= 1;
			end
		end else begin
			if (!lpc_frame_n) begin
				if ((rx_transfer_state == LPC_RX_TRANSFER_STATE_IDLE)
					|| (rx_transfer_state == LPC_RX_TRANSFER_STATE_TR01)) begin
					cycle_type <= 0;
					if (lpc_data_in == LPC_CODEWORD_ISA_START) begin
						tpm_cycle <= 0;
						firmware_cycle <= 0;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR01;
					end else begin
						if (lpc_data_in == LPC_CODEWORD_TPM_START) begin
							tpm_cycle <= 1;
							firmware_cycle <= 0;
							rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR01;
						end else begin
							if ((lpc_data_in == LPC_CODEWORD_FWR_START) || (lpc_data_in == LPC_CODEWORD_FWW_START)) begin
`ifdef ENABLE_FIRMWARE_MEMORY_CYCLES
								tpm_cycle <= 0;
								firmware_cycle <= 1;
								if (lpc_data_in == LPC_CODEWORD_FWW_START) begin
									cycle_direction <= 1;
								end else begin
									cycle_direction <= 0;
								end
								rx_transfer_state <= LPC_RX_TRANSFER_STATE_FR01;
`else
								tpm_cycle <= 0;
								firmware_cycle <= 0;
								rx_transfer_state <= LPC_RX_TRANSFER_STATE_IDLE;
`endif
							end else begin
								rx_transfer_state <= LPC_RX_TRANSFER_STATE_IDLE;
							end
						end
					end
				end else begin
					if (!lpc_frame_n_prev) begin
						// Host requested active cycle abort
						lpc_data_direction_req <= 0;
						abort_tx_cycle <= 1;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_IDLE;

						// Signal exception to CPU
						if (!exception_ack) begin
							exception[0] <= 1;
						end
					end
				end
			end else begin
				case (rx_transfer_state)
					LPC_RX_TRANSFER_STATE_IDLE: begin
						// Idle state
						cycle_type <= 0;
						cycle_direction <= 0;
						io_address <= 0;
						tpm_cycle <= 0;
						firmware_cycle <= 0;
						data_ready <= 0;
						address_ready <= 0;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_IDLE;

						abort_tx_cycle <= 1;
						lpc_data_direction_req <= 0;
					end
					LPC_RX_TRANSFER_STATE_TR01: begin
						// Receive cycle type and direction
						cycle_type <= lpc_data_in[3:2];
						cycle_direction <= lpc_data_in[1];

						rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR02;
					end
					LPC_RX_TRANSFER_STATE_TR02: begin
						if (cycle_type == LPC_CYCLE_TYPE_IO) begin
							// Receive I/O address -- nibble 1
							io_address[15:12] <= lpc_data_in;
							rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR03;
						end else begin
							// Cycle type not handled by this peripheral, return to idle
							rx_transfer_state <= LPC_RX_TRANSFER_STATE_IDLE;
						end

						abort_tx_cycle <= 0;
					end
					LPC_RX_TRANSFER_STATE_TR03: begin
						// Receive I/O address -- nibble 2
						io_address[11:8] <= lpc_data_in;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR04;
					end
					LPC_RX_TRANSFER_STATE_TR04: begin
						// Receive I/O address -- nibble 3
						io_address[7:4] <= lpc_data_in;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR05;
					end
					LPC_RX_TRANSFER_STATE_TR05: begin
						// Receive I/O address -- nibble 4
						io_address[3:0] <= lpc_data_in;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR06;
					end
					LPC_RX_TRANSFER_STATE_TR06: begin
						if (({io_address[15:0]} == LPC_PERIPHERAL_ADDRESS_BASE_1[15:0])			// 1 byte of address space
							|| ({io_address[15:0]} == LPC_PERIPHERAL_ADDRESS_BASE_2[15:0])		// 1 byte of address space
							|| ({io_address[15:2]} == LPC_PERIPHERAL_ADDRESS_BASE_3[15:2])		// 4 bytes of address space
							|| ({io_address[15:1]} == LPC_PERIPHERAL_ADDRESS_BASE_4[15:1])		// 2 bytes of address space
							|| ({io_address[15:3]} == LPC_PERIPHERAL_ADDRESS_BASE_5[15:3])		// 8 bytes of address space
							|| tpm_cycle) begin							// TPM cycles are always decoded
							// Address handled by this peripheral
							if (cycle_direction == 1) begin
								// Receive I/O data -- nibble 1
								rx_data[3:0] <= lpc_data_in;
								address_ready <= 1;
								rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR07;
							end else begin
								// Signal CPU that address / data are ready
								address_ready <= 1;
								if (cycle_direction == 1) begin
									data_ready <= 1;
								end

								// Start driving LAD lines
								lpc_data_direction_req <= 1;

								// Assert TX cycle start flag for > 1 clock
								start_tx_cycle <= 1;
								rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR09;
							end
						end else begin
							// Address not handled by this peripheral, return to idle
							rx_transfer_state <= LPC_RX_TRANSFER_STATE_IDLE;

							abort_tx_cycle <= 1;
							lpc_data_direction_req <= 0;
						end

					end
					LPC_RX_TRANSFER_STATE_TR07: begin
						// Receive I/O data -- nibble 2
						rx_data[7:4] <= lpc_data_in;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR08;

						// Start driving LAD lines
						lpc_data_direction_req <= 1;

						// Assert TX cycle start flag for > 1 clock
						start_tx_cycle <= 1;
					end
					LPC_RX_TRANSFER_STATE_TR08: begin
						// Signal CPU that address / data are ready
						address_ready <= 1;
						if (cycle_direction == 1) begin
							data_ready <= 1;
						end

						rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR09;
					end
					LPC_RX_TRANSFER_STATE_TR09: begin
						// Wait for TX cycle to complete
						start_tx_cycle <= 0;
						if (tx_cycle_done_reg_rx) begin
							lpc_data_direction_req <= 0;
							rx_transfer_state <= LPC_RX_TRANSFER_STATE_IDLE;
						end
					end
					LPC_RX_TRANSFER_STATE_FR01: begin
						// Receive IDSEL field
						fw_cycle_idsel <= lpc_data_in;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_FR02;
					end
					LPC_RX_TRANSFER_STATE_FR02: begin
						// Receive firmware cycle address -- nibble 1
						io_address[27:24] <= lpc_data_in;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_FR03;

						abort_tx_cycle <= 0;
					end
					LPC_RX_TRANSFER_STATE_FR03: begin
						// Receive firmware cycle address -- nibble 2
						io_address[23:20] <= lpc_data_in;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_FR04;
					end
					LPC_RX_TRANSFER_STATE_FR04: begin
						// Receive firmware cycle address -- nibble 3
						io_address[19:16] <= lpc_data_in;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_FR05;
					end
					LPC_RX_TRANSFER_STATE_FR05: begin
						// Receive firmware cycle address -- nibble 4
						io_address[15:12] <= lpc_data_in;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_FR06;
					end
					LPC_RX_TRANSFER_STATE_FR06: begin
						// Receive firmware cycle address -- nibble 5
						io_address[11:8] <= lpc_data_in;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_FR07;
					end
					LPC_RX_TRANSFER_STATE_FR07: begin
						// Receive firmware cycle address -- nibble 6
						io_address[7:4] <= lpc_data_in;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_FR08;
					end
					LPC_RX_TRANSFER_STATE_FR08: begin
						// Receive firmware cycle address -- nibble 7
						io_address[3:0] <= lpc_data_in;
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_FR09;
					end
					LPC_RX_TRANSFER_STATE_FR09: begin
						// Receive MSIZE field
						fw_cycle_msize <= lpc_data_in;

						// Handle data transfer
						if (cycle_direction == 1) begin
							rx_transfer_state <= LPC_RX_TRANSFER_STATE_FR10;

							fw_cycle_rx_nibble_counter <= 0;
						end else begin
							// Start driving LAD lines
							lpc_data_direction_req <= 1;

							// Assert TX cycle start flag for > 1 clock
							start_tx_cycle <= 1;

							rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR08;
						end
					end
					LPC_RX_TRANSFER_STATE_FR10: begin
						// Signal CPU that address is ready
						address_ready <= 1;

						// Receive data, LSN first
						if (!fw_cycle_rx_nibble_counter[0]) begin
							lpc_fw_output_xfer_write_addr <= fw_cycle_rx_nibble_counter[8:1];
							lpc_fw_output_xfer_write_data[3:0] <= lpc_data_in;
							lpc_fw_output_xfer_write_wren <= 0;
						end else begin
							lpc_fw_output_xfer_write_data[7:4] <= lpc_data_in;
							lpc_fw_output_xfer_write_wren <= 1;
						end

						case (fw_cycle_msize)
							4'b0000: begin
								if (fw_cycle_rx_nibble_counter >= 1) begin
									// Start driving LAD lines
									lpc_data_direction_req <= 1;

									// Assert TX cycle start flag for > 1 clock
									start_tx_cycle <= 1;

									rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR08;
								end
							end
							4'b0001: begin
								if (fw_cycle_rx_nibble_counter >= 2) begin
									// Start driving LAD lines
									lpc_data_direction_req <= 1;

									// Assert TX cycle start flag for > 1 clock
									start_tx_cycle <= 1;
		
									rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR08;
								end
							end
							4'b0010: begin
								if (fw_cycle_rx_nibble_counter >= 8) begin
									// Start driving LAD lines
									lpc_data_direction_req <= 1;

									// Assert TX cycle start flag for > 1 clock
									start_tx_cycle <= 1;
		
									rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR08;
								end
							end
							4'b0100: begin
								if (fw_cycle_rx_nibble_counter >= 32) begin
									// Start driving LAD lines
									lpc_data_direction_req <= 1;

									// Assert TX cycle start flag for > 1 clock
									start_tx_cycle <= 1;

									rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR08;
								end
							end
							4'b0111: begin
								if (fw_cycle_rx_nibble_counter >= 256) begin
									// Start driving LAD lines
									lpc_data_direction_req <= 1;

									// Assert TX cycle start flag for > 1 clock
									start_tx_cycle <= 1;

									rx_transfer_state <= LPC_RX_TRANSFER_STATE_TR08;
								end
							end
							default: begin
								// Disallowed size codeword
								// Abort cycle and signal exception
								rx_transfer_state <= LPC_RX_TRANSFER_STATE_IDLE;

								// Signal exception to CPU
								if (!exception_ack) begin
									exception[2] <= 1;
								end
							end
						endcase

						fw_cycle_rx_nibble_counter <= fw_cycle_rx_nibble_counter + 1;
					end
					default: begin
						// Not reachable under normal operation!
						rx_transfer_state <= LPC_RX_TRANSFER_STATE_IDLE;
					end
				endcase

				if (exception_ack) begin
					exception <= 0;
				end
			end
		end

		lpc_frame_n_prev <= lpc_frame_n;
	end

	reg start_tx_cycle_reg_tx = 0;
	reg abort_tx_cycle_reg_tx = 0;
	reg data_ack_reg_tx = 0;
	reg continue_reg_tx = 0;
	reg [7:0] lpc_tx_data_buffer;

	always @(posedge lpc_tx_clock) begin
		// Avoid logic glitches due to these signals crossing clock domains
		start_tx_cycle_reg_tx <= start_tx_cycle;
		abort_tx_cycle_reg_tx <= abort_tx_cycle;
		data_ack_reg_tx <= data_ack;
		continue_reg_tx <= continue;
		lpc_data_direction <= lpc_data_direction_req;

		if (abort_tx_cycle_reg_tx) begin
			tx_transfer_state <= LPC_TX_TRANSFER_STATE_IDLE;
		end else begin
			case (tx_transfer_state)
				LPC_TX_TRANSFER_STATE_IDLE: begin
					if (start_tx_cycle_reg_tx) begin
						if (cycle_direction == 1) begin
							tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR01;
						end else begin
							if (firmware_cycle) begin
								tx_transfer_state <= LPC_TX_TRANSFER_STATE_FR01;
							end else begin
								tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR04;
							end
						end
					end

					tx_cycle_done <= 0;

					// Drive LWAIT by default
					lpc_data_out <= LPC_CODEWORD_SYNC_LWAIT;
				end
				LPC_TX_TRANSFER_STATE_TR01: begin
					if (data_ack_reg_tx) begin
						tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR02;
					end

					// Drive LWAIT
					lpc_data_out <= LPC_CODEWORD_SYNC_LWAIT;
				end
				LPC_TX_TRANSFER_STATE_TR02: begin
					if (!data_ack_reg_tx) begin
						tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR03;
					end

					// Drive LWAIT
					lpc_data_out <= LPC_CODEWORD_SYNC_LWAIT;
				end
				LPC_TX_TRANSFER_STATE_TR03: begin
					// Drive sync
					tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR09;

					// Drive sync
					lpc_data_out <= LPC_CODEWORD_SYNC_READY;
				end
				LPC_TX_TRANSFER_STATE_TR04: begin
					if (continue_reg_tx) begin
						tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR05;
					end

					// Drive LWAIT
					lpc_data_out <= LPC_CODEWORD_SYNC_LWAIT;
				end
				LPC_TX_TRANSFER_STATE_TR05: begin
					if (!continue_reg_tx) begin
						tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR06;
					end

					// Drive LWAIT
					lpc_data_out <= LPC_CODEWORD_SYNC_LWAIT;
				end
				LPC_TX_TRANSFER_STATE_TR06: begin
					// Drive sync
					tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR07;

					// Drive sync
					lpc_data_out <= LPC_CODEWORD_SYNC_READY;
				end
				LPC_TX_TRANSFER_STATE_TR07: begin
					// Transmit first nibble of I/O data
					tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR08;

					// Transmit first nibble of I/O data
					lpc_data_out <= tx_data[3:0];
				end
				LPC_TX_TRANSFER_STATE_TR08: begin
					// Transmit second nibble of I/O data
					tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR09;

					// Transmit second nibble of I/O data
					lpc_data_out <= tx_data[7:4];
				end
				LPC_TX_TRANSFER_STATE_TR09: begin
					// Drive turn-around cycle part 1
					tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR10;

					// Drive turn-around cycle part 1
					lpc_data_out <= LPC_CODEWORD_TURNAROUND;
				end
				LPC_TX_TRANSFER_STATE_TR10: begin
					// Drive turn-around cycle part 2
					tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR11;
					tx_cycle_done <= 1;

					// Drive turn-around cycle part 2
					lpc_data_out <= LPC_CODEWORD_TURNAROUND;
				end
				LPC_TX_TRANSFER_STATE_TR11: begin
					// Assert done flag for > 1 clock, then return to idle
					tx_transfer_state <= LPC_TX_TRANSFER_STATE_IDLE;

					// Keep driving turn-around cycle during I/O direction switch
					lpc_data_out <= LPC_CODEWORD_TURNAROUND;
				end
				LPC_TX_TRANSFER_STATE_FR01: begin
					if (continue_reg_tx) begin
						tx_transfer_state <= LPC_TX_TRANSFER_STATE_FR02;
					end

					// Drive LWAIT
					lpc_data_out <= LPC_CODEWORD_SYNC_LWAIT;
				end
				LPC_TX_TRANSFER_STATE_FR02: begin
					if (!continue_reg_tx) begin
						tx_transfer_state <= LPC_TX_TRANSFER_STATE_FR03;
					end

					// Set up transfer
					lpc_fw_input_xfer_read_addr <= 0;
					fw_cycle_tx_byte_counter <= 0;

					// Drive LWAIT
					lpc_data_out <= LPC_CODEWORD_SYNC_LWAIT;
				end
				LPC_TX_TRANSFER_STATE_FR03: begin
					// Drive sync
					tx_transfer_state <= LPC_TX_TRANSFER_STATE_FR04;

					// Drive sync
					lpc_data_out <= LPC_CODEWORD_SYNC_READY;
				end
				LPC_TX_TRANSFER_STATE_FR04: begin
					// Drive first nibble in TX state machine, then set up next byte read from RAM
					tx_transfer_state <= LPC_TX_TRANSFER_STATE_FR05;

					lpc_tx_data_buffer <= lpc_fw_input_xfer_read_data;
					fw_cycle_tx_byte_counter <= fw_cycle_tx_byte_counter + 1;
					lpc_fw_input_xfer_read_addr <= fw_cycle_tx_byte_counter + 1;

					// Transmit first nibble of FW data byte
					lpc_data_out <= lpc_fw_input_xfer_read_data[3:0];
				end
				LPC_TX_TRANSFER_STATE_FR05: begin
					// Drive second nibble in TX state machine
					case (fw_cycle_msize)
						4'b0000: begin
							tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR09;
						end
						4'b0001: begin
							if (fw_cycle_tx_byte_counter >= 1) begin
								tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR09;
							end else begin
								tx_transfer_state <= LPC_TX_TRANSFER_STATE_FR04;
							end
						end
						4'b0010: begin
							if (fw_cycle_tx_byte_counter >= 4) begin
								tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR09;
							end else begin
								tx_transfer_state <= LPC_TX_TRANSFER_STATE_FR04;
							end
						end
						4'b0100: begin
							if (fw_cycle_tx_byte_counter >= 16) begin
								tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR09;
							end else begin
								tx_transfer_state <= LPC_TX_TRANSFER_STATE_FR04;
							end
						end
						4'b0111: begin
							if (fw_cycle_tx_byte_counter >= 128) begin
								tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR09;
							end else begin
								tx_transfer_state <= LPC_TX_TRANSFER_STATE_FR04;
							end
						end
						default: begin
							// Disallowed size codeword
							// Abort cycle
							tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR09;
						end
					endcase

					// Transmit second nibble of FW data byte
					lpc_data_out <= lpc_tx_data_buffer[7:4];
				end
				default: begin
					// Should never reach this point!
					// In case of a glitch into this state, drive
					// turnaround in preparation to unlock bus...
					tx_transfer_state <= LPC_TX_TRANSFER_STATE_TR09;

					// Drive LWAIT
					lpc_data_out <= LPC_CODEWORD_SYNC_LWAIT;
				end
			endcase
		end
	end

	wire [7:0] input_xfer_bram_junk_data;
	SB_RAM40_4K #(
		.INIT_0(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_1(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_2(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_3(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_4(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_5(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_6(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_7(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_8(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_9(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_A(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_B(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_C(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_D(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_E(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_F(256'h0000000000000000000000000000000000000000000000000000000000000000),
		// 512 elements deep, 8 bits wide
		.READ_MODE(1),
		.WRITE_MODE(1)
	) lpc_fw_cycle_input_xfer_bram(
		.RDATA({input_xfer_bram_junk_data[7], lpc_fw_input_xfer_read_data[7], input_xfer_bram_junk_data[6], lpc_fw_input_xfer_read_data[6], input_xfer_bram_junk_data[5], lpc_fw_input_xfer_read_data[5], input_xfer_bram_junk_data[4], lpc_fw_input_xfer_read_data[4], input_xfer_bram_junk_data[3], lpc_fw_input_xfer_read_data[3], input_xfer_bram_junk_data[2], lpc_fw_input_xfer_read_data[2], input_xfer_bram_junk_data[1], lpc_fw_input_xfer_read_data[1], input_xfer_bram_junk_data[0], lpc_fw_input_xfer_read_data[0]}),
		.RADDR({2'b00, lpc_fw_input_xfer_read_addr}),
		.WADDR({2'b00, lpc_fw_input_xfer_write_addr}),
		.MASK(16'hxxxx),
		.WDATA({1'bx, lpc_fw_input_xfer_write_data[7], 1'bx, lpc_fw_input_xfer_write_data[6], 1'bx, lpc_fw_input_xfer_write_data[5], 1'bx, lpc_fw_input_xfer_write_data[4], 1'bx, lpc_fw_input_xfer_write_data[3], 1'bx, lpc_fw_input_xfer_write_data[2], 1'bx, lpc_fw_input_xfer_write_data[1], 1'bx, lpc_fw_input_xfer_write_data[0]}),
		.RCLKE(1'b1),
		.RCLK(lpc_tx_clock),
		.RE(1'b1),
		.WCLKE(lpc_fw_input_xfer_write_wren),
		.WCLK(lpc_fw_input_xfer_write_clk),
		.WE(1'b1)
	);

	wire [7:0] output_xfer_bram_junk_data;
	SB_RAM40_4K #(
		.INIT_0(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_1(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_2(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_3(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_4(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_5(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_6(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_7(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_8(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_9(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_A(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_B(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_C(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_D(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_E(256'h0000000000000000000000000000000000000000000000000000000000000000),
		.INIT_F(256'h0000000000000000000000000000000000000000000000000000000000000000),
		// 512 elements deep, 8 bits wide
		.READ_MODE(1),
		.WRITE_MODE(1)
	) lpc_fw_cycle_output_xfer_bram(
		.RDATA({output_xfer_bram_junk_data[7], lpc_fw_output_xfer_read_data[7], output_xfer_bram_junk_data[6], lpc_fw_output_xfer_read_data[6], output_xfer_bram_junk_data[5], lpc_fw_output_xfer_read_data[5], output_xfer_bram_junk_data[4], lpc_fw_output_xfer_read_data[4], output_xfer_bram_junk_data[3], lpc_fw_output_xfer_read_data[3], output_xfer_bram_junk_data[2], lpc_fw_output_xfer_read_data[2], output_xfer_bram_junk_data[1], lpc_fw_output_xfer_read_data[1], output_xfer_bram_junk_data[0], lpc_fw_output_xfer_read_data[0]}),
		.RADDR({2'b00, lpc_fw_output_xfer_read_addr}),
		.WADDR({2'b00, lpc_fw_output_xfer_write_addr}),
		.MASK(16'hxxxx),
		.WDATA({1'bx, lpc_fw_output_xfer_write_data[7], 1'bx, lpc_fw_output_xfer_write_data[6], 1'bx, lpc_fw_output_xfer_write_data[5], 1'bx, lpc_fw_output_xfer_write_data[4], 1'bx, lpc_fw_output_xfer_write_data[3], 1'bx, lpc_fw_output_xfer_write_data[2], 1'bx, lpc_fw_output_xfer_write_data[1], 1'bx, lpc_fw_output_xfer_write_data[0]}),
		.RCLKE(1'b1),
		.RCLK(lpc_fw_output_xfer_read_clk),
		.RE(1'b1),
		.WCLKE(lpc_fw_output_xfer_write_wren),
		.WCLK(lpc_rx_clock),
		.WE(1'b1)
	);
endmodule

module lpc_master_interface(
		input wire [15:0] address,
		input wire [7:0] tx_data,
		output reg [7:0] rx_data,
		input wire tpm_cycle,
		input wire tx_start,
		input wire rx_start,
		input wire cycle_abort,
		output reg transaction_complete,
		output reg [16:0] irq_status,
		input wire irq_ack,
		input wire irq_poll,
		input wire reset,

		output reg [3:0] lpc_data_out,
		input wire [3:0] lpc_data_in,
		output reg lpc_data_direction,	// 0 == tristate (input), 1 == driven (output)
		output reg lpc_irq_out,
		input wire lpc_irq_in,
		output reg lpc_irq_direction,	// 0 == tristate (input), 1 == driven (output)

		output reg lpc_frame_n,
		output reg lpc_reset_n,
		input wire lpc_clock
	);

	parameter LPC_CODEWORD_ISA_START = 4'b0000;
	parameter LPC_CODEWORD_TPM_START = 4'b0101;

	parameter LPC_CODEWORD_SYNC_READY = 4'b0000;
	parameter LPC_CODEWORD_SYNC_SWAIT = 4'b0101;
	parameter LPC_CODEWORD_SYNC_LWAIT = 4'b0110;
	parameter LPC_CODEWORD_SYNC_ERROR = 4'b1010;
	parameter LPC_CODEWORD_TURNAROUND = 4'b1111;

	parameter LPC_CYCLE_TYPE_IO = 2'b00;

	parameter LPC_TRANSFER_STATE_IDLE = 0;
	parameter LPC_TRANSFER_STATE_TR01 = 1;
	parameter LPC_TRANSFER_STATE_TR02 = 2;
	parameter LPC_TRANSFER_STATE_TR03 = 3;
	parameter LPC_TRANSFER_STATE_TR04 = 4;
	parameter LPC_TRANSFER_STATE_TR05 = 5;
	parameter LPC_TRANSFER_STATE_TR06 = 6;
	parameter LPC_TRANSFER_STATE_TR07 = 7;
	parameter LPC_TRANSFER_STATE_TR08 = 8;
	parameter LPC_TRANSFER_STATE_TR09 = 9;
	parameter LPC_TRANSFER_STATE_TR10 = 10;
	parameter LPC_TRANSFER_STATE_TR11 = 11;
	parameter LPC_TRANSFER_STATE_TR12 = 12;
	parameter LPC_TRANSFER_STATE_TR13 = 13;
	parameter LPC_TRANSFER_STATE_TR14 = 14;
	parameter LPC_TRANSFER_STATE_TR15 = 15;
	parameter LPC_TRANSFER_STATE_TR16 = 16;
	parameter LPC_TRANSFER_STATE_TR17 = 17;
	parameter LPC_TRANSFER_STATE_TR18 = 18;
	parameter LPC_TRANSFER_STATE_TR19 = 19;
	parameter LPC_TRANSFER_STATE_TR20 = 20;
	parameter LPC_TRANSFER_STATE_TR21 = 21;

	parameter LPC_SERIRQ_STATE_IDLE = 0;
	parameter LPC_SERIRQ_STATE_TR01 = 1;
	parameter LPC_SERIRQ_STATE_TR02 = 2;
	parameter LPC_SERIRQ_STATE_TR03 = 3;
	parameter LPC_SERIRQ_STATE_TR04 = 4;

	reg [4:0] transfer_state;
	reg [2:0] serirq_state;
	reg [1:0] cycle_type = 0;
	reg cycle_direction = 0;	// 0 == read, 1 == write
	reg [15:0] io_address = 0;
	reg [7:0] io_data = 0;
	reg [3:0] timeout = 0;
	reg [3:0] irq_delay_counter = 0;
	reg [4:0] irq_frame_number = 0;
	reg irq_ack_prev = 0;
	reg irq_poll_prev = 0;

	reg reset_reg = 0;
	reg tx_start_reg = 0;
	reg rx_start_reg = 0;
	reg cycle_abort_reg = 0;
	reg irq_reset_reg = 0;
	reg irq_ack_reg = 0;
	reg irq_poll_reg = 0;

	always @(posedge lpc_clock) begin
		// Avoid logic glitches due to these signals crossing clock domains
		irq_reset_reg <= reset;
		irq_ack_reg <= irq_ack;
		irq_poll_reg <= irq_poll;

		if (irq_reset_reg) begin
			irq_status <= 0;
			lpc_irq_out <= 0;
			lpc_irq_direction <= 0;
			serirq_state <= LPC_SERIRQ_STATE_IDLE;
		end else begin
			case (serirq_state)
				LPC_SERIRQ_STATE_IDLE: begin
					lpc_irq_direction <= 0;
					if (irq_poll_reg && !irq_poll_prev) begin
						lpc_irq_out <= 0;
						lpc_irq_direction <= 1;
						irq_delay_counter <= 0;
						serirq_state <= LPC_SERIRQ_STATE_TR01;
					end
				end
				LPC_SERIRQ_STATE_TR01: begin
					// Continue driving frame start
					if (irq_delay_counter > 3) begin
						lpc_irq_out <= 1;
						serirq_state <= LPC_SERIRQ_STATE_TR02;
					end else begin
						irq_delay_counter <= irq_delay_counter + 1;
					end
				end
				LPC_SERIRQ_STATE_TR02: begin
					// Tristate line before slave TX start
					lpc_irq_direction <= 0;
					irq_delay_counter <= 0;
					irq_frame_number <= 0;
					serirq_state <= LPC_SERIRQ_STATE_TR03;
				end
				LPC_SERIRQ_STATE_TR03: begin
					if (irq_delay_counter == 0) begin
						if (!lpc_irq_in) begin
							irq_status[irq_frame_number] = 1;
						end
					end

					if (irq_delay_counter > 1) begin
						irq_frame_number <= irq_frame_number + 1;
						irq_delay_counter <= 0;
					end else begin
						irq_delay_counter <= irq_delay_counter + 1;
					end

					if (irq_frame_number > 16) begin
						lpc_irq_out <= 0;
						lpc_irq_direction <= 1;
						irq_delay_counter <= 0;
						serirq_state <= LPC_SERIRQ_STATE_TR04;
					end
				end
				LPC_SERIRQ_STATE_TR04: begin
					// Continue driving frame stop (continuous mode)
					if (irq_delay_counter > 1) begin
						lpc_irq_out <= 1;
						lpc_irq_direction <= 0;
						serirq_state <= LPC_SERIRQ_STATE_IDLE;
					end else begin
						irq_delay_counter <= irq_delay_counter + 1;
					end
				end
			endcase
		end

		if (irq_ack_reg && !irq_ack_prev) begin
			irq_status = 0;
		end
		irq_ack_prev <= irq_ack_reg;
		irq_poll_prev <= irq_poll_reg;
	end

	always @(posedge lpc_clock) begin
		// Avoid logic glitches due to these signals crossing clock domains
		reset_reg <= reset;
		tx_start_reg <= tx_start;
		rx_start_reg <= rx_start;
		cycle_abort_reg <= cycle_abort;

		if (reset_reg) begin
			lpc_frame_n <= 1;
			lpc_reset_n <= 0;
			lpc_data_direction <= 0;
			lpc_data_out <= LPC_CODEWORD_TURNAROUND;

			transaction_complete <= 0;
			transfer_state <= LPC_TRANSFER_STATE_IDLE;
		end else begin
			case (transfer_state)
				LPC_TRANSFER_STATE_IDLE: begin
					// Idle state
					cycle_type = 0;
					cycle_direction = 0;
					io_address <= 0;
					timeout <= 0;
					transaction_complete <= 0;
					if ((tx_start_reg) || (rx_start_reg)) begin
						io_address <= address;
						io_data <= tx_data;
						transfer_state <= LPC_TRANSFER_STATE_TR01;
					end else begin
						transfer_state <= LPC_TRANSFER_STATE_IDLE;
					end

					lpc_frame_n <= 1;
					lpc_data_direction <= 0;
					lpc_data_out <= LPC_CODEWORD_TURNAROUND;
				end
				LPC_TRANSFER_STATE_TR01: begin
					// Drive frame start
					if (tpm_cycle) begin
						lpc_data_out <= LPC_CODEWORD_TPM_START;
					end else begin
						lpc_data_out <= LPC_CODEWORD_ISA_START;
					end
					lpc_frame_n <= 0;
					lpc_data_direction <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR02;
				end
				LPC_TRANSFER_STATE_TR02: begin
					// Drive cycle type and direction
					lpc_data_out[3:2] <= LPC_CYCLE_TYPE_IO;
					lpc_data_out[1] <= tx_start_reg;
					lpc_data_out[0] <= 1'b0;
					lpc_frame_n <= 1;
					lpc_data_direction <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR03;
				end
				LPC_TRANSFER_STATE_TR03: begin
					// Transmit I/O address -- nibble 1
					lpc_data_out <= io_address[15:12];
					transfer_state <= LPC_TRANSFER_STATE_TR04;
				end
				LPC_TRANSFER_STATE_TR04: begin
					// Transmit I/O address -- nibble 2
					lpc_data_out <= io_address[11:8];
					transfer_state <= LPC_TRANSFER_STATE_TR05;
				end
				LPC_TRANSFER_STATE_TR05: begin
					// Transmit I/O address -- nibble 3
					lpc_data_out <= io_address[7:4];
					transfer_state <= LPC_TRANSFER_STATE_TR06;
				end
				LPC_TRANSFER_STATE_TR06: begin
					// Transmit I/O address -- nibble 4
					lpc_data_out <= io_address[3:0];

					// Set up data transfer
					if (rx_start_reg) begin
						transfer_state <= LPC_TRANSFER_STATE_TR09;
					end else begin
						transfer_state <= LPC_TRANSFER_STATE_TR07;
					end
				end
				LPC_TRANSFER_STATE_TR07: begin
					// Transmit I/O data -- nibble 1
					lpc_data_out <= io_data[3:0];
					lpc_data_direction <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR08;
				end
				LPC_TRANSFER_STATE_TR08: begin
					// Transmit I/O data -- nibble 2
					lpc_data_out <= io_data[7:4];
					lpc_data_direction <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR09;
				end
				LPC_TRANSFER_STATE_TR09: begin
					// Drive turn-around cycle part 1
					lpc_data_out <= LPC_CODEWORD_TURNAROUND;
					lpc_data_direction <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR10;
				end
				LPC_TRANSFER_STATE_TR10: begin
					// Drive turn-around cycle part 2
					lpc_data_direction <= 0;
					transfer_state <= LPC_TRANSFER_STATE_TR11;
				end
				LPC_TRANSFER_STATE_TR11: begin
					// Wait for sync ready flag from peripheral
					if (lpc_data_in == LPC_CODEWORD_SYNC_READY) begin
						if (rx_start_reg) begin
							transfer_state <= LPC_TRANSFER_STATE_TR12;
						end else begin
							transfer_state <= LPC_TRANSFER_STATE_TR14;
						end
					end else begin
						if ((cycle_abort_reg) || (timeout > 3)) begin
							transfer_state <= LPC_TRANSFER_STATE_TR16;
						end
					end

					timeout <= timeout + 1;
				end
				LPC_TRANSFER_STATE_TR12: begin
					// Receive I/O data -- nibble 1
					rx_data[3:0] = lpc_data_in;
					transfer_state <= LPC_TRANSFER_STATE_TR13;
				end
				LPC_TRANSFER_STATE_TR13: begin
					// Receive I/O data -- nibble 2
					rx_data[7:4] = lpc_data_in;
					transfer_state <= LPC_TRANSFER_STATE_TR14;
				end
				LPC_TRANSFER_STATE_TR14: begin
					// Wait for turn-around time to elapse
					lpc_data_direction <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR15;
				end
				LPC_TRANSFER_STATE_TR15: begin
					// Continue waiting for turn-around time to elapse
					transaction_complete <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR21;
				end
				LPC_TRANSFER_STATE_TR16: begin
					// Initiate abort cycle
					lpc_frame_n <= 0;
					transfer_state <= LPC_TRANSFER_STATE_TR17;
				end
				LPC_TRANSFER_STATE_TR17: begin
					// Abort cycle stage 2
					lpc_frame_n <= 0;
					transfer_state <= LPC_TRANSFER_STATE_TR18;
				end
				LPC_TRANSFER_STATE_TR18: begin
					// Abort cycle stage 3
					lpc_frame_n <= 0;
					transfer_state <= LPC_TRANSFER_STATE_TR19;
				end
				LPC_TRANSFER_STATE_TR19: begin
					// Abort cycle stage 4
					lpc_frame_n <= 0;
					lpc_data_direction <= 1;
					lpc_data_out <= LPC_CODEWORD_TURNAROUND;
					transfer_state <= LPC_TRANSFER_STATE_TR20;
				end
				LPC_TRANSFER_STATE_TR20: begin
					// Finish abort cycle and return bus to idle
					lpc_frame_n <= 1;
					lpc_data_direction <= 0;
					lpc_data_out <= LPC_CODEWORD_TURNAROUND;
					transaction_complete <= 1;
					transfer_state <= LPC_TRANSFER_STATE_TR21;
				end
				LPC_TRANSFER_STATE_TR21: begin
					// Wait for host to deassert transaction request
					if ((!tx_start_reg) && (!rx_start_reg) && (!cycle_abort_reg)) begin
						transaction_complete <= 0;
						transfer_state <= LPC_TRANSFER_STATE_IDLE;
					end
				end

				default: begin
					transfer_state <= LPC_TRANSFER_STATE_IDLE;
				end
			endcase

			lpc_reset_n <= 1;
		end
	end
endmodule
