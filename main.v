// © 2017 - 2020 Raptor Engineering, LLC
//
// Released under the terms of the AGPL v3
// See the LICENSE file for full details

`define LPC_SLAVE_DEBUG
`define RESYNTHESIZE_LPC_CLOCK
// `define ENABLE_LPC_PULLUPS
`define ENABLE_FSI_PULLUPS
`define ENABLE_SERIAL_IRQ_SUPPORT
//`define ENABLE_16550_UART_SUPPORT
`define ENABLE_FSI_MASTER_SUPPORT

// Move diagnostic ports off of 81h/82h and disable firmware memory cycle decoding
// `define SWITCH_TO_PARALLEL_OPERATION_WITH_BMC

// Block writes to GUARD partition
// Useful for debug or in situations where failed hardware
// should be automatically retried on the next IPL
`define BLOCK_GUARD_PARTITION_WRITES

// Block writes to everything except DJVPD / MVPD / CVPD / NVRAM
// This implicitly blocks GUARD partition writes
`define BLOCK_NONESSENTIAL_WRITES

// Block writes to NVRAM partition
// `define BLOCK_NVRAM_PARTITION_WRITES

`ifdef BLOCK_NONESSENTIAL_WRITES
`undef BLOCK_GUARD_PARTITION_WRITES
`endif

`ifdef SWITCH_TO_PARALLEL_OPERATION_WITH_BMC
`define DISABLE_FIRMWARE_MEMORY_CYCLES
`endif

// Main modules
`include "clock_generator.v"
`include "lpc_interface.v"
`include "spi_master.v"
`include "fsi_master.v"
`include "ipmi_bt_slave.v"
`include "flash_block_cache.v"

// Third party modules
`include "third_party/uart/uart_top.v"

module lpc_bridge_top(
		// Main 12MHz platform clock
		input wire primary_platform_clock,

		// Debug / GPIO connections
		output wire led_7,
		output wire led_6,
		output wire led_5,
		output wire led_4,
		output wire led_3,
		output wire led_2,
		output wire led_1,
		output wire led_0,

		// Debug port 2
		output wire led_15,
		output wire led_14,
		output wire led_13,
		output wire led_12,
		output wire led_11,
		output wire led_10,
		output wire led_9,
		output wire led_8,

		// LPC slave interface
		inout lpc_slave_addrdata_3,
		inout lpc_slave_addrdata_2,
		inout lpc_slave_addrdata_1,
		inout lpc_slave_addrdata_0,
		inout lpc_slave_addrdata_3_2,
		inout lpc_slave_addrdata_2_2,
		inout lpc_slave_addrdata_1_2,
		inout lpc_slave_addrdata_0_2,
`ifdef ENABLE_SERIAL_IRQ_SUPPORT
		inout lpc_slave_serirq,
		inout lpc_slave_serirq_2,
`else
		input lpc_slave_serirq,
		input lpc_slave_serirq_2,
`endif

		input wire lpc_slave_pwrdn_n,
		input wire lpc_slave_clkrun_n,
		input wire lpc_slave_frame_n,
		input wire lpc_slave_reset_n,
		input wire lpc_slave_tpm_gpio0,
		input wire lpc_slave_clock,
		input wire lpc_slave_frame_n_2,
		input wire lpc_slave_reset_n_2,

		// SPI external master interface
		output wire spi_external_master_clock,
		output wire spi_external_master_mosi,
		input wire spi_external_master_miso,
		output wire spi_external_master_ss_n,
		output wire spi_external_master_hold_n,
		output wire spi_external_master_wp_n,
		output wire spi_external_master_reset_n,

		// LPC debug mirror port
		output wire lpc_debug_mirror_clock,
		output wire lpc_debug_mirror_clock_extern_uart,
		output wire lpc_debug_mirror_clock_dbl,

`ifdef ENABLE_FSI_MASTER_SUPPORT
		output wire fsi_master_fsi_clock,
		inout fsi_master_fsi_data,
		output wire fsi_master_fsi_data_direction,
`endif

		input wire ext_ctl_enable_fsi_start,

`ifdef ENABLE_16550_UART_SUPPORT
		// Host 16550 UART
		input wire host_uart_rx,
		output wire host_uart_tx
`endif
	);

`ifndef SWITCH_TO_PARALLEL_OPERATION_WITH_BMC
	parameter DEBUG_PORT_ADDRESS_HIGH = 16'h0081;
	parameter DEBUG_PORT_ADDRESS_LOW = 16'h0082;
	parameter IPMI_BT_PORT_BASE_ADDRESS = 16'h00e4;
	parameter IPMI_BT_IRQ = 10;
	parameter HOST_UART_BASE_ADDRESS = 16'h03f8;
	parameter HOST_UART_IRQ = 4;
`else
	parameter DEBUG_PORT_ADDRESS_HIGH = 16'h0083;
	parameter DEBUG_PORT_ADDRESS_LOW = 16'h0084;
	parameter IPMI_BT_PORT_BASE_ADDRESS = 16'h00d4;
	parameter IPMI_BT_IRQ = 11;
	parameter HOST_UART_BASE_ADDRESS = 16'h02f8;
	parameter HOST_UART_IRQ = 3;
`endif

	parameter HIOMAP_MAX_CACHE_SIZE_BYTES = 4096;

`ifdef BLOCK_GUARD_PARTITION_WRITES
	parameter GUARD_PARTITION_START_ADDRESS = 28'h2c000;
	parameter GUARD_PARTITION_END_ADDRESS =   28'h5cfff;
`endif

`ifdef BLOCK_NONESSENTIAL_WRITES
	parameter VPD_PARTITION_START_ADDRESS =   28'h0e5000;
	parameter VPD_PARTITION_END_ADDRESS =     28'h204fff;
	parameter NVRAM_PARTITION_START_ADDRESS = 28'h031000;
	parameter NVRAM_PARTITION_END_ADDRESS =   28'h0c0fff;
`endif

	// General FSI register definitions for IBM CFAM slaves
	parameter IBM_CFAM_FSI_SMODE =            16'h0800;
	parameter IBM_CFAM_FSI_SISC =             16'h0802;
	parameter IBM_CFAM_FSI_SSTAT =            16'h0805;

	// Boot-related CFAM register definitions for IBM POWER9 processors
	parameter IBM_POWER9_FSI_A_SI1S =         16'h081c;
	parameter IBM_POWER9_LL_MODE_REG =        16'h0840;
	parameter IBM_POWER9_FSI2PIB_CHIPID =     16'h100a;
	parameter IBM_POWER9_FSI2PIB_INTERRUPT =  16'h100b;
	parameter IBM_POWER9_FSI2PIB_TRUE_MASK =  16'h100d;
	parameter IBM_POWER9_CBS_CS =             16'h2801;
	parameter IBM_POWER9_SBE_CTRL_STATUS =    16'h2808;
	parameter IBM_POWER9_SBE_MSG_REGISTER =   16'h2809;
	parameter IBM_POWER9_ROOT_CTRL0 =         16'h2810;
	parameter IBM_POWER9_PERV_CTRL0 =         16'h281a;
	parameter IBM_POWER9_HB_MBX5_REG =        16'h283c;
	parameter IBM_POWER9_SCRATCH_REGISTER_8 = 16'h283f;
	parameter IBM_POWER9_ROOT_CTRL8 =         16'h2918;
	parameter IBM_POWER9_ROOT_CTRL1_CLEAR =   16'h2931;

	// Up to 108MHz clock rate on SPI outgoing clock (Flash device default)
	//parameter SPI_FAST_READ_DUMMY_CLOCK_CYCLES = 10;

	// Up to 30MHz clock rate on SPI outgoing clock
	parameter SPI_FAST_READ_DUMMY_CLOCK_CYCLES = 1;

	// Fully isolate / power down host interfaces on external request
	reg drive_host_interfaces = 1;

	// LPC reset signal
	wire lpc_slave_reset_n_buffered;
	reg lpc_slave_reset_n_buffered_gated = 0;

`ifdef RESYNTHESIZE_LPC_CLOCK
	// Clean up incoming LPC clock (33.33MHz)
	wire lpc_pll_lock;
	wire lpc_slave_tx_clock_resynthesized;
	wire lpc_slave_clock_resynthesized;
`ifdef ENABLE_EXPERIMENTAL_RECLOCKING_PLL_DOUBLING
	// NOTE:
	// The iCE40 PLL is very poorly documented in the
	// external feedback path.  Discussion follows:
	//
	// In the iCE40 Ultra Plus documentation, it is mentioned
	// that EXTERNAL_DIVIDE_FACTOR actually influences the clock
	// synthesis by acting as a multiplier for GENCLK.
	// Therefore, to get a doubled frequency along with the phase
	// aligned / duty cycle corrected original frequency, the PLL
	// needs to be set for 1:1 clock synthesis and the feedback
	// path division factor needs to be set to 2.  Feeding the
	// GENCLK_HALF signal back into the feedback pin then seems
	// to work as intended -- the original clock is aligned, and
	// a doubled clock is available on port B via the GENCLK route.
	//
	// Unfortunately, even with this inferred information, the PLL
	// is not exactly phase matching its output with the input.
	// Since this is apparently a little used feature of the iCE40
	// PLLs, it's probably not worth debugging in this context.
	// Larger devices like the ECP5 have four PLLs to work with,
	// making the use of a second PLL to generate the SPI clock
	// more reasonable.
	wire lpc_slave_clock_doubled;
	SB_PLL40_2F_PAD #(
		.FEEDBACK_PATH("EXTERNAL"),
		.PLLOUT_SELECT_PORTA("GENCLK_HALF"),
		.PLLOUT_SELECT_PORTB("GENCLK"),
		.EXTERNAL_DIVIDE_FACTOR(2'b10),
		.SHIFTREG_DIV_MODE(1'b0),
		.DIVR(4'b0000),
		.DIVF(7'b0000000),
		.DIVQ(3'b100),
		.FILTER_RANGE(3'b011)
	) lpc_slave_pll (
		.LOCK(lpc_pll_lock),
		.EXTFEEDBACK(lpc_slave_clock_resynthesized),
		.RESETB(lpc_slave_reset_n_buffered_gated),
		.BYPASS(1'b0),
		.PACKAGEPIN(lpc_slave_clock),
		.PLLOUTGLOBALA(lpc_slave_clock_resynthesized),
		.PLLOUTGLOBALB(lpc_slave_clock_doubled)
	);
`else
	SB_PLL40_2F_PAD #(
		.FEEDBACK_PATH("PHASE_AND_DELAY"),
		.PLLOUT_SELECT_PORTA("SHIFTREG_0deg"),
		.PLLOUT_SELECT_PORTB("SHIFTREG_0deg"),
		.SHIFTREG_DIV_MODE(1'b0),
		.DIVR(4'b0000),
		.DIVF(7'b0000000),
		.DIVQ(3'b100),
		.FILTER_RANGE(3'b011)
	) lpc_slave_pll (
		.LOCK(lpc_pll_lock),
		.RESETB(lpc_slave_reset_n_buffered_gated),
		.BYPASS(1'b0),
		.PACKAGEPIN(lpc_slave_clock),
		.PLLOUTGLOBALA(lpc_slave_clock_resynthesized)
	);
`endif
	assign lpc_slave_tx_clock_resynthesized = lpc_slave_clock_resynthesized;
`else
	wire lpc_slave_clock_buffered;
	wire lpc_slave_tx_clock_resynthesized;
	wire lpc_slave_clock_resynthesized;
	assign lpc_slave_tx_clock_resynthesized = lpc_slave_clock_buffered;
	assign lpc_slave_clock_resynthesized = lpc_slave_clock_buffered;
`endif

	// Not enough PLLs on the iCE40 to actually double this clock
	assign lpc_debug_mirror_clock_dbl = lpc_slave_clock_resynthesized;

	// Debug port
`ifdef LPC_SLAVE_DEBUG
	wire [15:0] debug_leds;
`else
	reg [15:0] debug_leds = 0;
`endif

	assign led_15 = debug_leds[15];
	assign led_14 = debug_leds[14];
	assign led_13 = debug_leds[13];
	assign led_12 = debug_leds[12];
	assign led_11 = debug_leds[11];
	assign led_10 = debug_leds[10];
	assign led_9 = debug_leds[9];
	assign led_8 = debug_leds[8];
	assign led_7 = debug_leds[7];
	assign led_6 = debug_leds[6];
	assign led_5 = debug_leds[5];
	assign led_4 = debug_leds[4];
	assign led_3 = debug_leds[3];
	assign led_2 = debug_leds[2];
	assign led_1 = debug_leds[1];
	assign led_0 = debug_leds[0];

	// LPC debug mirror port
	assign lpc_debug_mirror_clock = lpc_slave_clock_resynthesized;
	assign lpc_debug_mirror_clock_extern_uart = lpc_slave_clock_resynthesized;

	// Instantiate clock generator
	wire spi_core_pll_lock;
	wire platform_pll_unstable;
	wire slow_1843khz_uart_clock;
	wire slow_175khz_platform_clock;
	wire slow_183hz_platform_clock;
	wire slow_11hz_platform_clock;

	wire sequencer_clock;
	wire spi_core_clock;

`ifdef RESYNTHESIZE_LPC_CLOCK
	assign platform_pll_unstable = ~lpc_pll_lock;
`else
	assign platform_pll_unstable = 0;
`endif

	clock_generator clock_generator(
		// Main 12MHz platform clock
		.platform_clock(primary_platform_clock),
		.spi_core_pll_lock(spi_core_pll_lock),
		.slow_1843khz_uart_clock(slow_1843khz_uart_clock),
		.slow_175khz_platform_clock(slow_175khz_platform_clock),
		.slow_183hz_platform_clock(slow_183hz_platform_clock),
		.slow_11hz_platform_clock(slow_11hz_platform_clock),

		// Main 33MHz LPC clock
		.lpc_clock(lpc_slave_clock_resynthesized),
		.sequencer_clock(sequencer_clock),
		.spi_core_clock(spi_core_clock),
		.reset_lpc_derived_plls(~lpc_pll_lock)
	);

	// Generate power-on reset signals
	wire fpga_power_on_reset;
	reg fpga_power_on_reset_internal = 1;
	reg [3:0] fpga_power_on_reset_counter = 0;
	assign fpga_power_on_reset = fpga_power_on_reset_internal | platform_pll_unstable;
	always @(posedge primary_platform_clock or posedge platform_pll_unstable) begin
		if (platform_pll_unstable) begin
			fpga_power_on_reset_internal <= 1;
			fpga_power_on_reset_counter <= 0;
		end else begin
			if (fpga_power_on_reset_counter < 10) begin
				fpga_power_on_reset_counter <= fpga_power_on_reset_counter + 1;
				fpga_power_on_reset_internal <= 1;
			end else begin
				fpga_power_on_reset_internal <= 0;
			end
		end
	end

	// Instantiate LPC slave interface
	wire [27:0] lpc_slave_address;
	wire [7:0] lpc_slave_rx_data;
	reg [7:0] lpc_slave_tx_data;
	wire lpc_slave_tpm_cycle;
	wire lpc_slave_firmware_cycle;
	reg lpc_slave_continue = 0;
	reg lpc_slave_data_ack = 0;
	reg lpc_slave_exception_ack = 0;
	wire lpc_slave_address_ready;
	wire lpc_slave_data_ready;
	wire [2:0] lpc_slave_exception;
	wire lpc_slave_cycle_direction;
	reg [16:0] lpc_slave_irq_request = 0;
	reg lpc_slave_irq_tx_ready = 0;

`ifdef ENABLE_SERIAL_IRQ_SUPPORT
	wire lpc_slave_irq_tx_queued;
`endif

	reg [8:0] lpc_fw_input_xfer_write_addr;
	reg [7:0] lpc_fw_input_xfer_write_data;
	reg lpc_fw_input_xfer_write_wren;
	reg [8:0] lpc_fw_output_xfer_read_addr;
	reg [8:0] lpc_fw_output_xfer_read_addr_prev;
	wire [7:0] lpc_fw_output_xfer_read_data;

	wire [3:0] lpc_slave_fw_idsel;
	wire [3:0] lpc_slave_fw_msize;

`ifdef LPC_SLAVE_DEBUG
	wire [15:0] lpc_slave_debug_port;
`endif

	wire lpc_slave_data_direction;
	wire lpc_slave_irq_direction;
	wire [3:0] lpc_slave_data_out;
	wire [3:0] lpc_slave_data_in;

`ifdef ENABLE_SERIAL_IRQ_SUPPORT
	wire lpc_slave_irq_out;
	wire lpc_slave_irq_in;
`endif

	wire lpc_slave_frame_n_buffered;

	lpc_slave_interface #(
		.LPC_PERIPHERAL_ADDRESS_BASE_1(DEBUG_PORT_ADDRESS_HIGH),
		.LPC_PERIPHERAL_ADDRESS_BASE_2(DEBUG_PORT_ADDRESS_LOW),
		.LPC_PERIPHERAL_ADDRESS_BASE_3(IPMI_BT_PORT_BASE_ADDRESS),
		.LPC_PERIPHERAL_ADDRESS_BASE_5(HOST_UART_BASE_ADDRESS)
	) lpc_slave_interface (
		.address(lpc_slave_address),
		.tx_data(lpc_slave_tx_data),
		.rx_data(lpc_slave_rx_data),
		.tpm_cycle(lpc_slave_tpm_cycle),
		.firmware_cycle(lpc_slave_firmware_cycle),
		.continue(lpc_slave_continue),
		.data_ack(lpc_slave_data_ack),
		.exception_ack(lpc_slave_exception_ack),
		.address_ready(lpc_slave_address_ready),
		.data_ready(lpc_slave_data_ready),
		.exception(lpc_slave_exception),
		.data_direction(lpc_slave_cycle_direction),

`ifdef ENABLE_SERIAL_IRQ_SUPPORT
		.irq_request(lpc_slave_irq_request),
		.irq_tx_ready(lpc_slave_irq_tx_ready),
		.irq_tx_queued(lpc_slave_irq_tx_queued),
`endif

		.lpc_fw_input_xfer_write_addr(lpc_fw_input_xfer_write_addr),
		.lpc_fw_input_xfer_write_data(lpc_fw_input_xfer_write_data),
		.lpc_fw_input_xfer_write_clk(sequencer_clock),
		.lpc_fw_input_xfer_write_wren(lpc_fw_input_xfer_write_wren),
		.lpc_fw_output_xfer_read_addr(lpc_fw_output_xfer_read_addr),
		.lpc_fw_output_xfer_read_data(lpc_fw_output_xfer_read_data),
		.lpc_fw_output_xfer_read_clk(sequencer_clock),

		.fw_idsel(lpc_slave_fw_idsel),
		.fw_msize(lpc_slave_fw_msize),

`ifdef LPC_SLAVE_DEBUG
		.debug_port(lpc_slave_debug_port),
`endif

		.lpc_data_out(lpc_slave_data_out),
		.lpc_data_in(lpc_slave_data_in),
		.lpc_data_direction(lpc_slave_data_direction),

`ifdef ENABLE_SERIAL_IRQ_SUPPORT
		.lpc_irq_out(lpc_slave_irq_out),
		.lpc_irq_in(lpc_slave_irq_in),
		.lpc_irq_direction(lpc_slave_irq_direction),
`endif

		.lpc_frame_n(lpc_slave_frame_n_buffered),
		.lpc_reset_n(lpc_slave_reset_n_buffered_gated),
		.lpc_rx_clock(lpc_slave_clock_resynthesized),
		.lpc_tx_clock(lpc_slave_tx_clock_resynthesized)
	);

`ifdef LPC_SLAVE_DEBUG
	assign debug_leds = lpc_slave_debug_port;
`endif

	// Control bidirectional interface
	// Work around yosys / arachne-pnr issues by instantiating each SB_IO manually
	SB_IO #(
		.PIN_TYPE(6'b100100),
`ifdef ENABLE_LPC_PULLUPS
		.PULLUP(1'b1),
`else
		.PULLUP(1'b0),
`endif
		.NEG_TRIGGER(1'b0)
	) lpc_slave_addrdata_3_io (
		.PACKAGE_PIN(lpc_slave_addrdata_3),
		.OUTPUT_ENABLE(lpc_slave_data_direction & drive_host_interfaces),
		.OUTPUT_CLK(lpc_slave_tx_clock_resynthesized),
		.D_OUT_0(lpc_slave_data_out[3]),
		.INPUT_CLK(lpc_slave_clock_resynthesized),
		.D_IN_0(lpc_slave_data_in[3]),
		.CLOCK_ENABLE(1'b1)
	);
	SB_IO #(
		.PIN_TYPE(6'b100100),
`ifdef ENABLE_LPC_PULLUPS
		.PULLUP(1'b1),
`else
		.PULLUP(1'b0),
`endif
		.NEG_TRIGGER(1'b0)
	) lpc_slave_addrdata_2_io (
		.PACKAGE_PIN(lpc_slave_addrdata_2),
		.OUTPUT_ENABLE(lpc_slave_data_direction & drive_host_interfaces),
		.OUTPUT_CLK(lpc_slave_tx_clock_resynthesized),
		.D_OUT_0(lpc_slave_data_out[2]),
		.INPUT_CLK(lpc_slave_clock_resynthesized),
		.D_IN_0(lpc_slave_data_in[2]),
		.CLOCK_ENABLE(1'b1)
	);
	SB_IO #(
		.PIN_TYPE(6'b100100),
`ifdef ENABLE_LPC_PULLUPS
		.PULLUP(1'b1),
`else
		.PULLUP(1'b0),
`endif
		.NEG_TRIGGER(1'b0)
	) lpc_slave_addrdata_1_io (
		.PACKAGE_PIN(lpc_slave_addrdata_1),
		.OUTPUT_ENABLE(lpc_slave_data_direction & drive_host_interfaces),
		.OUTPUT_CLK(lpc_slave_tx_clock_resynthesized),
		.D_OUT_0(lpc_slave_data_out[1]),
		.INPUT_CLK(lpc_slave_clock_resynthesized),
		.D_IN_0(lpc_slave_data_in[1]),
		.CLOCK_ENABLE(1'b1)
	);
	SB_IO #(
		.PIN_TYPE(6'b100100),
`ifdef ENABLE_LPC_PULLUPS
		.PULLUP(1'b1),
`else
		.PULLUP(1'b0),
`endif
		.NEG_TRIGGER(1'b0)
	) lpc_slave_addrdata_0_io (
		.PACKAGE_PIN(lpc_slave_addrdata_0),
		.OUTPUT_ENABLE(lpc_slave_data_direction & drive_host_interfaces),
		.OUTPUT_CLK(lpc_slave_tx_clock_resynthesized),
		.D_OUT_0(lpc_slave_data_out[0]),
		.INPUT_CLK(lpc_slave_clock_resynthesized),
		.D_IN_0(lpc_slave_data_in[0]),
		.CLOCK_ENABLE(1'b1)
	);
`ifdef ENABLE_SERIAL_IRQ_SUPPORT
	SB_IO #(
		.PIN_TYPE(6'b110101),
`ifdef ENABLE_LPC_PULLUPS
		.PULLUP(1'b1),
`else
		.PULLUP(1'b0),
`endif
		.NEG_TRIGGER(1'b0)
	) lpc_slave_serirq_io (
		.PACKAGE_PIN(lpc_slave_serirq),
		.OUTPUT_ENABLE(lpc_slave_irq_direction & drive_host_interfaces),
		.INPUT_CLK(lpc_slave_clock_resynthesized),
		.OUTPUT_CLK(lpc_slave_tx_clock_resynthesized),
		.D_OUT_0(lpc_slave_irq_out),
		.D_IN_0(lpc_slave_irq_in),
		.CLOCK_ENABLE(1'b1)
	);
`else
	SB_IO #(
		.PIN_TYPE(6'b000001),
		.PULLUP(1'b0)
	) lpc_slave_serirq_io (
		.PACKAGE_PIN(lpc_slave_serirq),
		.OUTPUT_ENABLE(1'b0)
	);
`endif
	SB_IO #(
		.PIN_TYPE(6'b100101),
`ifdef ENABLE_LPC_PULLUPS
		.PULLUP(1'b1),
`else
		.PULLUP(1'b0),
`endif
		.NEG_TRIGGER(1'b0)
	) lpc_slave_addrdata_3_2_io (
		.PACKAGE_PIN(lpc_slave_addrdata_3_2),
		.OUTPUT_ENABLE(lpc_slave_data_direction & drive_host_interfaces),
		.OUTPUT_CLK(lpc_slave_tx_clock_resynthesized),
		.D_OUT_0(lpc_slave_data_out[3]),
		.CLOCK_ENABLE(1'b1)
	);
	SB_IO #(
		.PIN_TYPE(6'b100101),
`ifdef ENABLE_LPC_PULLUPS
		.PULLUP(1'b1),
`else
		.PULLUP(1'b0),
`endif
		.NEG_TRIGGER(1'b0)
	) lpc_slave_addrdata_2_2_io (
		.PACKAGE_PIN(lpc_slave_addrdata_2_2),
		.OUTPUT_ENABLE(lpc_slave_data_direction & drive_host_interfaces),
		.OUTPUT_CLK(lpc_slave_tx_clock_resynthesized),
		.D_OUT_0(lpc_slave_data_out[2]),
		.CLOCK_ENABLE(1'b1)
	);
	SB_IO #(
		.PIN_TYPE(6'b100101),
`ifdef ENABLE_LPC_PULLUPS
		.PULLUP(1'b1),
`else
		.PULLUP(1'b0),
`endif
		.NEG_TRIGGER(1'b0)
	) lpc_slave_addrdata_1_2_io (
		.PACKAGE_PIN(lpc_slave_addrdata_1_2),
		.OUTPUT_ENABLE(lpc_slave_data_direction & drive_host_interfaces),
		.OUTPUT_CLK(lpc_slave_tx_clock_resynthesized),
		.D_OUT_0(lpc_slave_data_out[1]),
		.CLOCK_ENABLE(1'b1)
	);
	SB_IO #(
		.PIN_TYPE(6'b100101),
`ifdef ENABLE_LPC_PULLUPS
		.PULLUP(1'b1),
`else
		.PULLUP(1'b0),
`endif
		.NEG_TRIGGER(1'b0)
	) lpc_slave_addrdata_0_2_io (
		.PACKAGE_PIN(lpc_slave_addrdata_0_2),
		.OUTPUT_ENABLE(lpc_slave_data_direction & drive_host_interfaces),
		.OUTPUT_CLK(lpc_slave_tx_clock_resynthesized),
		.D_OUT_0(lpc_slave_data_out[0]),
		.CLOCK_ENABLE(1'b1)
	);
`ifdef ENABLE_SERIAL_IRQ_SUPPORT
	SB_IO #(
		.PIN_TYPE(6'b100101),
`ifdef ENABLE_LPC_PULLUPS
		.PULLUP(1'b1),
`else
		.PULLUP(1'b0),
`endif
		.NEG_TRIGGER(1'b0)
	) lpc_slave_serirq_2_io (
		.PACKAGE_PIN(lpc_slave_serirq_2),
		.OUTPUT_ENABLE(lpc_slave_irq_direction & drive_host_interfaces),
		.OUTPUT_CLK(lpc_slave_tx_clock_resynthesized),
		.D_OUT_0(lpc_slave_irq_out),
		.CLOCK_ENABLE(1'b1)
	);
`else
	SB_IO #(
		.PIN_TYPE(6'b000001),
		.PULLUP(1'b0)
	) lpc_slave_serirq_2_io (
		.PACKAGE_PIN(lpc_slave_serirq_2),
		.OUTPUT_ENABLE(1'b0)
	);
`endif
`ifndef RESYNTHESIZE_LPC_CLOCK
	SB_PLL40_PAD #(
	) lpc_slave_pll (
		.RESETB(1'b1),
		.BYPASS(1'b1),
		.PACKAGEPIN(lpc_slave_clock),
		.PLLOUTGLOBAL(lpc_slave_clock_buffered)
	);
`endif
	SB_IO #(
		.PIN_TYPE(6'b000000),
`ifdef ENABLE_LPC_PULLUPS
		.PULLUP(1'b1),
`else
		.PULLUP(1'b0),
`endif
		.NEG_TRIGGER(1'b0)
	) lpc_slave_frame_n_io (
		.PACKAGE_PIN(lpc_slave_frame_n),
		.OUTPUT_ENABLE(1'b0),
		.INPUT_CLK(lpc_slave_clock_resynthesized),
		.D_IN_0(lpc_slave_frame_n_buffered),
		.CLOCK_ENABLE(1'b1)
	);
	SB_IO #(
		.PIN_TYPE(6'b000000),
`ifdef ENABLE_LPC_PULLUPS
		.PULLUP(1'b1),
`else
		.PULLUP(1'b0),
`endif
		.NEG_TRIGGER(1'b0)
	) lpc_slave_reset_n_io (
		.PACKAGE_PIN(lpc_slave_reset_n),
		.OUTPUT_ENABLE(1'b0),
		.INPUT_CLK(primary_platform_clock),
		.D_IN_0(lpc_slave_reset_n_buffered),
		.CLOCK_ENABLE(1'b1)
	);

	// Instantiate SPI external master interface
	wire [31:0] spi_external_master_rx_data;
	reg [31:0] spi_external_master_tx_data = 0;
	reg [7:0] spi_external_master_dummy_cycle_count = 0;
	reg spi_external_master_cycle_start = 0;
	reg spi_external_master_hold_ss_active = 0;
	wire spi_external_master_transaction_complete;

	reg spi_external_master_passthrough = 0;
	reg spi_external_master_reset_n_buffered = 0;

	reg spi_external_master_qspi_mode_active = 0;
	reg spi_external_master_qspi_transfer_mode = 0;
	reg spi_external_master_qspi_transfer_direction = 0;
	wire spi_external_master_data_direction;
	wire spi_external_master_quad_mode_pin_enable;

	wire spi_external_master_clock_int;
	wire spi_external_master_mosi_int;
	wire spi_external_master_miso_int;
	wire spi_external_master_ss_n_int;
	wire spi_external_master_wp_n_int;
	wire spi_external_master_hold_n_int;

	wire spi_external_master_mosi_buffered;
	wire spi_external_master_miso_buffered;
	wire spi_external_master_wp_n_buffered;
	wire spi_external_master_hold_n_buffered;

	spi_master_interface_quad spi_master_interface_quad(
		.platform_clock(spi_core_clock),
		.reset(fpga_power_on_reset || (~lpc_slave_reset_n_buffered_gated)),
		.tx_data(spi_external_master_tx_data),
		.rx_data(spi_external_master_rx_data),
		.dummy_cycle_count(spi_external_master_dummy_cycle_count),
		.qspi_mode_active(spi_external_master_qspi_mode_active),
		.qspi_transfer_mode(spi_external_master_qspi_transfer_mode),
		.qspi_transfer_direction(spi_external_master_qspi_transfer_direction),
		.hold_ss_active(spi_external_master_hold_ss_active),
		.cycle_start(spi_external_master_cycle_start),
		.transaction_complete(spi_external_master_transaction_complete),

		.spi_clock(spi_external_master_clock_int),
		.spi_d0_out(spi_external_master_mosi_int),
		.spi_d0_in(spi_external_master_mosi_buffered),
		.spi_d1_out(spi_external_master_miso_int),
		.spi_d1_in(spi_external_master_miso_buffered),
		.spi_d2_out(spi_external_master_wp_n_int),
		.spi_d2_in(spi_external_master_wp_n_buffered),
		.spi_d3_out(spi_external_master_hold_n_int),
		.spi_d3_in(spi_external_master_hold_n_buffered),
		.spi_ss_n(spi_external_master_ss_n_int),
		.spi_data_direction(spi_external_master_data_direction),
		.spi_quad_mode_pin_enable(spi_external_master_quad_mode_pin_enable)
	);

	// Direct output pin assignments
	assign spi_external_master_clock = spi_external_master_clock_int;
	assign spi_external_master_ss_n = spi_external_master_ss_n_int;
	assign spi_external_master_reset_n = spi_external_master_reset_n_buffered;

	// Bidirectional I/O pin assignments
	SB_IO #(
		.PIN_TYPE(6'b101001),
		.PULLUP(1'b0)
	) spi_external_master_mosi_io (
		.PACKAGE_PIN(spi_external_master_mosi),
		.OUTPUT_ENABLE(!spi_external_master_quad_mode_pin_enable || (spi_external_master_data_direction & drive_host_interfaces)),
		.D_OUT_0(spi_external_master_mosi_int),
		.D_IN_0(spi_external_master_mosi_buffered)
	);

	SB_IO #(
		.PIN_TYPE(6'b101001),
		.PULLUP(1'b0)
	) spi_external_master_miso_io (
		.PACKAGE_PIN(spi_external_master_miso),
		.OUTPUT_ENABLE(spi_external_master_quad_mode_pin_enable && (spi_external_master_data_direction & drive_host_interfaces)),
		.D_OUT_0(spi_external_master_miso_int),
		.D_IN_0(spi_external_master_miso_buffered)
	);

	SB_IO #(
		.PIN_TYPE(6'b101001),
		.PULLUP(1'b0)
	) spi_external_master_wp_n_io (
		.PACKAGE_PIN(spi_external_master_wp_n),
		.OUTPUT_ENABLE(!spi_external_master_quad_mode_pin_enable || (spi_external_master_data_direction & drive_host_interfaces)),
		.D_OUT_0(spi_external_master_wp_n_int),
		.D_IN_0(spi_external_master_wp_n_buffered)
	);

	SB_IO #(
		.PIN_TYPE(6'b101001),
		.PULLUP(1'b0)
	) spi_external_master_hold_n_io (
		.PACKAGE_PIN(spi_external_master_hold_n),
		.OUTPUT_ENABLE(!spi_external_master_quad_mode_pin_enable || (spi_external_master_data_direction & drive_host_interfaces)),
		.D_OUT_0(spi_external_master_hold_n_int),
		.D_IN_0(spi_external_master_hold_n_buffered)
	);

`ifdef ENABLE_FSI_MASTER_SUPPORT
	// Instantiate FSI master interface
	reg [1:0] fsi_master_slave_id = 0;
	wire [20:0] fsi_master_address;
	reg [31:0] fsi_master_tx_data = 0;
	wire [31:0] fsi_master_rx_data;
	reg [1:0] fsi_master_data_length = 0;
	reg fsi_master_data_direction = 0;
	reg fsi_master_start_cycle = 0;
	wire fsi_master_cycle_complete;
	wire [2:0] fsi_master_cycle_error;
	reg [1:0] fsi_master_enhanced_error_recovery = 0;
	reg [7:0] fsi_master_internal_cmd_issue_delay = 0;
	reg fsi_master_enable_ipoll = 0;
	wire fsi_master_core_clock;

	wire fsi_master_fsi_data_out;
	wire fsi_master_fsi_data_in;
	wire fsi_master_fsi_clock_out;
	wire fsi_master_ipoll_error;

	wire [7:0] fsi_master_debug_port;

	// Generate 750KHz FSI clock
	// NOTE: This could be configured to run up to 166MHz according to the specification
	reg [3:0] fsi_clock_divider = 0;
	always @(posedge primary_platform_clock) begin
		fsi_clock_divider <= fsi_clock_divider + 1;
	end
	assign fsi_master_core_clock = fsi_clock_divider[3];

	fsi_master_interface fsi_master_interface(
		.slave_id(fsi_master_slave_id),
		.address(fsi_master_address),
		.tx_data(fsi_master_tx_data),
		.rx_data(fsi_master_rx_data),
		.enable_relative_address(1'b1),
		.enable_extended_address_mode(1'b0),
		.enable_crc_protection(1'b1),
		.data_length(fsi_master_data_length),
		.data_direction(fsi_master_data_direction),
		.start_cycle(fsi_master_start_cycle),
		.cycle_complete(fsi_master_cycle_complete),
		.cycle_error(fsi_master_cycle_error),
		.enhanced_error_recovery(fsi_master_enhanced_error_recovery),
		.internal_cmd_issue_delay(fsi_master_internal_cmd_issue_delay),
		.ipoll_enable_slave_id(4'b0001),
		.enable_ipoll(fsi_master_enable_ipoll),
		.ipoll_error(fsi_master_ipoll_error),

		.fsi_data_out(fsi_master_fsi_data_out),
		.fsi_data_in(fsi_master_fsi_data_in),
		.fsi_data_direction(fsi_master_fsi_data_direction),
		.fsi_clock_out(fsi_master_fsi_clock_out),

		.debug_port(fsi_master_debug_port),

		.peripheral_reset(fpga_power_on_reset),
		.peripheral_clock(fsi_master_core_clock)
	);

	// Direct output pin assignment
	assign fsi_master_fsi_clock = ~fsi_master_fsi_clock_out;

	// Bidirectional I/O pin assignment
	SB_IO #(
		.PIN_TYPE(6'b110100),
`ifdef ENABLE_FSI_PULLUPS
		.PULLUP(1'b1),
`else
		.PULLUP(1'b0),
`endif
		.NEG_TRIGGER(1'b0)
	) fsi_master_data_io (
		.PACKAGE_PIN(fsi_master_fsi_data),
		.OUTPUT_ENABLE(fsi_master_fsi_data_direction & drive_host_interfaces),
		.INPUT_CLK(fsi_master_fsi_clock_out),
		.OUTPUT_CLK(fsi_master_fsi_clock_out),
		.D_OUT_0(fsi_master_fsi_data_out),
		.D_IN_0(fsi_master_fsi_data_in),
		.CLOCK_ENABLE(1'b1)
	);

	// CFAM address translation
	reg [15:0] fsi_master_cfam_address = 0;
	assign fsi_master_address = (fsi_master_cfam_address & 16'hfc00) | ((fsi_master_cfam_address & 16'h03ff) << 2);
`endif

	// Instantiate IPMI BT interface
	reg [8:0] ipmi_bt_input_xfer_write_addr_reg;
	reg [7:0] ipmi_bt_input_xfer_write_data_reg;
	reg ipmi_bt_input_xfer_write_wren_reg = 0;
	reg [8:0] ipmi_bt_output_xfer_read_addr;
	wire [7:0] ipmi_bt_output_xfer_read_data;

	reg ipmi_bt_bmc_to_host_ctl_sms_ack_reg = 0;
	reg ipmi_bt_bmc_to_host_ctl_attn_ack_reg = 0;
	reg ipmi_bt_host_to_bmc_ctl_attn_req_reg = 0;
	reg ipmi_bt_irq_ack_reg = 0;
	reg ipmi_bt_irq_bmc_reset_reg = 0;

	wire ipmi_bt_bmc_to_host_ctl_sms_req;
	wire ipmi_bt_bmc_to_host_ctl_attn_req;
	wire ipmi_bt_bmc_to_host_ctl_sms_ack_cont;
	wire ipmi_bt_bmc_to_host_ctl_attn_ack_cont;
	wire ipmi_bt_host_to_bmc_ctl_attn_req_cont;
	wire ipmi_bt_irq_ack_cont;
	wire ipmi_bt_irq_bmc_reset_cont;

	reg ipmi_bt_host_to_bmc_ctl_h_busy_reg = 0;
	wire ipmi_bt_bmc_to_host_ctl_b_busy;
	reg ipmi_bt_irq_enable = 0;
	wire ipmi_bt_irq_req;

	wire ipmi_bt_hiomap_new_window_req;
	reg ipmi_bt_hiomap_new_window_ack = 0;
	wire [27:0] ipmi_bt_hiomap_active_window_start_address;
	wire [27:0] ipmi_bt_hiomap_active_window_size_bytes;
	wire ipmi_bt_hiomap_window_type_write;

	wire ipmi_bt_hiomap_flush_req;
	reg ipmi_bt_hiomap_flush_ack = 0;
	wire [27:0] ipmi_bt_hiomap_flush_sector_count;
	wire ipmi_bt_hiomap_flush_type_erase;
	wire [27:0] ipmi_bt_hiomap_erase_address_offset;
	wire [27:0] ipmi_bt_hiomap_erase_sector_count;

	wire [27:0] ipmi_bt_hiomap_erase_address_step;
	wire [27:0] ipmi_bt_hiomap_program_address_step;
	wire [27:0] ipmi_bt_hiomap_def_win_size_bytes;

	wire [7:0] ipmi_bt_slave_interface_debug_port;

	ipmi_bt_slave_interface ipmi_bt_slave_interface(
		.input_xfer_write_addr(ipmi_bt_input_xfer_write_addr_reg),
		.input_xfer_write_data(ipmi_bt_input_xfer_write_data_reg),
		.input_xfer_write_clk(sequencer_clock),
		.input_xfer_write_wren(ipmi_bt_input_xfer_write_wren_reg),
		.output_xfer_read_addr(ipmi_bt_output_xfer_read_addr),
		.output_xfer_read_data(ipmi_bt_output_xfer_read_data),
		.output_xfer_read_clk(sequencer_clock),

		.bmc_to_host_ctl_sms_req(ipmi_bt_bmc_to_host_ctl_sms_req),
		.bmc_to_host_ctl_sms_ack(ipmi_bt_bmc_to_host_ctl_sms_ack_reg),
		.bmc_to_host_ctl_attn_req(ipmi_bt_bmc_to_host_ctl_attn_req),
		.bmc_to_host_ctl_attn_ack(ipmi_bt_bmc_to_host_ctl_attn_ack_reg),
		.host_to_bmc_ctl_attn_req(ipmi_bt_host_to_bmc_ctl_attn_req_reg),
		.irq_ack(ipmi_bt_irq_ack_reg),
		.irq_bmc_reset(ipmi_bt_irq_bmc_reset_reg),

		.bmc_to_host_ctl_sms_ack_cont(ipmi_bt_bmc_to_host_ctl_sms_ack_cont),
		.bmc_to_host_ctl_attn_ack_cont(ipmi_bt_bmc_to_host_ctl_attn_ack_cont),
		.host_to_bmc_ctl_attn_req_cont(ipmi_bt_host_to_bmc_ctl_attn_req_cont),
		.irq_ack_cont(ipmi_bt_irq_ack_cont),
		.irq_bmc_reset_cont(ipmi_bt_irq_bmc_reset_cont),

		.h_busy(ipmi_bt_host_to_bmc_ctl_h_busy_reg),
		.b_busy(ipmi_bt_bmc_to_host_ctl_b_busy),

		.irq_enable(ipmi_bt_irq_enable),
		.irq_req(ipmi_bt_irq_req),

		.hiomap_new_window_req(ipmi_bt_hiomap_new_window_req),
		.hiomap_new_window_ack(ipmi_bt_hiomap_new_window_ack),
		.hiomap_active_window_start_address(ipmi_bt_hiomap_active_window_start_address),
		.hiomap_active_window_size_bytes(ipmi_bt_hiomap_active_window_size_bytes),
		.hiomap_window_type_write(ipmi_bt_hiomap_window_type_write),

		.hiomap_flush_req(ipmi_bt_hiomap_flush_req),
		.hiomap_flush_ack(ipmi_bt_hiomap_flush_ack),
		.hiomap_flush_sector_count(ipmi_bt_hiomap_flush_sector_count),
		.hiomap_flush_type_erase(ipmi_bt_hiomap_flush_type_erase),
		.hiomap_erase_address_offset(ipmi_bt_hiomap_erase_address_offset),
		.hiomap_erase_sector_count(ipmi_bt_hiomap_erase_sector_count),

		.hiomap_erase_address_step(ipmi_bt_hiomap_erase_address_step),
		.hiomap_program_address_step(ipmi_bt_hiomap_program_address_step),
		.hiomap_def_win_size_bytes(ipmi_bt_hiomap_def_win_size_bytes),

		.reset(fpga_power_on_reset || (~lpc_slave_reset_n_buffered_gated)),
		.logic_clk(sequencer_clock),
		.rtc_base_clock(primary_platform_clock),

		.debug_port(ipmi_bt_slave_interface_debug_port)
	);

	// Instantiate Flash block cache memory
	reg [27:0] flash_cache_addr_port_a;
	reg [27:0] flash_cache_addr_port_b;
	reg [27:0] flash_cache_read_addr;
	reg [7:0] flash_cache_write_data_port_a;
	reg [7:0] flash_cache_write_data_port_b;
	wire [7:0] flash_cache_read_data;
	reg flash_cache_wren_port_a = 0;
	reg flash_cache_wren_port_b = 0;
	reg flash_cache_port_ab_control_switch = 0;

	flash_block_cache flash_block_cache(
		.addr_port_a(flash_cache_addr_port_a[11:0]),
		.addr_port_b(flash_cache_addr_port_b[11:0]),
		.read_addr(flash_cache_read_addr[11:0]),
		.write_data_port_a(flash_cache_write_data_port_a),
		.write_data_port_b(flash_cache_write_data_port_b),
		.read_data(flash_cache_read_data),
		.wren_port_a(flash_cache_wren_port_a),
		.wren_port_b(flash_cache_wren_port_b),
		.port_ab_control_switch(flash_cache_port_ab_control_switch),
		.clk(sequencer_clock)
	);

`ifdef ENABLE_16550_UART_SUPPORT
	// Instantiate 16550-compatible UART
	reg [2:0] host_uart_3f8_address_reg = 0;
	reg [7:0] host_uart_3f8_data_out_reg = 0;
	wire [7:0] host_uart_3f8_data_in;
	reg host_uart_3f8_wren_reg = 0;
	reg host_uart_3f8_chip_select_reg = 0;
	reg host_uart_3f8_xfer_cycle_active_reg = 0;
	wire host_uart_3f8_ack;
	wire host_uart_3f8_irq;

	// WARNING: Hostboot expects a 1.8432MHz clock fed to the UART
	// This is the standard frequency used on the original PCs
	// Hostboot will blindly set the divisors assuming this is the
	// actual frequency fed to the UARTs...
	uart_top host_uart_3f8(
		.wb_clk_i(slow_1843khz_uart_clock),
		.wb_rst_i(fpga_power_on_reset || (~lpc_slave_reset_n_buffered_gated)),

		.wb_adr_i(host_uart_3f8_address_reg),
		.wb_dat_i(host_uart_3f8_data_out_reg),
		.wb_dat_o(host_uart_3f8_data_in),
		.wb_we_i(host_uart_3f8_wren_reg),
		.wb_stb_i(host_uart_3f8_chip_select_reg),
		.wb_cyc_i(host_uart_3f8_xfer_cycle_active_reg),
		.wb_ack_o(host_uart_3f8_ack),

		.int_o(host_uart_3f8_irq),

		.stx_pad_o(host_uart_tx),
		.srx_pad_i(host_uart_rx),

		// Not used
		.cts_pad_i(1'b0),
		.dsr_pad_i(1'b0),
		.ri_pad_i(1'b0),
		.dcd_pad_i(1'b0)
	);
`endif

	reg [3:0] lpc_reset_gate_counter = 0;
`ifdef RESYNTHESIZE_LPC_CLOCK
	reg [15:0] pll_unlock_counter = 0;
`endif
	reg pll_reset_request = 0;
	reg [15:0] clock_watchdog_counter = 0;
	reg clock_watchdog_reset_request = 0;
`ifndef RESYNTHESIZE_LPC_CLOCK
	reg sequencer_clock_prev = 0;
`endif
	always @(posedge primary_platform_clock) begin
`ifdef RESYNTHESIZE_LPC_CLOCK
		if (!lpc_pll_lock) begin
			if (pll_unlock_counter < 1024) begin
				pll_unlock_counter <= pll_unlock_counter + 1;
				pll_reset_request <= 0;
			end else begin
				if (pll_unlock_counter < 2048) begin
					pll_unlock_counter <= pll_unlock_counter + 1;
					pll_reset_request <= 1;
				end else begin
					pll_unlock_counter <= 0;
					pll_reset_request <= 0;
				end
			end
		end else begin
			pll_unlock_counter <= 0;
			pll_reset_request <= 0;
		end
`endif
		if (!lpc_slave_reset_n_buffered || pll_reset_request || clock_watchdog_reset_request) begin
			if (pll_reset_request || clock_watchdog_reset_request) begin
				lpc_slave_reset_n_buffered_gated <= 0;
			end else begin
				if (lpc_reset_gate_counter < 4) begin
					lpc_reset_gate_counter <= lpc_reset_gate_counter + 1;
				end else begin
					lpc_slave_reset_n_buffered_gated <= 0;
				end
			end
		end else begin
			lpc_slave_reset_n_buffered_gated <= 1;
			lpc_reset_gate_counter <= 0;
		end

`ifndef RESYNTHESIZE_LPC_CLOCK
		if (sequencer_clock == sequencer_clock_prev) begin
			if (clock_watchdog_counter < 32) begin
				clock_watchdog_counter <= clock_watchdog_counter + 1;
				clock_watchdog_reset_request <= 0;
			end else begin
				clock_watchdog_reset_request <= 1;
			end
		end else begin
			clock_watchdog_counter <= 0;
		end
		sequencer_clock_prev <= sequencer_clock;
`endif
	end

	parameter LPC_BRIDGE_INITIALIZE_STATE_01 = 0;
	parameter LPC_BRIDGE_INITIALIZE_STATE_02 = 1;
	parameter LPC_BRIDGE_INITIALIZE_STATE_03 = 2;
	parameter LPC_BRIDGE_INITIALIZE_STATE_04 = 3;
	parameter LPC_BRIDGE_INITIALIZE_STATE_05 = 4;
	parameter LPC_BRIDGE_INITIALIZE_STATE_06 = 5;
	parameter LPC_BRIDGE_INITIALIZE_STATE_07 = 6;
	parameter LPC_BRIDGE_INITIALIZE_STATE_08 = 7;
	parameter LPC_BRIDGE_INITIALIZE_STATE_09 = 8;
	parameter LPC_BRIDGE_INITIALIZE_STATE_10 = 9;
	parameter LPC_BRIDGE_INITIALIZE_STATE_11 = 10;
	parameter LPC_BRIDGE_INITIALIZE_STATE_12 = 11;
	parameter LPC_BRIDGE_INITIALIZE_STATE_13 = 12;
	parameter LPC_BRIDGE_INITIALIZE_STATE_14 = 13;
	parameter LPC_BRIDGE_INITIALIZE_STATE_15 = 14;
	parameter LPC_BRIDGE_TRANSFER_STATE_IDLE = 16;
	parameter LPC_BRIDGE_TRANSFER_STATE_TR01 = 17;
	parameter LPC_BRIDGE_TRANSFER_STATE_IO01 = 18;
	parameter LPC_BRIDGE_TRANSFER_STATE_IW01 = 32;
	parameter LPC_BRIDGE_TRANSFER_STATE_IW02 = 33;
	parameter LPC_BRIDGE_TRANSFER_STATE_IW03 = 34;
	parameter LPC_BRIDGE_TRANSFER_STATE_IW04 = 35;
	parameter LPC_BRIDGE_TRANSFER_STATE_IW05 = 36;
	parameter LPC_BRIDGE_TRANSFER_STATE_IR01 = 64;
	parameter LPC_BRIDGE_TRANSFER_STATE_IR02 = 65;
	parameter LPC_BRIDGE_TRANSFER_STATE_IR03 = 66;
	parameter LPC_BRIDGE_TRANSFER_STATE_IR04 = 67;
	parameter LPC_BRIDGE_TRANSFER_STATE_WR01 = 128;
	parameter LPC_BRIDGE_TRANSFER_STATE_WR02 = 129;
	parameter LPC_BRIDGE_TRANSFER_STATE_WR03 = 130;
	parameter LPC_BRIDGE_TRANSFER_STATE_WR04 = 131;
	parameter LPC_BRIDGE_TRANSFER_STATE_WR05 = 132;
	parameter LPC_BRIDGE_TRANSFER_STATE_WR06 = 133;
	parameter LPC_BRIDGE_TRANSFER_STATE_WR07 = 134;
	parameter LPC_BRIDGE_TRANSFER_STATE_WR08 = 135;
	parameter LPC_BRIDGE_TRANSFER_STATE_RD01 = 192;
	parameter LPC_BRIDGE_TRANSFER_STATE_RD02 = 193;
	parameter LPC_BRIDGE_TRANSFER_STATE_RD03 = 194;
	parameter LPC_BRIDGE_TRANSFER_STATE_RD04 = 195;
	parameter LPC_BRIDGE_TRANSFER_STATE_RD05 = 196;
	parameter LPC_BRIDGE_TRANSFER_STATE_RD06 = 197;
	parameter LPC_BRIDGE_TRANSFER_STATE_RD07 = 198;
	parameter LPC_BRIDGE_TRANSFER_STATE_RD08 = 199;
	parameter LPC_BRIDGE_TRANSFER_STATE_RD09 = 200;
	parameter LPC_BRIDGE_TRANSFER_STATE_RD10 = 201;
	parameter LPC_BRIDGE_TRANSFER_STATE_RD11 = 202;
	parameter LPC_BRIDGE_TRANSFER_STATE_RC01 = 224;
	parameter LPC_BRIDGE_TRANSFER_STATE_RC02 = 225;
	parameter LPC_BRIDGE_TRANSFER_STATE_RC03 = 226;

	parameter BLOCK_CACHE_INITIALIZE_STATE_01 = 0;
	parameter BLOCK_CACHE_TRANSFER_STATE_IDLE = 8;
	parameter BLOCK_CACHE_TRANSFER_STATE_RD01 = 16;
	parameter BLOCK_CACHE_TRANSFER_STATE_RD02 = 17;
	parameter BLOCK_CACHE_TRANSFER_STATE_RD03 = 18;
	parameter BLOCK_CACHE_TRANSFER_STATE_RD04 = 19;
	parameter BLOCK_CACHE_TRANSFER_STATE_RD05 = 20;
	parameter BLOCK_CACHE_TRANSFER_STATE_RD06 = 21;
	parameter BLOCK_CACHE_TRANSFER_STATE_RD07 = 22;
	parameter BLOCK_CACHE_TRANSFER_STATE_RD08 = 23;
	parameter BLOCK_CACHE_TRANSFER_STATE_RD09 = 24;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW01 = 64;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW02 = 65;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW03 = 66;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW04 = 67;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW05 = 68;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW06 = 69;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW07 = 70;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW08 = 71;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW09 = 72;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW10 = 73;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW11 = 74;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW12 = 75;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW13 = 76;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW14 = 77;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW15 = 78;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW16 = 79;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW17 = 80;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW18 = 81;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW19 = 82;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW20 = 83;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW21 = 84;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW22 = 85;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW23 = 86;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW24 = 87;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW25 = 88;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW26 = 89;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW27 = 90;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW28 = 91;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW29 = 92;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW30 = 93;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW31 = 94;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW32 = 95;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW33 = 96;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW34 = 97;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW35 = 98;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW36 = 99;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW37 = 100;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW38 = 101;
	parameter BLOCK_CACHE_TRANSFER_STATE_EW39 = 102;

`ifdef ENABLE_FSI_MASTER_SUPPORT
	parameter FSI_COMMAND_INITIALIZE_STATE_01 = 0;
	parameter FSI_COMMAND_INITIALIZE_STATE_02 = 1;
	parameter FSI_COMMAND_INITIALIZE_STATE_03 = 2;
	parameter FSI_COMMAND_INITIALIZE_STATE_04 = 3;
	parameter FSI_COMMAND_INITIALIZE_STATE_05 = 4;
	parameter FSI_COMMAND_INITIALIZE_STATE_06 = 5;
	parameter FSI_COMMAND_INITIALIZE_STATE_07 = 6;
	parameter FSI_COMMAND_INITIALIZE_STATE_08 = 7;
	parameter FSI_COMMAND_INITIALIZE_STATE_09 = 8;
	parameter FSI_COMMAND_INITIALIZE_STATE_10 = 9;
	parameter FSI_COMMAND_INITIALIZE_STATE_11 = 10;
	parameter FSI_COMMAND_INITIALIZE_STATE_12 = 11;
	parameter FSI_COMMAND_INITIALIZE_STATE_13 = 12;
	parameter FSI_COMMAND_INITIALIZE_STATE_14 = 13;
	parameter FSI_COMMAND_INITIALIZE_STATE_15 = 14;
	parameter FSI_COMMAND_INITIALIZE_STATE_16 = 15;
	parameter FSI_COMMAND_INITIALIZE_STATE_17 = 16;
	parameter FSI_COMMAND_TRANSFER_STATE_IDLE = 32;
	parameter FSI_COMMAND_POWER9_IPL_STATE_01 = 48;
	parameter FSI_COMMAND_POWER9_IPL_STATE_02 = 49;
	parameter FSI_COMMAND_POWER9_IPL_STATE_03 = 50;
	parameter FSI_COMMAND_POWER9_IPL_STATE_04 = 51;
	parameter FSI_COMMAND_POWER9_IPL_STATE_05 = 52;
	parameter FSI_COMMAND_POWER9_IPL_STATE_06 = 53;
	parameter FSI_COMMAND_POWER9_IPL_STATE_07 = 54;
	parameter FSI_COMMAND_POWER9_IPL_STATE_08 = 55;
	parameter FSI_COMMAND_POWER9_IPL_STATE_09 = 56;
	parameter FSI_COMMAND_POWER9_IPL_STATE_10 = 57;
	parameter FSI_COMMAND_POWER9_IPL_STATE_11 = 58;
	parameter FSI_COMMAND_POWER9_IPL_STATE_12 = 59;
	parameter FSI_COMMAND_POWER9_IPL_STATE_13 = 60;
	parameter FSI_COMMAND_POWER9_IPL_STATE_14 = 61;
	parameter FSI_COMMAND_POWER9_IPL_STATE_15 = 62;
	parameter FSI_COMMAND_POWER9_IPL_STATE_16 = 63;
	parameter FSI_COMMAND_POWER9_IPL_STATE_17 = 64;
	parameter FSI_COMMAND_POWER9_IPL_STATE_18 = 65;
	parameter FSI_COMMAND_POWER9_IPL_STATE_19 = 66;
	parameter FSI_COMMAND_POWER9_IPL_STATE_20 = 67;
	parameter FSI_COMMAND_POWER9_IPL_STATE_21 = 68;
	parameter FSI_COMMAND_POWER9_IPL_STATE_22 = 69;
	parameter FSI_COMMAND_POWER9_IPL_STATE_23 = 70;
	parameter FSI_COMMAND_POWER9_IPL_STATE_24 = 71;
	parameter FSI_COMMAND_POWER9_IPL_STATE_25 = 72;
	parameter FSI_COMMAND_POWER9_IPL_STATE_26 = 73;
`endif

	// Main sequencer
	reg [7:0] fw_transfer_state = 0;
	reg [7:0] spi_reset_counter = 0;
	reg [27:0] spi_byte_write_count = 0;
	reg [27:0] spi_byte_read_count = 0;
	reg [27:0] lpc_slave_address_reg = 0;
	reg [7:0] lpc_slave_rx_data_reg = 0;
	reg [1:0] lpc_done_pulse_counter = 0;

	reg lpc_slave_address_ready_reg = 0;
	reg lpc_slave_data_ready_reg = 0;
	reg lpc_slave_tpm_cycle_reg = 0;
	reg lpc_slave_firmware_cycle_reg = 0;
	reg lpc_slave_cycle_direction_reg = 0;
	reg lpc_slave_write_complete = 0;

	reg [27:0] hiomap_active_window_start_address = 0;
	reg [27:0] hiomap_active_window_size_bytes = 0;
	reg hiomap_window_type_write = 0;
	reg hiomap_flush_type_erase = 0;
	reg [27:0] hiomap_erase_address_offset = 0;

	// Flash block cache handler
	reg [7:0] cache_load_state = 0;
	reg use_flash_sector_cache = 0;
	reg [27:0] hiomap_current_erase_address = 0;
	reg [27:0] hiomap_current_write_address = 0;
	reg [27:0] sector_count_erase = 0;

`ifdef LPC_SLAVE_DEBUG
	reg [15:0] port_80h_register;
`endif

`ifdef ENABLE_FSI_MASTER_SUPPORT
	always @(posedge fsi_master_core_clock) begin
		if (fpga_power_on_reset || (~lpc_slave_reset_n_buffered_gated)) begin
			fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_01;
		end else begin
			case (fsi_master_state)
				FSI_COMMAND_INITIALIZE_STATE_01: begin
					// Wait for FSI core to start up
					if (!fsi_master_cycle_complete) begin
						fsi_master_enhanced_error_recovery <= 3;
						fsi_master_enable_ipoll <= 0;
						// The POWER9 will not respond correctly if we issue commands too quickly!
						// Purposefully add ACK to CMD turnaround time to compensate...
						// This delay was determined with the main FSI clock running at 750KHz
						// It is currently unknown if clock cycle count or an absolute time interval is the underlying requirement
						fsi_master_internal_cmd_issue_delay <= 20;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_02;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_02: begin
					if (!fsi_master_cycle_complete) begin
						// Set up FSI slave...
						// [31]=1:	Warm start done
						// [29]=1:	HW CRC check enabled
						// [26:24]:	FSI slave ID (0)
						// [23:20]:	Echo delay (7)
						// [19:16]:	Send delay (7)
						// [11:8]:	Clock ratio (8)
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_CFAM_FSI_SMODE;
						fsi_master_tx_data <= 32'ha0ff0800;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 1;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_03;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_03: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_04;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_04: begin
					if (!fsi_master_cycle_complete) begin
						// Ensure asynchronous clock mode is set
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_LL_MODE_REG;
						fsi_master_tx_data <= 32'h00000001;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 1;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_05;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_05: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_06;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_06: begin
					if (!fsi_master_cycle_complete) begin
						// Read IBM_POWER9_CBS_CS in preparation to reconfigure SBE
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_CBS_CS;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 0;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_07;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_07: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_tx_data <= fsi_master_rx_data;
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_08;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_08: begin
					if (!fsi_master_cycle_complete) begin
						// Configure SBE to pre-IPL state
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_CBS_CS;
						fsi_master_tx_data[31] <= 0;	// Clear SBE IPL start flag
						fsi_master_tx_data[29] <= 0;	// Run SCAN0 and CLOCKSTART during IPL
						fsi_master_tx_data[28] <= 0;	// Clear SBE start prevention flag
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 1;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_09;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_09: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_10;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_10: begin
					if (!fsi_master_cycle_complete) begin
						// Clear SBE status mailbox
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_SBE_MSG_REGISTER;
						fsi_master_tx_data <= 32'h00000000;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 1;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_11;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_11: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_12;
					end
				end

				FSI_COMMAND_INITIALIZE_STATE_12: begin
					if (!fsi_master_cycle_complete) begin
						// Read CFAM error IRQ register
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_CFAM_FSI_SISC;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 0;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_13;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_13: begin
					if (fsi_master_cycle_complete) begin
						fsi_cfam_error_register = fsi_master_rx_data;
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_14;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_14: begin
					if (!fsi_master_cycle_complete) begin
						// Read CFAM status register
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_CFAM_FSI_SSTAT;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 0;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_15;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_15: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_16;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_16: begin
					if (!fsi_master_cycle_complete) begin
						// Clear CFAM error IRQ register
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_CFAM_FSI_SISC;
						fsi_master_tx_data <= fsi_cfam_error_register;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 1;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_17;
					end
				end
				FSI_COMMAND_INITIALIZE_STATE_17: begin
					if (fsi_master_cycle_complete) begin
						fsi_transfer_first_command_post_reset <= 1;
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_TRANSFER_STATE_IDLE;
					end
				end
				FSI_COMMAND_TRANSFER_STATE_IDLE: begin
					if ((!ext_ctl_enable_fsi_start_prev && ext_ctl_enable_fsi_start)
						|| (fsi_transfer_first_command_post_reset && ext_ctl_enable_fsi_start)) begin
						fsi_transfer_first_command_post_reset <= 0;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_01;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_01: begin
					if (!fsi_master_cycle_complete) begin
						// Ensure asynchronous clock mode is set
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_LL_MODE_REG;
						fsi_master_tx_data <= 32'h00000001;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 1;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_02;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_02: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_03;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_03: begin
					if (!fsi_master_cycle_complete) begin
						// Read IBM_POWER9_ROOT_CTRL8 for clock mux select override
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_ROOT_CTRL8;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 0;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_04;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_04: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_05;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_05: begin
					if (!fsi_master_cycle_complete) begin
						// Write clock mux select override
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_ROOT_CTRL8;
						fsi_master_tx_data <= fsi_master_rx_data | 32'hc;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 1;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_06;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_06: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_07;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_07: begin
					if (!fsi_master_cycle_complete) begin
						// Setup FSI2PIB to report checkstop
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_FSI_A_SI1S;
						fsi_master_tx_data <= 32'h20000000;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 1;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_08;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_08: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_09;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_09: begin
					if (!fsi_master_cycle_complete) begin
						// Enable XSTOP/ATTN interrupt
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_FSI2PIB_TRUE_MASK;
						fsi_master_tx_data <= 32'h60000000;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 1;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_10;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_10: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_11;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_11: begin
					if (!fsi_master_cycle_complete) begin
						// Arm XSTOP/ATTN interrupt
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_FSI2PIB_INTERRUPT;
						fsi_master_tx_data <= 32'hffffffff;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 1;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_12;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_12: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_13;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_13: begin
					if (!fsi_master_cycle_complete) begin
						// Read IBM_POWER9_SBE_CTRL_STATUS for SBE boot side configuration
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_SBE_CTRL_STATUS;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 0;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_14;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_14: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_tx_data <= fsi_master_rx_data;
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_15;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_15: begin
					if (!fsi_master_cycle_complete) begin
						// Write SBE boot side configuration
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_SBE_CTRL_STATUS;
						fsi_master_tx_data[14] <= 0;	// Set SBE boot side to 0
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 1;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_16;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_16: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_17;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_17: begin
					if (!fsi_master_cycle_complete) begin
						// Read IBM_POWER9_CBS_CS in preparation to fire SBE
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_CBS_CS;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 0;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_18;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_18: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_tx_data <= fsi_master_rx_data;
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_19;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_19: begin
					if (!fsi_master_cycle_complete) begin
						// Ensure edge-triggered SBE start bit is deasserted prior to ignition...
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_CBS_CS;
						fsi_master_tx_data[31] <= 0;	// Clear SBE IPL start flag
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 1;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_20;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_20: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_21;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_21: begin
					if (!fsi_master_cycle_complete) begin
						// ...and light the fuse!
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_CBS_CS;
						fsi_master_tx_data[31] <= 1;	// Set SBE IPL start flag
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 1;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_22;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_22: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_start_cycle <= 0;
						fsi_delay_counter <= 0;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_23;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_23: begin
					// Initialization complete!
					// Wait a little while, then get the SBE status register (IBM_POWER9_SBE_MSG_REGISTER)
					if (fsi_delay_counter >= 262144) begin
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_24;
					end else begin
						fsi_delay_counter <= fsi_delay_counter + 1;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_24: begin
					if (!fsi_master_cycle_complete) begin
						// Read IBM_POWER9_CBS_CS in preparation to fire SBE
						fsi_master_slave_id <= 0;
						fsi_master_cfam_address <= IBM_POWER9_SBE_MSG_REGISTER;
						fsi_master_data_length <= 2;
						fsi_master_data_direction <= 0;
						fsi_master_start_cycle <= 1;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_25;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_25: begin
					if (fsi_master_cycle_complete) begin
						fsi_master_tx_data <= fsi_master_rx_data;
						fsi_master_start_cycle <= 0;
						fsi_master_state <= FSI_COMMAND_POWER9_IPL_STATE_26;
					end
				end
				FSI_COMMAND_POWER9_IPL_STATE_26: begin
					// State machine done!
					// Idle here...
				end
				default: begin
					// Should never reach this state
					fsi_master_state <= FSI_COMMAND_INITIALIZE_STATE_01;
				end
			endcase
		end

		ext_ctl_enable_fsi_start_prev <= ext_ctl_enable_fsi_start;
	end
`endif

	always @(posedge sequencer_clock) begin
		if (fpga_power_on_reset || (~lpc_slave_reset_n_buffered_gated)) begin
			fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_01;
			cache_load_state <= BLOCK_CACHE_INITIALIZE_STATE_01;
			spi_external_master_cycle_start <= 0;
			spi_external_master_reset_n_buffered <= 1'b0;
			spi_external_master_hold_ss_active <= 0;
			lpc_fw_input_xfer_write_wren <= 0;
			flash_cache_wren_port_b <= 0;
			lpc_slave_irq_request <= 0;
		end else begin
			case (cache_load_state)
				BLOCK_CACHE_INITIALIZE_STATE_01: begin
					// Reset caching system
					use_flash_sector_cache <= 0;
					hiomap_active_window_start_address <= 0;
					hiomap_active_window_size_bytes <= ipmi_bt_hiomap_def_win_size_bytes;
					hiomap_window_type_write = 0;
					hiomap_flush_type_erase = 0;

					if (fw_transfer_state == LPC_BRIDGE_TRANSFER_STATE_IDLE) begin
						// Main sequencer initialized and ready
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_IDLE;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_IDLE: begin
					if (fw_transfer_state == LPC_BRIDGE_TRANSFER_STATE_IDLE) begin
						// Main transfer state machine has returned to idle, it is now safe to take control of the SPI flash core if needed
						if (ipmi_bt_hiomap_new_window_req && flash_cache_port_ab_control_switch) begin
							// Latch current HIOMAP window status
							hiomap_active_window_start_address <= ipmi_bt_hiomap_active_window_start_address;
							hiomap_active_window_size_bytes <= ipmi_bt_hiomap_active_window_size_bytes;
							hiomap_window_type_write <= ipmi_bt_hiomap_window_type_write;

							// Determine if window is cacheable
							if ((ipmi_bt_hiomap_active_window_size_bytes > 0) && (ipmi_bt_hiomap_active_window_size_bytes <= HIOMAP_MAX_CACHE_SIZE_BYTES)) begin
								// Cacheable request
								use_flash_sector_cache <= 1;
								cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_RD01;
							end else begin
								// Uncacheable request
								use_flash_sector_cache <= 0;
								ipmi_bt_hiomap_new_window_ack <= 1;
								cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_RD09;
							end
						end
						if (ipmi_bt_hiomap_flush_req) begin
							if (ipmi_bt_hiomap_window_type_write
								&& (!ipmi_bt_hiomap_flush_type_erase
									|| (ipmi_bt_hiomap_flush_type_erase && ((ipmi_bt_hiomap_erase_address_offset + ipmi_bt_hiomap_erase_sector_count) <= ipmi_bt_hiomap_active_window_size_bytes)))
`ifdef BLOCK_GUARD_PARTITION_WRITES
								&& (((ipmi_bt_hiomap_active_window_start_address + ipmi_bt_hiomap_active_window_size_bytes) < GUARD_PARTITION_START_ADDRESS)
								|| (ipmi_bt_hiomap_active_window_start_address > GUARD_PARTITION_END_ADDRESS))
`endif
`ifdef BLOCK_NONESSENTIAL_WRITES
								&& ((((ipmi_bt_hiomap_active_window_start_address + ipmi_bt_hiomap_active_window_size_bytes) >= VPD_PARTITION_START_ADDRESS)
									&& ((ipmi_bt_hiomap_active_window_start_address + ipmi_bt_hiomap_active_window_size_bytes) <= VPD_PARTITION_END_ADDRESS))
`ifndef BLOCK_NVRAM_PARTITION_WRITES
								|| (((ipmi_bt_hiomap_active_window_start_address + ipmi_bt_hiomap_active_window_size_bytes) >= NVRAM_PARTITION_START_ADDRESS)
									&& ((ipmi_bt_hiomap_active_window_start_address + ipmi_bt_hiomap_active_window_size_bytes) <= NVRAM_PARTITION_END_ADDRESS))
`endif
									)
`endif
								) begin

								// Latch request type and parameters
								hiomap_flush_type_erase <= ipmi_bt_hiomap_flush_type_erase;
								hiomap_erase_address_offset <= ipmi_bt_hiomap_erase_address_offset;
								if (ipmi_bt_hiomap_flush_type_erase) begin
									sector_count_erase <= ipmi_bt_hiomap_erase_sector_count;
								end else begin
									sector_count_erase <= ipmi_bt_hiomap_flush_sector_count;
								end

								// Initialize erase and write pointers
								if (ipmi_bt_hiomap_flush_type_erase) begin
									hiomap_current_erase_address <= ipmi_bt_hiomap_active_window_start_address + ipmi_bt_hiomap_erase_address_offset;
								end else begin
									hiomap_current_erase_address <= ipmi_bt_hiomap_active_window_start_address;
								end
								hiomap_current_write_address <= ipmi_bt_hiomap_active_window_start_address;

								// Initiate subsector erase
								cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW01;
							end else begin
								// Silently fail write to avoid system deadlock
								ipmi_bt_hiomap_flush_ack <= 1;
								cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW38;
							end
						end
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_RD01: begin
					// 4-byte quad input quad output fast read command
					// Requires 10 dummy cycles by default after last address byte transmission
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= 8'hec;
						spi_external_master_hold_ss_active <= 1;
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_RD02;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_RD02: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_RD03;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_RD03: begin
					// Address (4 bytes / 1 word)
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= {4'b0000, hiomap_active_window_start_address[27:0]};
						spi_external_master_qspi_mode_active <= 1;
						spi_external_master_qspi_transfer_mode <= 1;
						spi_external_master_qspi_transfer_direction <= 1;
						spi_external_master_dummy_cycle_count <= SPI_FAST_READ_DUMMY_CLOCK_CYCLES;
						spi_external_master_cycle_start <= 1;

						spi_byte_read_count <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_RD04;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_RD04: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_RD05;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_RD05: begin
					if (!spi_external_master_transaction_complete) begin
						// Data words
						spi_external_master_tx_data <= 0;	// Not used
						spi_external_master_qspi_mode_active <= 1;
						spi_external_master_qspi_transfer_mode <= 1;
						spi_external_master_qspi_transfer_direction <= 0;
						spi_external_master_dummy_cycle_count <= 0;
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_RD06;

						flash_cache_wren_port_b <= 0;
						flash_cache_addr_port_b <= spi_byte_read_count;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_RD06: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;

						// Start first memory write as early as possible
						flash_cache_write_data_port_b <= spi_external_master_rx_data[31:24];
						flash_cache_addr_port_b <= spi_byte_read_count;
						flash_cache_wren_port_b <= 1;
						spi_byte_read_count <= spi_byte_read_count + 1;

						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_RD07;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_RD07: begin
					// Write remaining bytes
					case (spi_byte_read_count[1:0])
						2'b01: flash_cache_write_data_port_b <= spi_external_master_rx_data[23:16];
						2'b10: flash_cache_write_data_port_b <= spi_external_master_rx_data[15:8];
						2'b11: flash_cache_write_data_port_b <= spi_external_master_rx_data[7:0];
					endcase
					flash_cache_addr_port_b <= spi_byte_read_count;
					flash_cache_wren_port_b <= 1;
					spi_byte_read_count <= spi_byte_read_count + 1;

					if (spi_byte_read_count[1:0] == 2'b11) begin
						// Word fully written to cache RAM
						// Determine if we need more words from Flash or if the transfer is complete
						if ((spi_byte_read_count + 1) >= hiomap_active_window_size_bytes) begin
							cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_RD08;
						end else begin
							cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_RD05;
						end
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_RD08: begin
					if (!spi_external_master_transaction_complete) begin
						// Release CS and prepare the SPI device for the next command
						spi_external_master_hold_ss_active <= 0;
						spi_external_master_qspi_transfer_mode <= 0;
						spi_external_master_qspi_mode_active <= 0;
						ipmi_bt_hiomap_new_window_ack <= 1;
						flash_cache_wren_port_b <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_RD09;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_RD09: begin
					if (!ipmi_bt_hiomap_new_window_req) begin
						ipmi_bt_hiomap_new_window_ack <= 0;
						if (ipmi_bt_hiomap_flush_req) begin
							// Finished re-reading cache due to previous ERASE or FLUSH command
							// Transfer control back to erase/write state machine
							cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW38;
						end else begin
							// Standard cache load for new window complete
							cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_IDLE;
						end
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW01: begin
					// Write enable
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= 8'h06;
						spi_external_master_hold_ss_active <= 0;
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW02;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW02: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW03;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW03: begin
					// 4-byte subsector erase
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= 8'h21;
						spi_external_master_hold_ss_active <= 1;
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW04;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW04: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW05;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW05: begin
					// Address byte 1
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= {4'b0000, hiomap_current_erase_address[27:24]};
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW06;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW06: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW07;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW07: begin
					// Address byte 2
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= hiomap_current_erase_address[23:16];
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW08;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW08: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW09;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW09: begin
					// Address byte 3
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= hiomap_current_erase_address[15:8];
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW10;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW10: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW11;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW11: begin
					// Address byte 4
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= hiomap_current_erase_address[7:0];
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW12;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW12: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW13;
					end

					// Release slave select after transaction completes
					spi_external_master_hold_ss_active <= 0;
				end
				BLOCK_CACHE_TRANSFER_STATE_EW13: begin
					// Read flag status register
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= 8'h70;
						spi_external_master_hold_ss_active <= 1;
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW14;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW14: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW15;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW15: begin
					if (!spi_external_master_transaction_complete) begin
						// Flag status register contents
						spi_external_master_tx_data <= 0;	// Don't care, but have to clock /something/ in to get data out over SPI
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW16;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW16: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;

						if (spi_external_master_rx_data[7]) begin
							// Operation complete!
							if (sector_count_erase > 1) begin
								// Erase next requested sector
								sector_count_erase <= sector_count_erase - 1;
								hiomap_current_erase_address <= hiomap_current_erase_address + ipmi_bt_hiomap_erase_address_step;
								cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW01;
							end else begin
								if (hiomap_flush_type_erase) begin
									// Request was only to erase subsector(s)
									// Exit without writing data
									cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW34;
								end else begin
									// Request was to erase + write (flush)
									// Initiate data write phase
									spi_byte_write_count <= 0;
									flash_cache_read_addr <= 0;
									cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW17;
								end
							end
						end else begin
							// Poll flag status register until device is ready
							cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW13;
						end
					end

					// Release slave select after transaction completes
					spi_external_master_hold_ss_active <= 0;
				end
				BLOCK_CACHE_TRANSFER_STATE_EW17: begin
					// Write enable
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= 8'h06;
						spi_external_master_hold_ss_active <= 0;
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW18;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW18: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW19;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW19: begin
					// 4-byte page program
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= 8'h12;
						spi_external_master_hold_ss_active <= 1;
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW20;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW20: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW21;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW21: begin
					// Address byte 1
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= {4'b0000, hiomap_current_write_address[27:24]};
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW22;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW22: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW23;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW23: begin
					// Address byte 2
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= hiomap_current_write_address[23:16];
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW24;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW24: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW25;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW25: begin
					// Address byte 3
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= hiomap_current_write_address[15:8];
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW26;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW26: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW27;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW27: begin
					// Address byte 4
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= hiomap_current_write_address[7:0];
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW28;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW28: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW29;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW29: begin
					// Wait for RAM to respond with read data
					cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW30;
				end
				BLOCK_CACHE_TRANSFER_STATE_EW30: begin
					// Data bytes
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= flash_cache_read_data;
						spi_external_master_cycle_start <= 1;
						spi_byte_write_count <= spi_byte_write_count + 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW31;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW31: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						flash_cache_read_addr <= flash_cache_read_addr + 1;
						if (spi_byte_write_count >= ipmi_bt_hiomap_program_address_step) begin
							spi_byte_write_count <= 0;
							cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW32;
						end else begin
							cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW29;
						end
					end

					if (spi_byte_write_count >= ipmi_bt_hiomap_program_address_step) begin
						// Release slave select after transaction completes
						spi_external_master_hold_ss_active <= 0;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW32: begin
					// Read flag status register
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= 8'h70;
						spi_external_master_hold_ss_active <= 1;
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW33;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW33: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW34;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW34: begin
					if (!spi_external_master_transaction_complete) begin
						// Flag status register contents
						spi_external_master_tx_data <= 0;	// Don't care, but have to clock /something/ in to get data out over SPI
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW35;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW35: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;

						if (spi_external_master_rx_data[7]) begin
							// Operation complete!
							if (flash_cache_read_addr < hiomap_active_window_size_bytes) begin
								// Program next requested page
								hiomap_current_write_address <= hiomap_current_write_address + ipmi_bt_hiomap_program_address_step;
								cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW17;
							end else begin
								cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW36;
							end
						end else begin
							// Poll flag status register until device is ready
							cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW32;
						end
					end

					// Release slave select after transaction completes
					spi_external_master_hold_ss_active <= 0;
				end
				BLOCK_CACHE_TRANSFER_STATE_EW36: begin
					// Write disable
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= 8'h04;
						spi_external_master_hold_ss_active <= 0;
						spi_external_master_cycle_start <= 1;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW37;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW37: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW38;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW38: begin
					ipmi_bt_hiomap_flush_ack <= 1;
					if (hiomap_flush_type_erase) begin
						// Request was only to erase subsector(s)
						// Depending on current window, we either
						// need to reload the cache or exit.
						if (hiomap_window_type_write || use_flash_sector_cache) begin
							// Reload cache
							cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_RD01;
						end else begin
							// Exit without writing data
							cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW39;
						end
					end else begin
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_EW39;
					end
				end
				BLOCK_CACHE_TRANSFER_STATE_EW39: begin
					if (!ipmi_bt_hiomap_flush_req) begin
						ipmi_bt_hiomap_flush_ack <= 0;
						cache_load_state <= BLOCK_CACHE_TRANSFER_STATE_IDLE;
					end
				end
				default: begin
					// Should never reach this state
					cache_load_state <= BLOCK_CACHE_INITIALIZE_STATE_01;
				end
			endcase

			case (fw_transfer_state)
				LPC_BRIDGE_INITIALIZE_STATE_01: begin
					// Start SPI device reset process
					if (!spi_external_master_transaction_complete) begin
						// Reset pin...
						spi_external_master_reset_n_buffered <= 1'b0;
						spi_reset_counter <= 0;

						// Issue RESET ENABLE command
						spi_external_master_tx_data <= 8'h66;
						spi_external_master_hold_ss_active <= 0;
						spi_external_master_cycle_start <= 1;

						fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_02;
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_02: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_03;
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_03: begin
					// wait for device to fully enter reset state
					if (spi_reset_counter < 64) begin
						spi_reset_counter <= spi_reset_counter + 1;
					end else begin
						// Issue RESET MEMORY command
						if (!spi_external_master_transaction_complete) begin
							spi_external_master_tx_data <= 8'h99;
							spi_external_master_hold_ss_active <= 0;
							spi_external_master_cycle_start <= 1;
							spi_external_master_reset_n_buffered <= 1'b1;
							spi_reset_counter <= 0;
							fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_04;
						end
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_04: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_05;
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_05: begin
					// wait for device to come back out of reset
					if (spi_reset_counter < 64) begin
						spi_reset_counter <= spi_reset_counter + 1;
					end else begin
						fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_06;
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_06: begin
					// Enable 4 byte addressing mode
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= 8'hb7;
						spi_external_master_hold_ss_active <= 0;
						spi_external_master_cycle_start <= 1;
						fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_07;
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_07: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_08;
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_08: begin
					// Write enable
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= 8'h06;
						spi_external_master_hold_ss_active <= 0;
						spi_external_master_cycle_start <= 1;
						fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_09;
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_09: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_10;
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_10: begin
					// Set up volatile configuration register write operation
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= 8'h81;
						spi_external_master_hold_ss_active <= 1;
						spi_external_master_cycle_start <= 1;
						fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_11;
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_11: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_12;
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_12: begin
					// Initialize volatile configuration register
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data[7:4] <= SPI_FAST_READ_DUMMY_CLOCK_CYCLES;
						spi_external_master_tx_data[3] <= 1;
						spi_external_master_tx_data[2] <= 0;
						spi_external_master_tx_data[1:0] <= 2'b11;
						spi_external_master_hold_ss_active <= 0;
						spi_external_master_cycle_start <= 1;
						fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_13;
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_13: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_14;
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_14: begin
					// Write disable
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= 8'h04;
						spi_external_master_hold_ss_active <= 0;
						spi_external_master_cycle_start <= 1;
						fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_15;
					end
				end
				LPC_BRIDGE_INITIALIZE_STATE_15: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IDLE;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_IDLE: begin
					if (lpc_slave_address_ready_reg && lpc_slave_firmware_cycle_reg && !lpc_slave_tpm_cycle_reg) begin
						if (!ipmi_bt_hiomap_new_window_req && !ipmi_bt_hiomap_flush_req) begin
							fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_TR01;
						end
					end else begin
						if (lpc_slave_address_ready_reg && !lpc_slave_firmware_cycle_reg && !lpc_slave_tpm_cycle_reg) begin
							fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IO01;
						end else begin
							if (lpc_slave_address_ready_reg && !lpc_slave_continue) begin
								lpc_slave_continue = 1;
							end else begin
								lpc_slave_continue = 0;
							end
							if (lpc_slave_data_ready_reg && !lpc_slave_data_ack) begin
								lpc_slave_data_ack = 1;
							end else begin
								lpc_slave_data_ack = 0;
							end
							if (lpc_slave_exception && !lpc_slave_exception_ack) begin
								lpc_slave_exception_ack = 1;
							end else begin
								lpc_slave_exception_ack = 0;
							end
						end
					end

					// To ensure atomicity of read/write operations, only switch the cache write mux when no LPC transaction is in progress
					flash_cache_port_ab_control_switch <= ipmi_bt_hiomap_new_window_req;
				end
				LPC_BRIDGE_TRANSFER_STATE_IO01: begin
					if (lpc_slave_address_ready_reg) begin
						lpc_slave_address_reg <= lpc_slave_address;

						if (lpc_slave_cycle_direction_reg) begin
							fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IW01;
						end else begin
							fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IR01;
						end
					end else begin
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IDLE;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_IW01: begin
					if (lpc_slave_data_ready_reg) begin
						lpc_slave_rx_data_reg <= lpc_slave_rx_data;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IW02;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_IW02: begin
					case (lpc_slave_address_reg[15:0])
`ifndef LPC_SLAVE_DEBUG
						DEBUG_PORT_ADDRESS_HIGH: debug_leds[15:8] <= lpc_slave_rx_data_reg;
						DEBUG_PORT_ADDRESS_LOW: debug_leds[7:0] <= lpc_slave_rx_data_reg;
`else
						DEBUG_PORT_ADDRESS_HIGH: port_80h_register[15:8] <= lpc_slave_rx_data_reg;
						DEBUG_PORT_ADDRESS_LOW: port_80h_register[7:0] <= lpc_slave_rx_data_reg;
`endif
						IPMI_BT_PORT_BASE_ADDRESS: begin
							if (lpc_slave_rx_data_reg[6]) begin
								if (ipmi_bt_host_to_bmc_ctl_h_busy_reg) begin
									ipmi_bt_host_to_bmc_ctl_h_busy_reg <= 1'b0;
								end else begin
									ipmi_bt_host_to_bmc_ctl_h_busy_reg <= 1'b1;
								end
							end
							if (lpc_slave_rx_data_reg[4]) begin
								ipmi_bt_bmc_to_host_ctl_sms_ack_reg <= 1'b1;
							end
							if (lpc_slave_rx_data_reg[3]) begin
								ipmi_bt_bmc_to_host_ctl_attn_ack_reg <= 1'b1;
							end
							if (lpc_slave_rx_data_reg[2]) begin
								ipmi_bt_host_to_bmc_ctl_attn_req_reg <= 1'b1;
							end
							if (lpc_slave_rx_data_reg[1]) begin
								ipmi_bt_output_xfer_read_addr <= 0;
							end
							if (lpc_slave_rx_data_reg[0]) begin
								ipmi_bt_input_xfer_write_addr_reg <= 0;
								ipmi_bt_input_xfer_write_wren_reg <= 0;
							end
						end
						(IPMI_BT_PORT_BASE_ADDRESS + 1): begin
							ipmi_bt_input_xfer_write_data_reg <= lpc_slave_rx_data_reg;
							ipmi_bt_input_xfer_write_wren_reg <= 1;
						end
						(IPMI_BT_PORT_BASE_ADDRESS + 2): begin
							if (lpc_slave_rx_data_reg[7]) begin
								ipmi_bt_irq_bmc_reset_reg <= 1'b1;
							end
							if (lpc_slave_rx_data_reg[1]) begin
								ipmi_bt_irq_ack_reg <= 1'b1;
							end
							ipmi_bt_irq_enable <= lpc_slave_rx_data_reg[0];
						end

						default: begin
`ifdef ENABLE_16550_UART_SUPPORT
							if (lpc_slave_address_reg[15:3] == HOST_UART_BASE_ADDRESS[15:3]) begin
								host_uart_3f8_address_reg <= lpc_slave_address_reg - HOST_UART_BASE_ADDRESS;
								host_uart_3f8_data_out_reg <= lpc_slave_rx_data_reg;
								host_uart_3f8_wren_reg <= 1'b1;
								host_uart_3f8_chip_select_reg <= 1'b1;
								host_uart_3f8_xfer_cycle_active_reg <= 1'b1;
							end
`endif
						end
					endcase

					lpc_slave_write_complete <= 0;
					fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IW03;
				end
				LPC_BRIDGE_TRANSFER_STATE_IW03: begin
					if (!lpc_slave_write_complete) begin
						case (lpc_slave_address_reg[15:0])
							(IPMI_BT_PORT_BASE_ADDRESS + 1): begin
								ipmi_bt_input_xfer_write_addr_reg <= ipmi_bt_input_xfer_write_addr_reg + 1;
								ipmi_bt_input_xfer_write_wren_reg <= 0;
								lpc_slave_write_complete <= 1;
							end
							(IPMI_BT_PORT_BASE_ADDRESS + 2): begin
								// Handle synchronous IPMI BT IRQ reset handshake signals
								if (ipmi_bt_irq_bmc_reset_cont) begin
									ipmi_bt_irq_bmc_reset_reg <= 0;
								end

								// Do not continue write until slave has completed its reset cycle
								if (!ipmi_bt_irq_bmc_reset_reg) begin
									lpc_slave_write_complete <= 1;
								end
							end
							default: begin
`ifdef ENABLE_16550_UART_SUPPORT
								if (lpc_slave_address_reg[15:3] == HOST_UART_BASE_ADDRESS[15:3]) begin
									// Wait for Wishbone bus cycle to complete
									if (host_uart_3f8_xfer_cycle_active_reg && host_uart_3f8_ack) begin
										host_uart_3f8_wren_reg <= 1'b0;
										host_uart_3f8_chip_select_reg <= 1'b0;
										host_uart_3f8_xfer_cycle_active_reg <= 1'b0;
									end
									if (!host_uart_3f8_xfer_cycle_active_reg && !host_uart_3f8_ack) begin
										lpc_slave_write_complete <= 1;
									end
								end else begin
									lpc_slave_write_complete <= 1;
								end
`else
								lpc_slave_write_complete <= 1;
`endif
							end
						endcase
					end else begin
						lpc_slave_data_ack <= 1;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IW04;
						lpc_done_pulse_counter <= 0;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_IW04: begin
					if (lpc_done_pulse_counter > 2) begin
						lpc_slave_data_ack <= 0;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IW05;
					end
					lpc_done_pulse_counter <= lpc_done_pulse_counter + 1;
				end
				LPC_BRIDGE_TRANSFER_STATE_IW05: begin
					if (!lpc_slave_data_ready_reg) begin
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IDLE;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_IR01: begin
					case (lpc_slave_address_reg[15:0])
`ifndef LPC_SLAVE_DEBUG
						DEBUG_PORT_ADDRESS_HIGH: lpc_slave_tx_data <= debug_leds[15:8];
						DEBUG_PORT_ADDRESS_LOW: lpc_slave_tx_data <= debug_leds[7:0];
`else
						DEBUG_PORT_ADDRESS_HIGH: lpc_slave_tx_data <= port_80h_register[15:8];
						DEBUG_PORT_ADDRESS_LOW: lpc_slave_tx_data <= port_80h_register[7:0];
`endif

						IPMI_BT_PORT_BASE_ADDRESS: begin
							lpc_slave_tx_data[7] <= ipmi_bt_bmc_to_host_ctl_b_busy;
							lpc_slave_tx_data[6] <= ipmi_bt_host_to_bmc_ctl_h_busy_reg;
							lpc_slave_tx_data[5] <= 1'b0;
							lpc_slave_tx_data[4] <= ipmi_bt_bmc_to_host_ctl_sms_req;
							lpc_slave_tx_data[3] <= ipmi_bt_bmc_to_host_ctl_attn_req;
							lpc_slave_tx_data[2] <= ipmi_bt_host_to_bmc_ctl_attn_req_reg;
							lpc_slave_tx_data[1] <= 1'b0;
							lpc_slave_tx_data[0] <= 1'b0;
						end
						(IPMI_BT_PORT_BASE_ADDRESS + 1): begin
							lpc_slave_tx_data <= ipmi_bt_output_xfer_read_data;
							ipmi_bt_output_xfer_read_addr <= ipmi_bt_output_xfer_read_addr + 1;
						end
						(IPMI_BT_PORT_BASE_ADDRESS + 2): begin
							lpc_slave_tx_data[7:2] = 6'b000000;
							lpc_slave_tx_data[1] = ipmi_bt_irq_req;
							lpc_slave_tx_data[0] = ipmi_bt_irq_enable;
						end

						default: begin
`ifdef ENABLE_16550_UART_SUPPORT
							if (lpc_slave_address_reg[15:3] == HOST_UART_BASE_ADDRESS[15:3]) begin
								host_uart_3f8_address_reg <= lpc_slave_address_reg - HOST_UART_BASE_ADDRESS;
								host_uart_3f8_wren_reg <= 1'b0;
								host_uart_3f8_chip_select_reg <= 1'b1;
								host_uart_3f8_xfer_cycle_active_reg <= 1'b1;
							end
`endif
						end
					endcase

					lpc_done_pulse_counter <= 0;
					fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IR02;
				end
				LPC_BRIDGE_TRANSFER_STATE_IR02: begin
`ifdef ENABLE_16550_UART_SUPPORT
					if (lpc_slave_address_reg[15:3] == HOST_UART_BASE_ADDRESS[15:3]) begin
						// Wait for Wishbone bus cycle to complete
						if (host_uart_3f8_xfer_cycle_active_reg && host_uart_3f8_ack) begin
							lpc_slave_tx_data <= host_uart_3f8_data_in;
							host_uart_3f8_chip_select_reg <= 1'b0;
							host_uart_3f8_xfer_cycle_active_reg <= 1'b0;
						end

						if (!host_uart_3f8_xfer_cycle_active_reg && !host_uart_3f8_ack) begin
							lpc_slave_continue <= 1;
							fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IR03;
						end
					end else begin
						lpc_slave_continue <= 1;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IR03;
					end
`else
					lpc_slave_continue <= 1;
					fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IR03;
`endif
				end
				LPC_BRIDGE_TRANSFER_STATE_IR03: begin
					if (lpc_done_pulse_counter > 2) begin
						lpc_slave_continue <= 0;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IR04;
					end
					lpc_done_pulse_counter <= lpc_done_pulse_counter + 1;
				end
				LPC_BRIDGE_TRANSFER_STATE_IR04: begin
					if (!lpc_slave_address_ready_reg) begin
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IDLE;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_TR01: begin
					if (lpc_slave_address_ready_reg) begin
						lpc_slave_address_reg <= lpc_slave_address;

						if (lpc_slave_cycle_direction_reg) begin
							fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR01;
						end else begin
							fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD01;
						end
					end else begin
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IDLE;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_WR01: begin
					if (lpc_slave_data_ready_reg) begin
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR02;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_WR02: begin
					if (use_flash_sector_cache) begin
						flash_cache_addr_port_a <= lpc_slave_address_reg - hiomap_active_window_start_address;
						flash_cache_wren_port_a <= 0;
						spi_byte_write_count <= 0;
						lpc_fw_output_xfer_read_addr <= 0;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR03;
					end else begin
						// This shouldn't happen!
						// According to the LPC specification, there is no way to signal
						// a failed write to the host.  What happens after this is
						// undefined behavior according to the specification, so just
						// ignore the write and carry on as if nothing happened.
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR06;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_WR03: begin
					// Wait for RAM to respond with read data
					fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR04;
				end
				LPC_BRIDGE_TRANSFER_STATE_WR04: begin
					// Data bytes
					flash_cache_write_data_port_a <= lpc_fw_output_xfer_read_data;
					flash_cache_wren_port_a <= 1;
					fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR05;

					lpc_fw_output_xfer_read_addr <= spi_byte_write_count + 1;
					spi_byte_write_count <= spi_byte_write_count + 1;
				end
				LPC_BRIDGE_TRANSFER_STATE_WR05: begin
					flash_cache_addr_port_a <= flash_cache_addr_port_a + 1;
					flash_cache_wren_port_a <= 0;

					case (lpc_slave_fw_msize)
						4'b0000: begin
							// Invalid MSIZE -- abort!
							fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR06;
						end
						4'b0001: begin
							if (spi_byte_write_count >= 1) begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR06;
							end else begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR04;
							end
						end
						4'b0010: begin
							if (spi_byte_write_count >= 4) begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR06;
							end else begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR04;
							end
						end
						4'b0100: begin
							if (spi_byte_write_count >= 16) begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR06;
							end else begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR04;
							end
						end
						4'b0111: begin
							if (spi_byte_write_count >= 128) begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR06;
							end else begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR04;
							end
						end
					endcase
				end
				LPC_BRIDGE_TRANSFER_STATE_WR06: begin
					lpc_slave_data_ack <= 1;
					fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR07;
					lpc_done_pulse_counter <= 0;
				end
				LPC_BRIDGE_TRANSFER_STATE_WR07: begin
					if (lpc_done_pulse_counter > 2) begin
						lpc_slave_data_ack <= 0;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_WR08;
					end
					lpc_done_pulse_counter <= lpc_done_pulse_counter + 1;
				end
				LPC_BRIDGE_TRANSFER_STATE_WR08: begin
					if (!lpc_slave_data_ready_reg) begin
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IDLE;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_RD01: begin
					if (use_flash_sector_cache) begin
						flash_cache_read_addr <= lpc_slave_address_reg - hiomap_active_window_start_address;
						spi_byte_read_count <= 0;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RC01;
					end else begin
						// 4-byte quad input quad output fast read command
						// Requires 10 dummy cycles by default after last address byte transmission
						if (!spi_external_master_transaction_complete) begin
							spi_external_master_tx_data <= 8'hec;
							spi_external_master_hold_ss_active <= 1;
							spi_external_master_cycle_start <= 1;
							fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD02;
						end
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_RD02: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD03;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_RD03: begin
					// Address (4 bytes / 1 word)
					if (!spi_external_master_transaction_complete) begin
						spi_external_master_tx_data <= {4'b0000, lpc_slave_address_reg[27:0]};
						spi_external_master_qspi_mode_active <= 1;
						spi_external_master_qspi_transfer_mode <= 1;
						spi_external_master_qspi_transfer_direction <= 1;
						spi_external_master_dummy_cycle_count <= SPI_FAST_READ_DUMMY_CLOCK_CYCLES;
						spi_external_master_cycle_start <= 1;

						spi_byte_read_count <= 0;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD04;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_RD04: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD05;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_RD05: begin
					if (!spi_external_master_transaction_complete) begin
						// Data words
						spi_external_master_tx_data <= 0;	// Not used
						spi_external_master_qspi_mode_active <= 1;
						if (lpc_slave_fw_msize == 4'b0001) begin
							spi_external_master_qspi_transfer_mode <= 0;
						end else begin
							spi_external_master_qspi_transfer_mode <= 1;
						end
						spi_external_master_qspi_transfer_direction <= 0;
						spi_external_master_dummy_cycle_count <= 0;
						spi_external_master_cycle_start <= 1;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD06;

						lpc_fw_input_xfer_write_wren <= 0;
						lpc_fw_input_xfer_write_addr <= spi_byte_read_count;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_RD06: begin
					if (spi_external_master_transaction_complete) begin
						spi_external_master_cycle_start <= 0;

						// Start first memory write as early as possible
						if (lpc_slave_fw_msize == 4'b0001) begin
							lpc_fw_input_xfer_write_data <= spi_external_master_rx_data[7:0];
						end else begin
							lpc_fw_input_xfer_write_data <= spi_external_master_rx_data[31:24];
						end
						lpc_fw_input_xfer_write_wren <= 1;
						spi_byte_read_count <= spi_byte_read_count + 1;

						case (lpc_slave_fw_msize)
							4'b0001: begin
								// Only needed that one byte
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD08;
							end
							4'b0010: begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD07;
							end
							4'b0100: begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD07;
							end
							4'b0111: begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD07;
							end
							default: begin
								// Invalid MSIZE
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IDLE;
							end
						endcase
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_RD07: begin
					// Write remaining bytes
					case (spi_byte_read_count[1:0])
						2'b01: lpc_fw_input_xfer_write_data <= spi_external_master_rx_data[23:16];
						2'b10: lpc_fw_input_xfer_write_data <= spi_external_master_rx_data[15:8];
						2'b11: lpc_fw_input_xfer_write_data <= spi_external_master_rx_data[7:0];
					endcase
					lpc_fw_input_xfer_write_addr <= spi_byte_read_count;
					lpc_fw_input_xfer_write_wren <= 1;
					spi_byte_read_count <= spi_byte_read_count + 1;

					if (spi_byte_read_count[1:0] == 2'b11) begin
						// Word fully written to LPC transfer RAM
						// Determine if we need more words from Flash or if the transfer is complete
						case (lpc_slave_fw_msize)
							4'b0010: begin
								if (spi_byte_read_count >= (4-1)) begin
									fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD08;
								end else begin
									fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD05;
								end
							end
							4'b0100: begin
								if (spi_byte_read_count >= (16-1)) begin
									fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD08;
								end else begin
									fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD05;
								end
							end
							4'b0111: begin
								if (spi_byte_read_count >= (128-1)) begin
									fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD08;
								end else begin
									fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD05;
								end
							end
						endcase
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_RD08: begin
					if (!spi_external_master_transaction_complete) begin
						// Release CS and prepare the SPI device for the next command
						spi_external_master_hold_ss_active <= 0;
						spi_external_master_qspi_mode_active <= 0;
						spi_external_master_qspi_transfer_mode <= 0;
						lpc_fw_input_xfer_write_wren <= 0;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD09;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_RD09: begin
					lpc_slave_continue <= 1;
					fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD10;
					lpc_done_pulse_counter <= 0;
				end
				LPC_BRIDGE_TRANSFER_STATE_RD10: begin
					if (lpc_done_pulse_counter > 2) begin
						lpc_slave_continue <= 0;
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD11;
					end
					lpc_done_pulse_counter <= lpc_done_pulse_counter + 1;
				end
				LPC_BRIDGE_TRANSFER_STATE_RD11: begin
					if (!lpc_slave_address_ready_reg) begin
						fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_IDLE;
					end
				end
				LPC_BRIDGE_TRANSFER_STATE_RC01: begin
					// Set up next write and wait for cache RAM to respond with read data
					lpc_fw_input_xfer_write_wren <= 0;
					lpc_fw_input_xfer_write_addr <= spi_byte_read_count;
					spi_byte_read_count <= spi_byte_read_count + 1;

					fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RC02;
				end
				LPC_BRIDGE_TRANSFER_STATE_RC02: begin
					// Write data to LPC buffer
					lpc_fw_input_xfer_write_data <= flash_cache_read_data;
					lpc_fw_input_xfer_write_wren <= 1;

					// Set up next read
					flash_cache_read_addr <= flash_cache_read_addr + 1;

					case (lpc_slave_fw_msize)
						4'b0000: begin
							// Invalid MSIZE -- abort!
							fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RC03;
						end
						4'b0001: begin
							if (spi_byte_read_count >= 1) begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RC03;
							end else begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RC01;
							end
						end
						4'b0010: begin
							if (spi_byte_read_count >= 4) begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RC03;
							end else begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RC01;
							end
						end
						4'b0100: begin
							if (spi_byte_read_count >= 16) begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RC03;
							end else begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RC01;
							end
						end
						4'b0111: begin
							if (spi_byte_read_count >= 128) begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RC03;
							end else begin
								fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RC01;
							end
						end
					endcase
				end
				LPC_BRIDGE_TRANSFER_STATE_RC03: begin
					lpc_fw_input_xfer_write_wren <= 0;
					lpc_slave_continue <= 1;
					fw_transfer_state <= LPC_BRIDGE_TRANSFER_STATE_RD10;
					lpc_done_pulse_counter <= 0;
				end
				default: begin
					// Should never reach this state
					spi_external_master_cycle_start <= 0;
					fw_transfer_state <= LPC_BRIDGE_INITIALIZE_STATE_01;
				end
			endcase

			if (fw_transfer_state != LPC_BRIDGE_TRANSFER_STATE_IW01) begin
				// Handle asynchronous IPMI BT interface handshake signals
				if (ipmi_bt_bmc_to_host_ctl_sms_ack_cont) begin
					ipmi_bt_bmc_to_host_ctl_sms_ack_reg <= 0;
				end
				if (ipmi_bt_bmc_to_host_ctl_attn_ack_cont) begin
					ipmi_bt_bmc_to_host_ctl_attn_ack_reg <= 0;
				end
				if (ipmi_bt_host_to_bmc_ctl_attn_req_cont) begin
					ipmi_bt_host_to_bmc_ctl_attn_req_reg <= 0;
				end
				if (ipmi_bt_irq_ack_cont) begin
					ipmi_bt_irq_ack_reg <= 0;
				end
			end
		end

`ifdef ENABLE_SERIAL_IRQ_SUPPORT
		// Handle IRQ lines
		// WARNING: This works for continous mode only!
		// For quiet mode, each assert / deassert needs to be specifically handled by strobing ipmi_bt_irq_req / lpc_slave_irq_tx_ready on IRQ status change
		lpc_slave_irq_request[IPMI_BT_IRQ] = ipmi_bt_irq_req;
`ifdef ENABLE_16550_UART_SUPPORT
		lpc_slave_irq_request[HOST_UART_IRQ] = host_uart_3f8_irq;
`endif
`endif

		// Avoid glitches from signals crossing clock domains
		lpc_slave_address_ready_reg <= lpc_slave_address_ready;
		lpc_slave_data_ready_reg <= lpc_slave_data_ready;
		lpc_slave_tpm_cycle_reg <= lpc_slave_tpm_cycle;
		lpc_slave_firmware_cycle_reg <= lpc_slave_firmware_cycle;
		lpc_slave_cycle_direction_reg <= lpc_slave_cycle_direction;
	end
endmodule
