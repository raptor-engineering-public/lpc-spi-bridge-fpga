EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "LPC Bridge Development System"
Date "2020-05-15"
Rev "1.1"
Comp "Raptor Engineering, LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1100 1050 1500 300 
U 5EDCE465
F0 "LPC Interface" 50
F1 "lpc_interface.sch" 50
$EndSheet
$Sheet
S 1100 1650 1500 300 
U 5EDE0EB9
F0 "SPI Interface" 50
F1 "spi_interface.sch" 50
$EndSheet
$Sheet
S 1100 2250 1500 300 
U 5EE3EF57
F0 "Debug LEDs" 50
F1 "debug_led.sch" 50
$EndSheet
$EndSCHEMATC
