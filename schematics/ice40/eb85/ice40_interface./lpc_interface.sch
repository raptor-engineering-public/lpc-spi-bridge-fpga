EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "LPC Bridge Development System"
Date "2020-05-15"
Rev "1.1"
Comp "Raptor Engineering, LLC"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J?
U 1 1 5EDDF4AC
P 1600 1850
AR Path="/5EDDF4AC" Ref="J?"  Part="1" 
AR Path="/5EDCE465/5EDDF4AC" Ref="J?"  Part="1" 
F 0 "J?" H 1650 2467 50  0000 C CNN
F 1 "TPM Connector" H 1650 2376 50  0000 C CNN
F 2 "" H 1600 1850 50  0001 C CNN
F 3 "~" H 1600 1850 50  0001 C CNN
	1    1600 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 1750 1150 1050
Wire Wire Line
	1150 1750 1400 1750
Wire Wire Line
	2500 1750 1900 1750
Wire Wire Line
	2500 1350 2500 1750
Wire Wire Line
	2750 1650 2750 1850
Wire Wire Line
	2750 1850 1900 1850
Wire Wire Line
	3000 2250 3000 900 
Wire Wire Line
	3000 900  1050 900 
Wire Wire Line
	1050 900  1050 1950
Wire Wire Line
	1050 1950 1400 1950
Wire Wire Line
	2850 3750 2850 2150
Wire Wire Line
	2850 2150 1900 2150
Text Label 4550 3750 0    50   ~ 0
SIRQ
Text Label 4550 2250 0    50   ~ 0
LAD0
Text Label 4550 1650 0    50   ~ 0
LAD1
Text Label 4550 1350 0    50   ~ 0
LAD2
Text Label 4550 1050 0    50   ~ 0
LAD3
Wire Wire Line
	1150 1050 5050 1050
Wire Wire Line
	2500 1350 5050 1350
Wire Wire Line
	3000 2250 5050 2250
$Comp
L Lattice_iCE_FPGA:iCE40-HX8K-CT256 U?
U 4 1 5EDDF4C6
P 9000 950
AR Path="/5EDDF4C6" Ref="U?"  Part="4" 
AR Path="/5EDCE465/5EDDF4C6" Ref="U3"  Part="4" 
F 0 "U3" H 10030 -1397 60  0000 L CNN
F 1 "iCE40-HX8K-CT256" H 10030 -1503 60  0000 L CNN
F 2 "" H 9200 1000 60  0001 L CNN
F 3 "" H 9200 800 60  0001 L CNN
	4    9000 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 3750 7550 3750
Wire Wire Line
	7550 3750 7550 6350
Text Label 4550 6350 0    50   ~ 0
LPC_CLK
Wire Wire Line
	3150 2650 3150 2200
Wire Wire Line
	3150 750  900  750 
Wire Wire Line
	900  750  900  1550
Wire Wire Line
	900  1550 1400 1550
Wire Wire Line
	3150 2650 4950 2650
Text Label 4550 2650 0    50   ~ 0
FRAME_N
Wire Wire Line
	3300 3050 3300 600 
Wire Wire Line
	3300 600  750  600 
Wire Wire Line
	750  600  750  1650
Wire Wire Line
	750  1650 1400 1650
Wire Wire Line
	3300 3050 5050 3050
Text Label 4550 3050 0    50   ~ 0
RESET_N
Wire Wire Line
	600  1450 600  6350
Wire Wire Line
	600  1450 1400 1450
Wire Wire Line
	7550 6350 600  6350
Text Notes 1300 2550 0    50   ~ 0
BLACKBIRD / TALOS II
Text Notes 7450 2000 0    50   ~ 0
iCE40 BREAKOUT BOARD EB85
Wire Notes Line
	5150 700  5150 6450
Wire Notes Line
	5150 6450 11050 6450
Wire Notes Line
	11050 6450 11050 700 
Wire Notes Line
	11050 700  5150 700 
Wire Notes Line
	1200 1150 1200 2600
Wire Notes Line
	1200 2600 2200 2600
Wire Notes Line
	2200 2600 2200 1150
Wire Notes Line
	2200 1150 1200 1150
Wire Wire Line
	5050 1250 5050 1050
Connection ~ 5050 1050
Wire Wire Line
	5050 1050 5900 1050
Wire Wire Line
	5050 1250 5900 1250
Wire Wire Line
	5050 1450 5050 1350
Connection ~ 5050 1350
Wire Wire Line
	5050 1850 5050 1650
Connection ~ 5050 1650
Wire Wire Line
	5050 1650 2750 1650
Wire Wire Line
	5050 2850 5050 2250
Wire Wire Line
	5050 1350 5900 1350
Wire Wire Line
	5050 1650 5900 1650
Connection ~ 5050 2250
Wire Wire Line
	5050 1850 5900 1850
$Comp
L Lattice_iCE_FPGA:iCE40-HX8K-CT256 U?
U 5 1 5EDDF4F6
P 5900 950
AR Path="/5EDDF4F6" Ref="U?"  Part="5" 
AR Path="/5EDCE465/5EDDF4F6" Ref="U3"  Part="5" 
F 0 "U3" H 6930 -1597 60  0000 L CNN
F 1 "iCE40-HX8K-CT256" H 6930 -1703 60  0000 L CNN
F 2 "" H 6100 1000 60  0001 L CNN
F 3 "" H 6100 800 60  0001 L CNN
	5    5900 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2250 5900 2250
Wire Wire Line
	5050 2850 5900 2850
Wire Wire Line
	5050 1450 5900 1450
Wire Wire Line
	5050 3850 5050 3750
Connection ~ 5050 3750
Wire Wire Line
	5050 3750 2850 3750
Wire Wire Line
	5050 3850 5900 3850
Wire Wire Line
	5050 3750 5900 3750
Wire Wire Line
	5050 3650 5050 3050
Connection ~ 5050 3050
Wire Wire Line
	5050 3050 5900 3050
Wire Wire Line
	5050 3650 5900 3650
Wire Wire Line
	4950 3450 4950 2650
Connection ~ 4950 2650
Wire Wire Line
	4950 2650 5900 2650
Wire Wire Line
	4950 3450 5900 3450
Wire Wire Line
	3150 800  3150 750 
Text GLabel 8900 1250 0    50   Input ~ 0
DEBUG_15
Text GLabel 8900 1450 0    50   Input ~ 0
DEBUG_14
Text GLabel 8900 1850 0    50   Input ~ 0
DEBUG_13
Text GLabel 8900 2650 0    50   Input ~ 0
DEBUG_12
Text GLabel 8900 2950 0    50   Input ~ 0
DEBUG_11
Text GLabel 8900 3150 0    50   Input ~ 0
DEBUG_10
Text GLabel 8900 3550 0    50   Input ~ 0
DEBUG_09
Text GLabel 8900 2850 0    50   Input ~ 0
DEBUG_08
Wire Wire Line
	8900 3550 9000 3550
Wire Wire Line
	8900 3150 9000 3150
Wire Wire Line
	8900 2950 9000 2950
Wire Wire Line
	8900 2850 9000 2850
Wire Wire Line
	8900 2650 9000 2650
Wire Wire Line
	8900 1850 9000 1850
Wire Wire Line
	8900 1450 9000 1450
Wire Wire Line
	8900 1250 9000 1250
$EndSCHEMATC
