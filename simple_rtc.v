// © 2020 Raptor Engineering, LLC
//
// Released under the terms of the AGPL v3
// See the LICENSE file for full details

module simple_rtc(
		input wire base_clock,
		input wire [31:0] clock_set,
		output wire [31:0] current_time,
		input wire set_strobe
	);

	parameter BASE_CLOCK_FREQUENCY_KHZ = 12000;

	reg [31:0] current_time_reg = 1577836800;	// 01/01/2020 00:00:00 UTC
	reg [31:0] seconds_counter = 0;
	reg set_strobe_prev = 0;

	assign current_time = current_time_reg;

	always @(posedge base_clock) begin
		if (set_strobe && !set_strobe_prev) begin
			current_time_reg <= clock_set;
			seconds_counter <= 0;
		end else begin
			if (seconds_counter > (BASE_CLOCK_FREQUENCY_KHZ * 1000)) begin
				current_time_reg <= current_time_reg + 1;
				seconds_counter <= 0;
			end else begin
				seconds_counter <= seconds_counter + 1;
			end
		end

		set_strobe_prev <= set_strobe;
	end
endmodule