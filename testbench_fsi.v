// © 2020 Raptor Engineering, LLC
//
// Released under the terms of the AGPL v3
// See the LICENSE file for full details

`timescale 1ns / 100ps

`define TEST_SLAVE_CORE

// Main testbench
module fsi_test();

	reg peripheral_reset = 1;
	reg peripheral_clock = 0;

	reg [1:0] slave_id = 0;
	reg [20:0] address = 0;
	reg [31:0] tx_data = 0;
	wire [31:0] rx_data;
	reg [1:0] data_length = 0;
	reg data_direction = 0;
	reg start_cycle = 0;
	wire cycle_complete;
	wire [2:0] cycle_error;
	reg [1:0] enhanced_error_recovery = 2'b11;
	reg enable_ipoll = 1;

	wire fsi_data_out;
`ifdef TEST_SLAVE_CORE
	wire fsi_data_in;
`else
	reg fsi_data_in = 0;
`endif
	wire fsi_data_direction;
	wire fsi_clock_out;
	wire ipoll_error;

	fsi_master_interface U1(
		.slave_id(slave_id),
		.address(address),
		.tx_data(tx_data),
		.rx_data(rx_data),
		.data_length(data_length),
		.data_direction(data_direction),
		.enable_relative_address(1'b1),
		.enable_extended_address_mode(1'b0),
		.enable_crc_protection(1'b1),
		.start_cycle(start_cycle),
		.cycle_complete(cycle_complete),
		.cycle_error(cycle_error),
		.enhanced_error_recovery(enhanced_error_recovery),
		.internal_cmd_issue_delay(8'h00),
		.ipoll_enable_slave_id(4'b0001),
		.enable_ipoll(enable_ipoll),
		.ipoll_error(ipoll_error),

		.fsi_data_out(fsi_data_out),
		.fsi_data_in(fsi_data_in),
		.fsi_data_direction(fsi_data_direction),
		.fsi_clock_out(fsi_clock_out),

		.peripheral_reset(peripheral_reset),
		.peripheral_clock(peripheral_clock)
	);

	wire [20:0] slave_address;
	reg [31:0] slave_tx_data = 0;
	wire [31:0] slave_rx_data;
	wire [1:0] slave_data_length;
	wire slave_data_direction;
	wire slave_data_request_strobe;
	reg slave_data_ready_strobe = 0;
	reg [1:0] slave_enhanced_error_recovery = 2'b11;

	reg [1:0] slave_interrupt_field = 1;
	reg [2:0] slave_dma_control_field = 5;

	wire slave_fsi_data_out;
	wire slave_fsi_data_in;
	wire slave_fsi_data_direction;
	wire slave_fsi_clock_in;

	fsi_slave_interface U2(
		.slave_id(2'b00),
		.address(slave_address),
		.tx_data(slave_tx_data),
		.rx_data(slave_rx_data),
		.data_length(slave_data_length),
		.data_direction(slave_data_direction),
		.enable_extended_address_mode(1'b0),
		.enable_crc_protection(1'b1),
		.data_request_strobe(slave_data_request_strobe),
		.data_ready_strobe(slave_data_ready_strobe),
		.enhanced_error_recovery(slave_enhanced_error_recovery),

		.interrupt_field(slave_interrupt_field),
		.dma_control_field(slave_dma_control_field),

		.fsi_data_out(slave_fsi_data_out),
		.fsi_data_in(slave_fsi_data_in),
		.fsi_data_direction(slave_fsi_data_direction),
		.fsi_clock_in(slave_fsi_clock_in),

		.peripheral_reset(peripheral_reset),
		.peripheral_clock(peripheral_clock)
	);

	reg emi_test = 0;
	assign slave_fsi_clock_in = fsi_clock_out;
	assign slave_fsi_data_in = fsi_data_out | emi_test;
`ifdef TEST_SLAVE_CORE
	assign fsi_data_in = slave_fsi_data_out | emi_test;
`endif

	initial begin
		$dumpfile("fsi_interface_test.vcd");		// Dumpfile
		$dumpvars;					// Dump all signals
	end

	always
		#3 peripheral_clock = ~peripheral_clock;	// Main 166.7MHz clock

	initial begin
		#15						// Wait for clock to stabilize
		peripheral_reset = 0;				// Bring cores out of reset
		#1554						// Wait for post-reset BREAK command to finish
		data_direction = 0;				// Drive read request
		slave_id = 0;					// Absolute addressing since it is the first transfer after reset
		address = 21'h012345;				// 33'b100100100001001000110100010111000, CRC protected portion: 29'h1242468B, CRC: 4'h8
		//address = 21'h154321;				// 33'b100100110101010000110010000111100, CRC protected portion: 29'h126A8643, CRC: 4'hc
		//address = 21'h054321;				// 33'b100100100101010000110010000111011, CRC protected portion: 29'h124A8643, CRC: 4'hb
		//address = 21'h050321;				// 33'b100100100101000000110010000110101, CRC protected portion: 29'h124A0643, CRC: 4'h5
		data_length = 2;				// 32 bit transfer
		start_cycle = 1;				// Start cycle
		#234
`ifdef TEST_SLAVE_CORE
		slave_tx_data = 32'hfacefeed;			// Send response via slave core
		slave_data_ready_strobe = 1;
		#12
		slave_data_ready_strobe = 0;
		#234
		start_cycle = 0;				// End cycle
		#48
		data_direction = 0;				// Drive read request
		slave_id = 0;					// Relative addressing since it is within 256 bytes of the previous transfer address
		address = 21'h012344;
		data_length = 0;				// 8 bit transfer
		start_cycle = 1;				// Start cycle
		#60
		emi_test <= 1;					// Simulate EMI
		#6
		emi_test <= 0;
		#156
		slave_tx_data = 8'h5a;				// Send response via slave core
		slave_data_ready_strobe = 1;
		#12
		slave_data_ready_strobe = 0;
		#234
		slave_data_ready_strobe = 1;
		#12
		slave_data_ready_strobe = 0;
		#60
		emi_test <= 1;					// Simulate EMI
		#6
		emi_test <= 0;
		#282
		start_cycle = 0;				// End cycle
		#1200
		data_direction = 0;				// Drive read request
		slave_id = 0;					// Relative addressing since it is within 256 bytes of the previous transfer address
		address = 21'h012245;
		data_length = 2;				// 32 bit transfer
		start_cycle = 1;				// Start cycle
		#4680						// Purposefully stall well beyond the normal timeout (test slave core BUSY generation)
		slave_tx_data = 32'hdeadbeef;			// Send response via slave core
		slave_data_ready_strobe = 1;
		#12
		slave_data_ready_strobe = 0;
		#282
		start_cycle = 0;				// End cycle
		#48
		data_direction = 1;				// Drive write request
		slave_id = 0;					// Absolute addressing since it is outside 256 bytes of the previous transfer address
		address = 21'h054321;
		tx_data = 32'hf00dface;
		data_length = 2;				// 32 bit transfer
		start_cycle = 1;				// Start cycle
		#426
		slave_data_ready_strobe = 1;			// Send response via slave core
		#12
		slave_data_ready_strobe = 0;
		#282
		start_cycle = 0;				// End cycle
`else
		fsi_data_in = 1'b0;				// Transmit ACK_D response, 37'b10000011101100101010000110010000100001011
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;				// Return to idle
		#24
		start_cycle = 0;				// End cycle
		#48
		data_direction = 0;				// Drive read request
		slave_id = 0;					// Relative addressing since it is within 256 bytes of the previous transfer address
		address = 21'h012344;
		data_length = 2;				// 32 bit transfer
		start_cycle = 1;				// Start cycle
		#1200
		fsi_data_in = 1'b0;				// Transmit BUSY response, 9'b100010101
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;				// Return to idle
		#1200
		fsi_data_in = 1'b0;				// Transmit BUSY response, 9'b100010101
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;				// Return to idle
		#1200
		fsi_data_in = 1'b0;				// Transmit ERR_C response, 9'b100111011
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;				// Return to idle
		#1200
		fsi_data_in = 1'b0;				// Transmit BUSY response, 9'b100010101
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;				// Return to idle
		#1200
		fsi_data_in = 1'b0;				// Transmit ***corrupted*** BUSY response, 9'b100010100
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;				// Return to idle
		#1200
		fsi_data_in = 1'b0;				// Transmit BUSY response, 9'b100010101
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;				// Return to idle
		#1200
		fsi_data_in = 1'b0;				// Transmit ACK_D response, 37'b10000011101100101010000110010000100001011
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;				// Return to idle
		#24
		start_cycle = 0;				// End cycle
		#1200						// System will run an IPOLL if left idle for this length of time
		fsi_data_in = 1'b0;				// Transmit ***corrupt*** I_POLL_RSP response, 13'b1000010100001
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;				// Return to idle
		#1200
		fsi_data_in = 1'b0;				// Transmit I_POLL_RSP response, 13'b1000010100001
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b1;
		#6
		fsi_data_in = 1'b0;
		#6
		fsi_data_in = 1'b1;				// Return to idle
`endif
        end

        initial
                #30000 $finish;                         // Terminate after 30µs

endmodule