// © 2017 - 2020 Raptor Engineering, LLC
//
// Released under the terms of the AGPL v3
// See the LICENSE file for full details

`timescale 1ns / 100ps

`include "/usr/share/yosys/ice40/cells_sim.v"

// Main testbench
module lpc_interface_test();
	reg primary_platform_clock;

	wire lpc_slave_addrdata_3_0;
	wire lpc_slave_addrdata_2_0;
	wire lpc_slave_addrdata_1_0;
	wire lpc_slave_addrdata_0_0;

	reg lpc_slave_frame_n;
	reg lpc_slave_reset_n;

	reg [3:0] lsadr_data;
	reg lsadr_direction;

	reg [3:0] lmadr_data;
	reg lmadr_direction;

	reg lpc_clock;
	reg cycle_in_progress;

	reg spi_external_master_miso;

	lpc_bridge_top U1(
		.primary_platform_clock(primary_platform_clock),

		.lpc_slave_addrdata_3(lpc_slave_addrdata_3_0),
		.lpc_slave_addrdata_2(lpc_slave_addrdata_2_0),
		.lpc_slave_addrdata_1(lpc_slave_addrdata_1_0),
		.lpc_slave_addrdata_0(lpc_slave_addrdata_0_0),

		.lpc_slave_frame_n(lpc_slave_frame_n),
		.lpc_slave_reset_n(lpc_slave_reset_n),
		.lpc_slave_clock(lpc_clock),

		.spi_external_master_miso(spi_external_master_miso)
	);

	initial begin
		$dumpfile("lpc_interface_test.vcd");	// Dumpfile
		$dumpvars;				// Dump all signals
	end

	always
		#15 lpc_clock = ~lpc_clock;		// Main 33MHz clock (approximated)

	always
		#10 primary_platform_clock = ~primary_platform_clock;		// Main 48MHz clock (approximated)

	always
		#26 spi_external_master_miso = ~spi_external_master_miso;	// Clock *something* into MISO...

	initial begin
		// Test LPC slave
		lpc_clock = 1'b1;			// Reset all lines
		primary_platform_clock = 1'b1;
		spi_external_master_miso = 1'b0;
		lpc_slave_frame_n = 1'b1;
		lpc_slave_reset_n = 1'b1;
		lsadr_direction = 1'b0;
		cycle_in_progress = 1'b0;
		#60
		lpc_slave_frame_n = 1'b0;		// Drive frame start w/ invalid data
		lsadr_direction = 1'b1;
		lsadr_data = 4'b1010;
		cycle_in_progress = 1'b1;
		#30
		lpc_slave_frame_n = 1'b0;		// Drive frame start w/ valid data
		lsadr_direction = 1'b1;
		lsadr_data = 4'b0000;			// ISA I/O cycle
		#30
		lpc_slave_frame_n = 1'b1;		// Drive CT/DIR (I/O, read)
		lsadr_direction = 1'b1;
		lsadr_data = 4'b0000;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 1)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 2)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 3)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 4)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive TAR (stage 1)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b0;			// Drive TAR (stage 2)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b0;			// Receive sync
		#30
		lsadr_direction = 1'b0;			// Receive data (nibble 1)
		#30
		lsadr_direction = 1'b0;			// Receive data (nibble 2)
		#30
		lsadr_direction = 1'b0;			// Receive TAR (stage 1)
		#30
		lsadr_direction = 1'b0;			// Receive TAR (stage 2)
		#30
		cycle_in_progress = 1'b0;
		#90
		lpc_slave_frame_n = 1'b0;		// Drive abort cycle
		lsadr_direction = 1'b1;
		lsadr_data = 4'b1111;
		#120
		lpc_slave_frame_n = 1'b1;
		lsadr_direction = 1'b0;
		lsadr_data = 4'b1111;
		#90
		lpc_slave_frame_n = 1'b0;		// Drive frame start
		lsadr_direction = 1'b1;
		lsadr_data = 4'b0000;			// ISA I/O cycle
		cycle_in_progress = 1'b1;
		#30
		lpc_slave_frame_n = 1'b1;		// Drive CT/DIR (I/O, write)
		lsadr_direction = 1'b1;
		lsadr_data = 4'b0010;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 1)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 2)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 3)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 4)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 1)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 2)
		lsadr_data = 4'ha;
		#30
		lsadr_direction = 1'b1;			// Drive TAR (stage 1)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b0;			// Drive TAR (stage 2)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b0;			// Receive sync
		#30
		lsadr_direction = 1'b0;			// Receive TAR (stage 1)
		#30
		lsadr_direction = 1'b0;			// Receive TAR (stage 2)
		#30
		cycle_in_progress = 1'b0;
		#120
		lpc_slave_frame_n = 1'b1;
		lsadr_direction = 1'b0;
		lsadr_data = 4'b1111;
		#90
		lpc_slave_frame_n = 1'b0;		// Drive frame start
		lsadr_direction = 1'b1;
		lsadr_data = 4'b1110;			// Firmware memory cycle, write
		cycle_in_progress = 1'b1;
		#30
		lpc_slave_frame_n = 1'b1;		// Drive IDSEL
		lsadr_direction = 1'b1;	
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 1)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 2)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 3)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 4)
		lsadr_data = 4'h4;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 5)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 6)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 7)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive MSIZE (128 byte)
		lsadr_data = 4'b0111;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 1)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 2)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 3)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 4)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 5)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 6)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 7)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 8)
		lsadr_data = 4'h4;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 9)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 10)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 11)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 12)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 13)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 14)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 15)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 16)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 17)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 18)
		lsadr_data = 4'h9;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 19)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 20)
		lsadr_data = 4'ha;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 21)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 22)
		lsadr_data = 4'hb;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 23)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 24)
		lsadr_data = 4'hc;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 25)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 26)
		lsadr_data = 4'hd;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 27)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 28)
		lsadr_data = 4'he;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 29)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 30)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 31)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 32)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 33)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 34)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 35)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 36)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 37)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 38)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 39)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 40)
		lsadr_data = 4'h4;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 41)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 42)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 43)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 44)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 45)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 46)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 47)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 48)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 49)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 50)
		lsadr_data = 4'h9;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 51)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 52)
		lsadr_data = 4'ha;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 53)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 54)
		lsadr_data = 4'hb;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 55)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 56)
		lsadr_data = 4'hc;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 57)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 58)
		lsadr_data = 4'hd;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 59)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 60)
		lsadr_data = 4'he;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 61)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 62)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 63)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 64)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 65)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 66)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 67)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 68)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 69)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 70)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 71)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 72)
		lsadr_data = 4'h4;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 73)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 74)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 75)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 76)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 77)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 78)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 79)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 80)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 81)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 82)
		lsadr_data = 4'h9;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 83)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 84)
		lsadr_data = 4'ha;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 85)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 86)
		lsadr_data = 4'hb;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 87)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 88)
		lsadr_data = 4'hc;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 89)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 90)
		lsadr_data = 4'hd;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 91)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 92)
		lsadr_data = 4'he;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 93)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 94)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 95)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 96)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 97)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 98)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 99)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 100)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 101)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 102)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 103)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 104)
		lsadr_data = 4'h4;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 105)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 106)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 107)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 108)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 109)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 110)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 111)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 112)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 113)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 114)
		lsadr_data = 4'h9;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 115)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 116)
		lsadr_data = 4'ha;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 117)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 118)
		lsadr_data = 4'hb;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 119)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 120)
		lsadr_data = 4'hc;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 121)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 122)
		lsadr_data = 4'hd;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 123)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 124)
		lsadr_data = 4'he;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 125)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 126)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 127)
		lsadr_data = 4'h5;			// Purposefully switch data sequence from 0x40 to 0x50 here
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 128)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 129)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 130)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 131)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 132)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 133)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 134)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 135)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 136)
		lsadr_data = 4'h4;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 137)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 138)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 139)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 140)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 141)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 142)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 143)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 144)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 145)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 146)
		lsadr_data = 4'h9;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 147)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 148)
		lsadr_data = 4'ha;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 149)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 150)
		lsadr_data = 4'hb;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 151)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 152)
		lsadr_data = 4'hc;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 153)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 154)
		lsadr_data = 4'hd;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 155)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 156)
		lsadr_data = 4'he;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 157)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 158)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 159)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 160)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 161)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 162)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 163)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 164)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 165)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 166)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 167)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 168)
		lsadr_data = 4'h4;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 169)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 170)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 171)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 172)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 173)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 174)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 175)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 176)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 177)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 178)
		lsadr_data = 4'h9;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 179)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 180)
		lsadr_data = 4'ha;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 181)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 182)
		lsadr_data = 4'hb;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 183)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 184)
		lsadr_data = 4'hc;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 185)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 186)
		lsadr_data = 4'hd;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 187)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 188)
		lsadr_data = 4'he;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 189)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 190)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 191)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 192)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 193)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 194)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 195)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 196)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 197)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 198)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 199)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 200)
		lsadr_data = 4'h4;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 201)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 202)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 203)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 204)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 205)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 206)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 207)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 208)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 209)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 210)
		lsadr_data = 4'h9;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 211)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 212)
		lsadr_data = 4'ha;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 213)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 214)
		lsadr_data = 4'hb;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 215)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 216)
		lsadr_data = 4'hc;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 217)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 218)
		lsadr_data = 4'hd;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 219)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 220)
		lsadr_data = 4'he;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 221)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 222)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 223)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 224)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 225)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 226)
		lsadr_data = 4'h1;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 227)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 228)
		lsadr_data = 4'h2;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 229)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 230)
		lsadr_data = 4'h3;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 231)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 232)
		lsadr_data = 4'h4;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 233)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 234)
		lsadr_data = 4'h5;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 235)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 236)
		lsadr_data = 4'h6;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 237)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 238)
		lsadr_data = 4'h7;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 239)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 240)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 241)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 242)
		lsadr_data = 4'h9;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 243)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 244)
		lsadr_data = 4'ha;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 245)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 246)
		lsadr_data = 4'hb;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 247)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 248)
		lsadr_data = 4'hc;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 249)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 250)
		lsadr_data = 4'hd;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 251)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 252)
		lsadr_data = 4'he;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 253)
		lsadr_data = 4'h8;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 254)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 255)
		lsadr_data = 4'h9;
		#30
		lsadr_direction = 1'b1;			// Drive data (nibble 256)
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive TAR (stage 1)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b0;			// Drive TAR (stage 2)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b0;			// Receive sync
		#30
		lsadr_direction = 1'b0;			// Receive TAR (stage 1)
		#30
		lsadr_direction = 1'b0;			// Receive TAR (stage 2)
		#30
		cycle_in_progress = 1'b0;
		#57000
		lpc_slave_frame_n = 1'b1;
		lsadr_direction = 1'b0;
		lsadr_data = 4'b1111;
		#90
		lpc_slave_frame_n = 1'b0;		// Drive frame start
		lsadr_direction = 1'b1;
		lsadr_data = 4'b1101;			// Firmware memory cycle, read
		cycle_in_progress = 1'b1;
		#30
		lpc_slave_frame_n = 1'b1;		// Drive IDSEL
		lsadr_direction = 1'b1;	
		lsadr_data = 4'h0;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 1)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 2)
		lsadr_data = 4'he;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 3)
		lsadr_data = 4'hd;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 4)
		lsadr_data = 4'hc;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 5)
		lsadr_data = 4'hb;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 6)
		lsadr_data = 4'ha;
		#30
		lsadr_direction = 1'b1;			// Drive address (nibble 7)
		lsadr_data = 4'h9;
		#30
		lsadr_direction = 1'b1;			// Drive MSIZE (128 byte)
		lsadr_data = 4'b0111;
		#30
		lsadr_direction = 1'b1;			// Drive TAR (stage 1)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b0;			// Drive TAR (stage 2)
		lsadr_data = 4'hf;
		#30
		lsadr_direction = 1'b0;			// Receive sync
		#7680					// Receive data (256 nibbles)
		lsadr_direction = 1'b0;			// Receive TAR (stage 1)
		#30
		lsadr_direction = 1'b0;			// Receive TAR (stage 2)
		#30
		cycle_in_progress = 1'b0;
		
	end

	assign lpc_slave_addrdata_3_0 = (lsadr_direction) ? lsadr_data[3] : 1'bz;
	assign lpc_slave_addrdata_2_0 = (lsadr_direction) ? lsadr_data[2] : 1'bz;
	assign lpc_slave_addrdata_1_0 = (lsadr_direction) ? lsadr_data[1] : 1'bz;
	assign lpc_slave_addrdata_0_0 = (lsadr_direction) ? lsadr_data[0] : 1'bz;

	initial
		#130000 $finish;			// Terminate after 130µs
endmodule
