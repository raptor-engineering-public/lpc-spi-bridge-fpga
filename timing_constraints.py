#!/usr/bin/python
#
# © 2020 Raptor Engineering, LLC
#
# Released under the terms of the AGPL v3
# See the LICENSE file for full details

# NextPNR timing constraint specification file
#
# Set by LPC standard document
# Same as PCI base frequency
lpc_clock_frequency = 33

# Main clocks
ctx.addClock("fast_48mhz_platform_clock", 96)
ctx.addClock("primary_platform_clock", 12)
ctx.addClock("sequencer_clock", 33)
ctx.addClock("slow_1843khz_uart_clock", 2)
ctx.addClock("spi_core_clock", lpc_clock_frequency * 2)

# For some reason both of these clocks are showing up, for presumably different
# sections of the LPC logic.  Make sure both can run at LPC speeds.
ctx.addClock("lpc_slave_tx_clock_resynthesized", lpc_clock_frequency)
ctx.addClock("lpc_debug_mirror_clock_extern_uart", lpc_clock_frequency)